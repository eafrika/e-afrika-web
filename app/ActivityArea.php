<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityArea extends Model
{
    //protected $table = 'secteur_activites';

    public function companies()
    {
        return $this->hasMany("App\Company", 'main_activity_id');
    }

    public function subActivityAreas()
    {
        return $this->hasMany("App\SubActivityArea");
    }
}
