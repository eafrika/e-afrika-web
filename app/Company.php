<?php

namespace App;

use App\Http\Hashidable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Company extends Model
{
    use SearchableTrait;
    use Hashidable;
    use Notifiable;

    //protected $table = 'entreprises';
    protected $fillable = [
        "name",
        "initials",
        "description",
        "legal_status_id",
        "main_activity_id",
        "founded_at",
        "size",
        "country_id",
        "town",
        "code_postal",
        "phone",
        "email",
        "website",
        "location",
        "zone",
        "agencies",
        "services",
        "products",
        "partners",
        "clients",
        "business_turnover",
        "market",
        "assets",
        "realisations",
        "facebook",
        "youtube",
        "more_information",
        "slogan",
        "twitter",
        "linkedin",
        "organisation",
    ];
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 10,
            'email' => 2,
            'description' => 7,
            'companies.keywords' => 7,
            'services' => 6,
            'products' => 6,
            'partners' => 6,
            'clients' => 6,
            'activity_areas.label' => 6,
            'activity_areas.keywords' => 6,
        ],
        'joins' => [
            'activity_areas' => ['companies.main_activity_id', 'activity_areas.id'],
        ],
    ];

    public function activity()
    {
        return $this->belongsTo("App\ActivityArea", "main_activity_id");
    }

    public function country()
    {
        return $this->belongsTo("App\Country");
    }

    public function subActivities()
    {
        return $this->hasMany("App\CompanySubActivityArea");
    }

    public function teams()
    {
        return $this->hasMany("App\Team");
    }

    public function images()
    {
        return $this->hasMany("App\Image");
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function conversations()
    {
        return Conversation::with(["first_company","second_company"])->where('company1', $this->id)->orWhere('company2', $this->id)->get();
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function packages()
    {
        return $this->hasMany('App\CompanyPackage');
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return config('slack.url');
    }
}
