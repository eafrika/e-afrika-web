<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPackage extends Model
{
    protected $fillable = ['sysAccount', 'paymentToken', 'paymentMode', 'amount', 'devise', 'company_id'];
}
