<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySubActivityArea extends Model
{
    protected $fillable = ['sub_activity_area_id', 'company_id'];

    public function company()
    {
        return $this->belongsTo("App\Company");
    }

    public function subActivityArea()
    {
        return $this->belongsTo("App\SubActivityArea");
    }
}
