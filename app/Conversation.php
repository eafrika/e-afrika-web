<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = ['company1', 'company2'];

    public function messages()
    {
        return $this->hasMany('App\Message')->orderBy('created_at', 'asc');
    }

    public function first_company()
    {
        return $this->belongsTo('App\Company', 'company1');
    }

    public function second_company()
    {
        return $this->belongsTo('App\Company', 'company2');
    }
}
