<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\ResetPassword;
use App\Role;
use App\User;
use Carbon\Carbon;
use Dirape\Token\Token;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'phone' => ['required', 'max:12'],
            'address' => ['required', 'max:255'],
            'survey' => ['required', 'max:255'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    public function register(Request $request)
    {
        $data = [
            'email' => $request->email,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'survey' => $request->survey,
            'role_id' => Role::where('label', 'Client')->first()->id,
            'password' => bcrypt($request->password),
        ];
        $validator = $this->validator($data);
        if ($validator->fails())
            return $this->json_response(config('code.request.MISSING_DATA'), null, $validator->errors());

        $user = User::create($data);

        $token = JWTAuth::fromUser($user);

        JWTAuth::setToken($token);
        return $this->json_response(config('code.request.SUCCESS'), $token, JWTAuth::toUser());
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->json_response(config('code.auth.WRONG_CREDENTIALS'));
            }
        } catch (JWTException $e) {
            return $this->json_response(config('code.request.FAILURE'), $e->getMessage());
        }
        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();
        return $this->json_response(config('code.request.SUCCESS'), $token, ['user' => $user,
            'default_company' =>collect([$user->main_company()])
                ->transform(function ($item, $key) {
                    $activity = $item->activity;
                    $images = $item->images;
                    $item->services=json_decode($item->services);
                    $item->partners=json_decode($item->partners);
                    $item->products=json_decode($item->products);
                    $item->assets=json_decode($item->assets);
                    $item->clients=json_decode($item->clients);
                    $item->realisations=json_decode($item->realisations);
                    $item->teams;
                    $item->country;
//dd(Str::contains($item->logo, 'bg-logo.png'),$item);
                    if (!Str::contains($item->logo, 'bg-logo.png'))
                        $item->logo = asset("assets/system-images/companies/" . $item->id . "/" . $item->logo);
                    else
                        $item->logo = asset("assets/images/mob-logo.jpg");

                    if (empty($images->first())) {
                        $item->banners = array(asset('assets/system-images/activities/') . '/' . ($activity!=null?$activity->id:1). '/1.jpg');
                    } else {
                        $item->images->transform(function ($i, $key) {

                            return asset('assets/system-images/companies') . '/' . $i->company_id . '/' . $i->path;
                        });
                        $item->banners = $item->images;
                    }
                    return $item;
                })->first()
        ]);
    }

    public function logout()
    {
        JWTAuth::parseToken()->logout();
        return $this->json_response(config('code.request.SUCCESS'));
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function forgotPassword(Request $request)
    {
        $data = $request->all('email');
        $data['created_at'] = Carbon::now();
        $token = new Token();

        $rule = [
            'email' => 'required|email|max:255|exists:users,email',
        ];

        $user = User::where('email', $data['email'])->first();
        $validator = Validator::make($data, $rule);
        if ($validator->fails())
            return $this->liteResponse(config('code.request.VALIDATION_ERROR'), "User not found");

        try {
            $data["code"] = $token->randomNumber(5);
            Password::sendResetLink($request->only('email'));
            DB::table('password_resets')->where('email', $data['email'])->update(['code' => $data['code']]);
            $data['token'] = DB::table('password_resets')
                ->where('email', $data['email'])
                ->first()->token;
            Notification::send($user, new ResetPassword($data['token'], $data['code']));
            return $this->liteResponse(config('code.request.SUCCESS'));
        } catch (Exception $e) {
            return $this->liteResponse(config('code.request.FAILURE'), $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function resetPassword(Request $request)
    {
        $data = $request->all(['email', 'code', 'password', 'password_confirmation']);

        $rule = [
            'email' => 'required|email|max:255|exists:users,email',
            'password' => 'required|confirmed|min:6',
            'code' => 'required|numeric'
        ];

        $user = User::where('email', $data['email'])->first();
        $validator = Validator::make($data, $rule);
        if ($validator->fails())
            return $this->liteResponse(config('code.request.VALIDATION_ERROR'), $validator->errors());

        try {
            //get unexpired token
            $result = DB::table('password_resets')
                ->where('code', $data['code'])
                ->where('email', $data['email'])
                ->where('created_at', '>', Carbon::now()->subHours(2))
                ->first();

            //check if code exist
            if (empty($result) || $result === null) {
                return $this->liteResponse(config('code.request.EXPIRED'), "Wrong code or expired");
            }
            DB::table('password_resets')->where('email', $data['email'])->delete();
            $user->update([
                'password' => bcrypt($data['password'])
            ]);

            try {
                return $this->login($request);
            } catch (JWTException $e) {
                return $this->liteResponse(config('code.request.FAILURE'), $e->getMessage());
            }
        } catch (QueryException $exception) {
            return $this->liteResponse(config('code.request.FAILURE'), $exception->getMessage());
        }
    }

}
