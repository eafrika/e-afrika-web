<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanySubActivityArea;
use App\CompanyUser;
use App\Image;
use App\Mail\ContactCompany;
use App\Notifications\Register;
use App\Notifications\SlackBugNotification;
use App\Team;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $company_data
     * @param array $other
     * @return \Illuminate\Http\Response
     */
    public function store(array $company_data, array $other)
    {
        $validator = Validator::make($company_data, ["name" => ['required'],
            "legal_status_id" => ['required', 'exists:legal_statuses,id'],
            "main_activity_id" => ['required', 'exists:activity_areas,id'],
            "founded_at" => ['required'],
            "size" => ['required', 'string'],
            "country_id" => ['required', 'exists:countries,id'],
            "town" => ['required', 'string'],
            "phone" => ['required', 'string'],
            "email" => ['required', 'string'],
            "location" => ['required', 'string'],
            "zone" => ['required', 'string'],
            "agencies" => ['required', 'integer'],
            "services" => ['required', 'string'],
            "organisation" => ['required', 'string'],
            "business_turnover" => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        }
        try {
            $company = new Company();
            $company = $company->create($company_data);

            $company_sub_activities = array();
            foreach ($other['activities'] as $sub_activity) {
                array_push($company_sub_activities,
                    [
                        'company_id' => $company->id,
                        'sub_activity_area_id' => $sub_activity
                    ]
                );
            }
            //dd($company_sub_activities);
            DB::table('company_sub_activity_areas')->insert($company_sub_activities);
            Team::create([
                'role' => 'Directeur Generale',
                'name' => $other['gm_name'],
                'email' => $other['gm_email'],
                'fax' => null,
                'company_id' => $company->id,
            ]);

            CompanyUser::create(['company_id' => $company->id, 'user_id' => Auth::id(), 'main' => User::main_company() == null]);
            $company->notify(new SlackBugNotification($company));
            \auth()->user()->notify(new \App\Notifications\Company($company));
            return $this->array_response(config('code.request.SUCCESS'), null, $company);
        } catch (QueryException $exception) {
           // dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $company_data
     * @param array $other
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(array $company_data, array $other, Company $company)
    {
        // dd($company_data, $other);
        $validator = Validator::make($company_data, ["name" => ['required'],
            "legal_status_id" => ['required', 'exists:legal_statuses,id'],
            "main_activity_id" => ['required', 'exists:activity_areas,id'],
            "founded_at" => ['required'],
            "size" => ['required', 'string'],
            "country_id" => ['required', 'exists:countries,id'],
            "town" => ['required', 'string'],
            "phone" => ['required', 'string'],
            "email" => ['required', 'string'],
            "location" => ['required', 'string'],
            "zone" => ['required', 'string'],
            "agencies" => ['required', 'integer'],
            "services" => ['required', 'string'],
            "organisation" => ['required', 'string'],
            "business_turnover" => ['required', 'string'],
        ]);
        if ($validator->fails())
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            $company->update($company_data);

            $company_sub_activities = array();
            CompanySubActivityArea::where('company_id', $company->id)->delete();
            foreach ($other['activities'] as $sub_activity) {
                array_push($company_sub_activities,
                    [
                        'company_id' => $company->id,
                        'sub_activity_area_id' => $sub_activity
                    ]
                );
            }

            DB::table('company_sub_activity_areas')->insert($company_sub_activities);
            if (Arr::has($other, 'team_member1'))
                if (count($other['team_member1']) == 4)
                    Team::where('id', $other['team_member1'][0])->update([
                        'name' => $other['team_member1'][1],
                        'phone' => $other['team_member1'][2],
                        'email' => $other['team_member1'][3]
                    ]);
                else
                    DB::table('teams')->insert(
                        [
                            'role' => 'Directeur Generale',
                            'name' => $other['team_member1'][0],
                            'phone' => $other['team_member1'][1],
                            'email' => $other['team_member1'][2],
                            'company_id' => $company->id,
                        ]
                    );
            if (Arr::has($other, 'team_member2'))
                if (count($other['team_member2']) == 5)
                    Team::where('id', $other['team_member2'][0])->update([

                        'name' => $other['team_member2'][1],
                        'phone' => $other['team_member2'][2],
                        'email' => $other['team_member2'][3],
                        'fax' => $other['team_member2'][4]
                    ]);
                else
                    DB::table('teams')->insert(
                        [
                            'role' => 'Service client',
                            'name' => $other['team_member2'][0],
                            'phone' => $other['team_member2'][1],
                            'email' => $other['team_member2'][2],
                            'fax' => $other['team_member2'][3],
                            'company_id' => $company->id,
                        ]
                    );
            $company->notify(new SlackBugNotification($company, false));
            //CompanyUser::update(['company_id' => $company->id, 'user_id' => Auth::id(), 'main' => User::main_company() == null]);
            return $this->array_response(config('code.request.SUCCESS'), null, $company);
        } catch (QueryException $exception) {
            //dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $data
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function upload_logo(array $data, Company $company)
    {
        $validator = Validator::make($data, [
            'cropped_logo' => 'string'
        ]);

        if ($validator->fails())
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            return $this->array_response(config('code.request.SUCCESS'), null, Company::where('id', $company->id)->update($data));
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $data
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function upload_banner_image(array $data, Company $company)
    {
        $validator = Validator::make($data, [
            'path' => 'string'
        ]);

        if ($validator->fails())
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            $data['company_id'] = $company->id;
            return $this->array_response(config('code.request.SUCCESS'), null, Image::create($data));
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }

    /**
     * @param array $data
     * @param string $org
     * @return mixed
     */
    public function search(array $data)
    {
        $page = 20;
        if (empty($data['k'] and empty($data['s'])))
            $companies = Company::where('priority', '>', 0)
                ->whereRaw('LENGTH(name) > 1')
                ->orderBy('name', 'asc')
                ->search(join(' ', $data))
                ->paginate($page);
        else
            $companies = Company::where('priority', '>', 0)//exclude priority 0 company
            ->whereRaw('LENGTH(name) > 1')//exclude .,' ' companies name
            ->search(join(' ', $data), null, true)
                ->paginate($page);
        if (empty($companies)) {
            return $this->array_response(config('code.request.FAILURE'));
        }
        $companies->appends($data);
        //$this->search_suggestion();
        return $this->array_response(config('code.request.SUCCESS'),
            null,
            array_merge(['results' => $companies]));
    }
    /**
     * @param array $data
     * @param string $org
     * @return mixed
     */
    public function extSearch(array $data, $org)
    {
        $page = 20;
        if (empty($data['k'] and empty($data['s'])))
            $companies = Company::where('priority', '>', 0)
                ->whereRaw('LENGTH(name) > 1')
                ->where('organisation', $org)
                ->orderBy('name', 'asc')
                ->search(join(' ', $data))
                ->paginate($page);
        else
            $companies = Company::where('priority', '>', 0)//exclude priority 0 company
            ->whereRaw('LENGTH(name) > 1')//exclude .,' ' companies name
            ->where('organisation', $org)
            ->search(join(' ', $data), null, true)
                ->where('organisation', $org)
                ->paginate($page);
        if (empty($companies)) {
            return $this->array_response(config('code.request.FAILURE'));
        }
        $companies->appends($data);
        //$this->search_suggestion();
        return $this->array_response(config('code.request.SUCCESS'),
            null,
            array_merge(['results' => $companies]));
    }

    public function search_suggestion()
    {
        $companies = Company::all();
        $companies->transform(function ($item, $key) {
            return $item->subActivities->transform(function ($subItem, $subKey) {
                return $subItem->subActivityArea;
            })->pluck('label')->unique();
        })->reduce(function ($carry, $item) {
            $carry = collect($carry);
            return $carry->merge($item);
        })->unique()->dd();
    }

    /**
     * @param array $data
     * @return array
     */
    public function send_contact_mail(array $data)
    {
        $validator = Validator::make($data, [
            'company.id' => ['required', 'exists:companies,id'],
            'message' => ['required', 'string']
        ]);
        if ($validator->fails()) {
            return $this->array_response(config('code.request.FAILURE'), null, $validator->errors());
        }
        Mail::to($data['company'])
            ->later(now()->addSeconds(10), new ContactCompany($data));
        return $this->array_response(config('code.request.SUCCESS'), null, ['success']);
    }

    /**
     * @param Company $company
     * @return array
     */
    public function set_default(Company $company)
    {
        $user_def_company = User::find(Auth::id())->companies()->where('main', true)->get();
        if ($user_def_company->count() > 0) {
            foreach ($user_def_company as $item) {
                CompanyUser::where('user_id', Auth::id())->where('company_id', $item->id)->update(['main' => false]);
            }
        }
        return $this->array_response(config('code.request.SUCCESS'), null,
            CompanyUser::where('user_id', Auth::id())
                ->where('company_id', $company->id)
                ->update(['main' => true]));
    }

}
