<?php

namespace App\Http\Controllers;

use App\CompanyPackage;
use Illuminate\Http\Request;

class CompanyPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyPackage $companyPackage
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyPackage $companyPackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyPackage $companyPackage
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyPackage $companyPackage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CompanyPackage $companyPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyPackage $companyPackage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyPackage $companyPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyPackage $companyPackage)
    {
        //
    }
}
