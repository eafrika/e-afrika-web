<?php

namespace App\Http\Controllers;

use App\CompanySubActivityArea;
use Illuminate\Http\Request;

class CompanySubActivityAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanySubActivityArea $companySubActivityArea
     * @return \Illuminate\Http\Response
     */
    public function show(CompanySubActivityArea $companySubActivityArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanySubActivityArea $companySubActivityArea
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanySubActivityArea $companySubActivityArea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CompanySubActivityArea $companySubActivityArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanySubActivityArea $companySubActivityArea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanySubActivityArea $companySubActivityArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanySubActivityArea $companySubActivityArea)
    {
        //
    }
}
