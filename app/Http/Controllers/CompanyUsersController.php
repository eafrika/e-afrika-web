<?php

namespace App\Http\Controllers;

use App\CompanyUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 20;
        $companies = User::find(Auth::id())->companies()->paginate($page);
        if (empty($companies)) {
            return $this->array_response(config('code.request.FAILURE'));
        }

        return $this->array_response(config('code.request.SUCCESS'), null, ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyUser $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyUser $companyUsers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyUser $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyUser $companyUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CompanyUser $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyUser $companyUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyUser $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyUser $companyUsers)
    {
        //
    }
}
