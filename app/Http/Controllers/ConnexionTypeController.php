<?php

namespace App\Http\Controllers;

use App\ConnexionType;
use Illuminate\Http\Request;

class ConnexionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConnexionType $connexionType
     * @return \Illuminate\Http\Response
     */
    public function show(ConnexionType $connexionType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConnexionType $connexionType
     * @return \Illuminate\Http\Response
     */
    public function edit(ConnexionType $connexionType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ConnexionType $connexionType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConnexionType $connexionType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConnexionType $connexionType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConnexionType $connexionType)
    {
        //
    }
}
