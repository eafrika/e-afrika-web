<?php

namespace App\Http\Controllers;


use App\Response\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function json_response($message, $token = null, $data = null, $data_name = "data")
    {
        return response()->json($this->array_response($message, $token, $data, $data_name));
    }

    public function array_response($message, $token = null, $data = null, $data_name = "data")
    {
        $code = new Builder($message, $data_name);
        $code->setData($data);
        $code->setToken($token);
        return $code->reply();
    }

    public function alertSave(&$result)
    {
        return $result['alert'] = ['success' => 'Save Successfully'];
    }

    public function alertUpdate(&$result)
    {
        return $result['alert'] = ['success' => 'Update Successfully'];
    }

    public function alertDelete(&$result)
    {
        return $result['alert'] = ['success' => 'Delete Successfully'];
    }

    public static function imageGenerator($text, $size = '150x150')
    {
        $text = join('\n', explode(' ', $text));
        return "https://i.pickadummy.com/$size?border=0&bordercolor=000000&outline=000000&foreground=000000&color=ffffff&shadow=000000&text=$text&font=Capture It";
    }

    public static function imageGeneratorV2($text, $size = '150x150')
    {
        $text = join('\n', explode(' ', $text));
        return "https://dummyimage.com/$size/9e1e9e/fff.png&text=$text.png";
    }

    public function savePicture($file, $file_name)
    {
        $path = 'assets/images/user/' . $file_name . '.jpeg';
        $img = Image::make($file);
        $img->save(public_path($path));
        return $path;
    }
}
