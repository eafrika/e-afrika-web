<?php

namespace App\Http\Controllers;

use App\Conversation;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ConversationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function store(array $data)
    {
        $validator = Validator::make($data, [
            'company1' => ['required', 'exists:companies,id'],
            'company2' => ['required', 'exists:companies,id']
        ]);
        if ($validator->fails() or $data['company1'] == $data['company2'])
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            $c1 = Conversation::where('company1', $data['company1'])->where('company2', $data['company2'])->count();
            $c2 = Conversation::where('company2', $data['company2'])->where('company1', $data['company1'])->count();
            if ($c1 != 0 || $c2 != 0)
                return $this->array_response(config('code.request.DATA_EXIST'), null, ['message' => 'Demande en attente de réponse']);

            $data['company1'] = Auth::user()->main_company()->id;
            return $this->array_response(config('code.request.SUCCESS'), null, Conversation::create($data));
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conversation $conversations
     * @return \Illuminate\Http\Response
     */
    public function show(Conversation $conversations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Conversation $conversations
     * @return \Illuminate\Http\Response
     */
    public function edit(Conversation $conversations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Conversation $conversations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conversation $conversations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conversation $conversations
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conversation $conversations)
    {
        //
    }
}
