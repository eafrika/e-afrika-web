<?php

namespace App\Http\Controllers;

use App\LegalStatus;
use Illuminate\Http\Request;

class LegalStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LegalStatus $legalStatus
     * @return \Illuminate\Http\Response
     */
    public function show(LegalStatus $legalStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LegalStatus $legalStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(LegalStatus $legalStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\LegalStatus $legalStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LegalStatus $legalStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LegalStatus $legalStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(LegalStatus $legalStatus)
    {
        //
    }
}
