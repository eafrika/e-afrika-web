<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Message;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function store(array $data)
    {
        $conversation = Conversation::findOrFail($data['conversation_id']);
        if ($conversation->company1 == Auth::user()->main_company()->id || $conversation->company2 == Auth::user()->main_company()->id) {
            $message = new Message;
            $message->conversation_id = $data['conversation_id'];
            $message->company_id = Auth::user()->main_company()->id;
            $message->text = $data['text'];;
            $message->is_read = 0;
            try {
                $message->save();
                return $this->array_response(config('code.request.SUCCESS'), null, $message);
            } catch (QueryException $exception) {
                return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
            }
        }
        return $this->array_response(config('code.request.NOT_AUTHORIZED'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message $messages
     * @return \Illuminate\Http\Response
     */
    public function show(Message $messages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message $messages
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $messages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Message $messages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $messages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message $messages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $messages)
    {
        //
    }
}
