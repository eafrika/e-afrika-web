<?php

namespace App\Http\Controllers\Mobile;

use App\Company;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CompanyMobileController extends Controller
{
    public function detail(Request $request)
    {

    }

    public function search(Request $request)
    {
        $controller = new CompanyController();
        $result = $controller->search($request->all(['k']))['data']['results'];
        $result->transform(function ($item, $key) {
            $activity = $item->activity;
            $images = $item->images;
            $item->services=json_decode($item->services);
            $item->partners=json_decode($item->partners);
            $item->products=json_decode($item->products);
            $item->assets=json_decode($item->assets);
            $item->clients=json_decode($item->clients);
            $item->realisations=json_decode($item->realisations);
            $item->teams;
            $item->country;
//dd(Str::contains($item->logo, 'bg-logo.png'),$item);
            if (!Str::contains($item->logo, 'bg-logo.png'))
                $item->logo = asset("assets/system-images/companies/" . $item->id . "/" . $item->logo);
            else
                $item->logo = asset("assets/images/mob-logo.jpg");

            if (empty($images->first())) {
                $item->banners = array(asset('assets/system-images/activities/') . '/' . ($activity!=null?$activity->id:1). '/1.jpg');
            } else {
                $item->images->transform(function ($i, $key) {

                    return asset('assets/system-images/companies') . '/' . $i->company_id . '/' . $i->path;
                });
                $item->banners = $item->images;
            }
            return $item;
        });
        return response()->json([
            'companies' => $result
        ]);
    }

    public function register(Request $request)
    {

        //
        $controller = new CompanyController();
        $data = $request->all((new Company())->getFillable());
        $data['services'] = json_encode(array_filter($data['services']));
        $data['products'] = json_encode(array_filter($data['products']));
        $data['partners'] = json_encode(array_filter($data['partners']));
        $data['clients'] = json_encode(array_filter($data['clients']));
        $data['assets'] = json_encode(array_filter($data['assets']));
        $data['realisations'] = json_encode(array_filter($data['realisations']));
        $result = $controller->store($data, $request->all());
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $company = Company::find($request->company);
        $controller = new CompanyController();
        $data = $request->all((new Company())->getFillable());
        $data['services'] = json_encode(array_filter($data['services']));
        $data['products'] = json_encode(array_filter($data['products']));
        $data['partners'] = json_encode(array_filter($data['partners']));
        $data['clients'] = json_encode(array_filter($data['clients']));
        $data['assets'] = json_encode(array_filter($data['assets']));
        $data['realisations'] = json_encode(array_filter($data['realisations']));
        $result = $controller->update($data, $request->all(), $company);
        return response()->json($result);
    }


    public function set_default_company(Request $request)
    {
        $company = Company::find($request->company_id);
        $controller = new CompanyController();
        $result = $controller->set_default($company);
        return response()->json($result);
    }

    public function my_companies(){
        return $this->json_response(config('code.request.SUCCESS'),null,Auth::user()->companies->transform(function ($item, $key) {
            $activity = $item->activity;
            $images = $item->images;
            $item->services = json_decode($item->services);
            $item->partners = json_decode($item->partners);
            $item->products = json_decode($item->products);
            $item->assets = json_decode($item->assets);
            $item->clients = json_decode($item->clients);
            $item->realisations = json_decode($item->realisations);
            $item->teams;
            $item->country;
            if (!Str::contains($item->logo, 'bg-logo.png'))
                $item->logo = asset("assets/system-images/companies/" . $item->id . "/" . $item->logo);
            else
                $item->logo = asset("assets/images/mob-logo.jpg");

            if (empty($images->first())) {
                $item->banners = array(asset('assets/system-images/activities/') . '/' . ($activity != null ? $activity->id : 1) . '/1.jpg');
            } else {
                $item->images->transform(function ($i, $key) {

                    return asset('assets/system-images/companies') . '/' . $i->company_id . '/' . $i->path;
                });
                $item->banners = $item->images;
            }
            return $item;
        }));
    }
}
