<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Conversation;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MessagesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class MessageController extends Controller
{
    public function store(Request $request)
    {
        $conversation = Conversation::findOrFail($request->conversation_id);
        $receiver_id = ($conversation->company1 == Auth::user()->main_company()->id) ? $conversation->company2 : $conversation->company1;
        $redis = Redis::connection();
        $data = array_merge($request->all(), ['client_id' => $receiver_id, 'name' => Company::find($receiver_id)->name]);
        $controller = new MessagesController();
        $result = $controller->store($data);
        if ($result['status']) {
            $redis->publish('message', json_encode($data));
            return $this->json_response(config('code.request.SUCCESS'));
        }
        return $this->json_response(config('code.request.NOT_AUTHORIZED'));
    }

    public function room2room(Request $request)
    {
        $conversation = Conversation::findOrFail($request->conversation_id);
        $receiver_id = ($conversation->company1 != Auth::user()->main_company()->id) ? $conversation->company2 : $conversation->company1;
        $redis = Redis::connection();
        $data = array_merge($request->all(), ['client_id' => $receiver_id, 'name' => Company::find($receiver_id)->name]);
        $controller = new MessagesController();
        $result = $controller->store($data);
        if ($result['status']) {
            $redis->publish('message', json_encode($data));
            return $this->json_response(config('code.request.SUCCESS'));
        }
        return $this->json_response(config('code.request.NOT_AUTHORIZED'));
    }
}
