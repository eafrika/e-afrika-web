<?php

namespace App\Http\Controllers\Mobile;

use App\Company;
use App\Conversation;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\SocialController as BaseController;
use App\Http\Controllers\Web\CompanyWebController;
use App\Message;
use App\Response\Builder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class SocialController extends Controller
{


    public function connect(Request $request)
    {
        //dd($request->all(), $company);
        $controller = new BaseController();
        $data['type_id'] = $request->type;
        $data['receiver_company_id'] = $request->company_id;

        if (Auth::user()->main_company() != null)
            $data['sender_company_id'] = Auth::user()->main_company()->id;
        else
            return $this->json_response(config('code.request.FAILURE'), null, ['message' => 'Vous n\'avez pas d\'entreprise par défaut']);

        return response()->json($controller->store($data));
    }

    public function connection_request(Request $request)
    {
        $controller = new BaseController();
        $result = $controller->index(Company::find($request->company_id));
        if (Builder::check($result['message'], config('code.request.NOT_AUTHORIZED')))
            return $this->json_response(config('code.request.NOT_AUTHORIZED'));
        return $this->json_response(config('code.request.SUCCESS'), null,
            [
                'connexions' => $result['data']->transform(function ($item, $key) {
                    $item = $item->sender;
                    $activity = $item->activity;
                    $images = $item->images;
                    $item->services = json_decode($item->services);
                    $item->partners = json_decode($item->partners);
                    $item->products = json_decode($item->products);
                    $item->assets = json_decode($item->assets);
                    $item->clients = json_decode($item->clients);
                    $item->realisations = json_decode($item->realisations);
                    $item->teams;
                    $item->country;
                    if (!Str::contains($item->logo, 'bg-logo.png'))
                        $item->logo = asset("assets/system-images/companies/" . $item->id . "/" . $item->logo);
                    else
                        $item->logo = asset("assets/images/mob-logo.jpg");

                    if (empty($images->first())) {
                        $item->banners = array(asset('assets/system-images/activities/') . '/' . ($activity != null ? $activity->id : 1) . '/1.jpg');
                    } else {
                        $item->images->transform(function ($i, $key) {

                            return asset('assets/system-images/companies') . '/' . $i->company_id . '/' . $i->path;
                        });
                        $item->banners = $item->images;
                    }
                    return $item;
                }),
                'company' => User::main_company(),
                'list' => true
            ]);
    }

    public function accept(Request $request)
    {
        $company = Company::find($request->company_id);
        $data['sender_company_id'] = $company->id;
        $data['receiver_company_id'] = Auth::user()->main_company()->id;
        $controller = new BaseController();
        $result = $controller->update($data);
        (new CompanyWebController())->get_conversation($company);
        return response()->json($result);
    }

    public function cancel(Request $request)
    {
        $company = Company::find($request->company_id);
        $data['sender_company_id'] = $company->id;
        $data['receiver_company_id'] = Auth::user()->main_company()->id;
        $controller = new BaseController();
        $result = $controller->destroy($data);
        return response()->json($result);
    }

    public function messages(Request $request)
    {
        return $this->json_response(config("code.request.SUCCESS"), null,Message::where('conversation_id',$request->conversation)->paginate(30));
    }

    public function sendMessage(Request $request)
    {
        $conversation = Conversation::find($request->conversation_id);
        if ($conversation==null)
            return $this->json_response(config('code.request.MISSING_DATA'),null,"Conversation not found");
        $receiver_id = ($conversation->company1 == Auth::user()->main_company()->id) ? $conversation->company2 : $conversation->company1;
        $redis = Redis::connection();
        $data = array_merge($request->all(), ['client_id' => $receiver_id, 'name' => Company::find($receiver_id)->name]);
        $controller = new MessagesController();
        $result = $controller->store($data);
        if ($result['status']) {
            $redis->publish('message', json_encode($data));
            return $this->json_response(config('code.request.SUCCESS'));
        }
        return $this->json_response(config('code.request.NOT_AUTHORIZED'));
    }

    public function room2room(Request $request)
    {
        $conversation = Conversation::find($request->conversation_id);
        if ($conversation==null)
            return $this->json_response(config('code.request.MISSING_DATA'),null,"Conversation not found");

        $receiver_id = ($conversation->company1 != Auth::user()->main_company()->id) ? $conversation->company2 : $conversation->company1;
        $redis = Redis::connection();
        $data = array_merge($request->all(), ['client_id' => $receiver_id, 'name' => Company::find($receiver_id)->name]);
        $controller = new MessagesController();
        $result = $controller->store($data);
        if ($result['status']) {
            $redis->publish('message', json_encode($data));
            return $this->json_response(config('code.request.SUCCESS'));
        }
        return $this->json_response(config('code.request.NOT_AUTHORIZED'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function board(Request $request)
    {
        $company = Company::find($request->company_id);
        return $this->json_response(config("code.request.SUCCESS"), null,
            $company->conversations()->transform(function ($item, $key) {
                $conversation = $item->id;
                $item = Auth::user()->main_company()->id == $item->first_company->id ? $item->second_company : $item->first_company;
                $item['conversation'] = $conversation;
                $activity = $item->activity;
                $images = $item->images;
                $item->services = json_decode($item->services);
                $item->partners = json_decode($item->partners);
                $item->products = json_decode($item->products);
                $item->assets = json_decode($item->assets);
                $item->clients = json_decode($item->clients);
                $item->realisations = json_decode($item->realisations);
                $item->teams;
                $item->country;
                if (!Str::contains($item->logo, 'bg-logo.png'))
                    $item->logo = asset("assets/system-images/companies/" . $item->id . "/" . $item->logo);
                else
                    $item->logo = asset("assets/images/mob-logo.jpg");

                if (empty($images->first())) {
                    $item->banners = array(asset('assets/system-images/activities/') . '/' . ($activity != null ? $activity->id : 1) . '/1.jpg');
                } else {
                    $item->images->transform(function ($i, $key) {

                        return asset('assets/system-images/companies') . '/' . $i->company_id . '/' . $i->path;
                    });
                    $item->banners = $item->images;
                }
                return $item;
            })
        );
    }


}
