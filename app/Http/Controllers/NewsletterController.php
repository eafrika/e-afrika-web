<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return array
     */
    public function store(array $data)
    {
        /* $validator = Validator::make($data, [
             'email' => 'string'
         ]);

         if ($validator->fails())
             return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());*/
        try {
            return $this->array_response(config('code.request.SUCCESS'), null, Newsletter::create($data));
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show(Newsletter $newsletter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit(Newsletter $newsletter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Newsletter $newsletter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Newsletter $newsletter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsletter $newsletter)
    {
        //
    }
}
