<?php

namespace App\Http\Controllers;

use App\Newstype;
use Illuminate\Http\Request;

class NewstypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Newstype $newstype
     * @return \Illuminate\Http\Response
     */
    public function show(Newstype $newstype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Newstype $newstype
     * @return \Illuminate\Http\Response
     */
    public function edit(Newstype $newstype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Newstype $newstype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Newstype $newstype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Newstype $newstype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newstype $newstype)
    {
        //
    }
}
