<?php

namespace App\Http\Controllers;

use App\PackageService;
use Illuminate\Http\Request;

class PackageServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PackageService $packageService
     * @return \Illuminate\Http\Response
     */
    public function show(PackageService $packageService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageService $packageService
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageService $packageService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\PackageService $packageService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackageService $packageService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageService $packageService
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageService $packageService)
    {
        //
    }
}
