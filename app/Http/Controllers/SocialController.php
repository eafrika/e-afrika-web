<?php

namespace App\Http\Controllers;

use App\Company;
use App\ConnexionType;
use App\Mail\ContactCompany;
use App\Social;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        if ($company == null) {
            $company = Auth::user()->main_company();
            if ($company == null) {
                return $this->array_response(config('code.request.MISSING_DATA'));
            } else
                return $this->array_response(config('code.auth.NOT_EXISTS'));
        } else if (Auth::user()->companies()->where('id', $company->id)->first() == null)
            return $this->array_response(config('code.request.NOT_AUTHORIZED'));
        $connexions = Social::with(['sender'])->where('receiver_company_id', $company->id)
            ->where('status', false)
            ->get();
        return $this->array_response(config('code.request.SUCCESS'), null, $connexions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return array
     */
    public function store(array $data)
    {
        $validator = Validator::make($data, [
            'type_id' => ['required', 'exists:connexion_types,id'],
            'receiver_company_id' => ['required', 'exists:companies,id'],
            'sender_company_id' => ['required', 'exists:companies,id'],
        ]);
        if (Social::where('receiver_company_id', $data['receiver_company_id'])
                ->where('sender_company_id', $data['sender_company_id'])->first() != null ||
            Social::where('receiver_company_id', $data['sender_company_id'])
                ->where('sender_company_id', $data['receiver_company_id'])->first() != null)
            return $this->array_response(config('code.request.DATA_EXIST'), null, ['message' => 'Demande en attente de réponse']);

        if ($validator->fails())
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            $mail_data = ['connect' => true, 'message' => 'Je vous invite a rejoindre mon réseau ' . ConnexionType::find($data['type_id'])->label, 'company' => Company::find($data['sender_company_id'])];
            Mail::to(Company::find($data['receiver_company_id'])->email)
                ->later(now()->addSeconds(10), new ContactCompany($mail_data));
            return $this->array_response(config('code.request.SUCCESS'), null, Social::create($data));
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Social $social
     * @return \Illuminate\Http\Response
     */
    public function show(Social $social)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Social $social
     * @return \Illuminate\Http\Response
     */
    public function edit(Social $social)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $data
     * @param  \App\Social $social
     * @return \Illuminate\Http\Response
     */
    public function update(array $data)
    {
        $validator = Validator::make($data, [
            'receiver_company_id' => ['required', 'exists:companies,id'],
            'sender_company_id' => ['required', 'exists:companies,id'],
        ]);
        if ($validator->fails())
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            $data['status'] = true;
            return $this->array_response(config('code.request.SUCCESS'), null, Social::where('receiver_company_id', $data['receiver_company_id'])
                ->where('sender_company_id', $data['sender_company_id'])->update($data));
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function destroy(array $data)
    {
        $validator = Validator::make($data, [
            'receiver_company_id' => ['required', 'exists:companies,id'],
            'sender_company_id' => ['required', 'exists:companies,id'],
        ]);
        if ($validator->fails())
            return $this->array_response(config('code.request.MISSING_DATA'), null, $validator->errors());
        try {
            $data['status'] = true;
            return $this->array_response(config('code.request.SUCCESS'), null, Social::where('receiver_company_id', $data['receiver_company_id'])
                ->where('sender_company_id', $data['sender_company_id'])->delete());
        } catch (QueryException $exception) {
            dd($exception->getMessage());
            return $this->array_response(config('code.request.FAILURE'), null, $exception->getMessage());
        }
    }
}
