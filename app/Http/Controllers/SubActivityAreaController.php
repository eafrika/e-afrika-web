<?php

namespace App\Http\Controllers;

use App\SubActivityArea;
use Illuminate\Http\Request;

class SubActivityAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubActivityArea $subActivityArea
     * @return \Illuminate\Http\Response
     */
    public function show(SubActivityArea $subActivityArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubActivityArea $subActivityArea
     * @return \Illuminate\Http\Response
     */
    public function edit(SubActivityArea $subActivityArea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\SubActivityArea $subActivityArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubActivityArea $subActivityArea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubActivityArea $subActivityArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubActivityArea $subActivityArea)
    {
        //
    }
}
