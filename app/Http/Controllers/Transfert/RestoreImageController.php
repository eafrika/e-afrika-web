<?php

namespace App\Http\Controllers\Transfert;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RestoreImageController extends Controller
{
    public function restore()
    {
        $companies = DB::select("SELECT id,nom,logo FROM `entreprises` WHERE SUBSTRING_INDEX(logo, '.', 1) > " . Carbon::now()->subDays(50)->timestamp);
        foreach (scandir(public_path('Downloads')) as $filename) {
            if (!is_dir($filename)) {
                foreach ($companies as $company)
                    if (str_contains(strtolower($company->nom), explode('.', strtolower($filename))[0])) {
                        if (!is_dir(public_path('done/' . $company->id)))
                            mkdir(public_path('done/' . $company->id), 0777, true);
                        copy(public_path('Downloads/' . $filename), public_path('done/' . $company->id . '/' . $company->logo));
                    }
            }
        }
        dd(compact('companies'));
    }

    public function onlineRestore()
    {
        $companies = DB::select("SELECT id,nom,logo FROM `entreprises`");
        $bad = array();
        foreach ($companies as $company) {
            if (!file_exists(public_path('images/entreprises/' . $company->id . '/' . $company->logo)))
                array_push($bad, $company);
            //copy(public_path('web_hi_res_512.png'), public_path('images/entreprises/' . $company->id . '/' . $company->logo));
        }
        dd(compact('bad'));
    }
}
