<?php

namespace App\Http\Controllers\Transfert;

use App\ActivityArea;
use App\Company;
use App\Country;
use App\Http\Controllers\Controller;
use App\LegalStatus;
use App\SubActivityArea;
use App\User;

class StructureUpdateController extends Controller
{
    public function generateAll()
    {
        //$this->generateActivityArea();
        //$this->generateCountry();
        //$this->generateLegalStatut();
        $this->generateUser();
        //$this->generateSubActivityArea();
        $this->generateTeam();
        //$this->generateCompanies();
    }

    public function generateActivityArea()
    {
        $results = ActivityArea::all();
        $final_body = "";
        foreach ($results as $result) {
            $final_body .= "DB::table('activity_areas')->insert([
                'id' => '" . $result->id . "',
                'label' => '" . $result->label . "',
                'keywords' => \"" . $result->keywords . "\",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";
        }
        file_put_contents(public_path('generate/ActivityAreaSeeder.php'), $final_body);
    }

    public function generateSubActivityArea()
    {
        $results = SubActivityArea::all();
        $final_body = "";
        foreach ($results as $result) {
            $final_body .= "DB::table('sub_activity_areas')->insert([
                'id' => '" . $result->id . "',
                'activity_area_id' => '" . $result->secteur_activite_id . "',
                'label' => \"" . $result->libelle . "\",
                'keywords' => \"" . $result->mots_cles . "\",
                'competitors' => '" . json_encode(explode(',', $result->concurrents), JSON_NUMERIC_CHECK) . "',
                'clients' => '" . json_encode(explode(',', $result->clients), JSON_NUMERIC_CHECK) . "',
                'providers' => '" . json_encode(explode(',', $result->fournisseurs), JSON_NUMERIC_CHECK) . "',
                'partners' => '" . json_encode(explode(',', $result->partenaires), JSON_NUMERIC_CHECK) . "',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";
        }
        file_put_contents(public_path('generate/SubActivityAreaSeeder.php'), $final_body);
    }

    public function generateTeam()
    {
        $results = Company::all();
        $final_body = "";
        foreach ($results as $result) {
            $final_body .= "DB::table('teams')->insert([
                'company_id' => '" . $result->id . "',
                'role' => \"Directeur Général\",
                'name' => \"" . $result->nom_directeur_general . "\",
                'phone' => \"" . $result->telephone_directeur_general . "\",
                'email' => \"" . $result->email_directeur_general . "\",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";

            $final_body .= "DB::table('teams')->insert([
                'company_id' => '" . $result->id . "',
                'role' => \"Service Client\",
                'name' => \"" . ($result->autre_personne == null ? 'Service Client' : $result->autre_personne) . "\",
                'phone' => \"" . $result->autre_telephone . "\",
                'fax' => \"" . $result->autre_fax . "\",
                'email' => \"" . $result->autre_email . "\",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";
        }
        file_put_contents(public_path('generate/TeamSeeder.php'), $final_body);
    }

    public function generateCompanies()
    {
        $results = Company::all(['id',
            'main_activity_id',
            'country_id',
            'legal_status_id',
            'name',
            'logo',
            'size',
            'town',
            'phone',
            'email',
            'description',
            'zone',
            'agencies',
            'initials',
            'slogan',
            'code_postal',
            'location',
            'keywords',
            'more_information',
            'website',
            'facebook',
            'twitter',
            'youtube',
            'linkedin',
            'services',
            'products',
            'partners',
            'clients',
            'business_turnover',
            'market',
            'realisations',
            'assets',
            'views',
            'priority',
            'main']);
        $final_body = "";
        foreach ($results as $result) {
            $str = "";
            $result = json_decode($result, true);
            array_walk($result, function ($item, $key) use (&$str) {
                if (strcmp($key, 'views') != 0)
                    $str .= "\"" . $key . "\" => \"" . $item . "\",";
            });
            $final_body .= "DB::table('companies')->insert([" . $str . "]);\n";
        }
        file_put_contents(public_path('generate/CompanySeeder.php'), $final_body);
    }


    public function generateUser()
    {
        $results = User::all();
        $final_body = "";
        foreach ($results as $result) {
            $final_body .= "DB::table('users')->insert([
                'id' => '" . $result->id . "',
                'name' => \"" . $result->name . "\",
                'email' => '" . $result->email . "',
                'password' => '" . $result->password . "',
                'phone' => '" . $result->telephone . "',
                'address' => \"" . $result->adresse . "\",
                'survey' => \"" . $result->nous_connaitre . "\",
                'role_id' => \App\Role::where('label', '$result->role')->first()->id ,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";
        }
        file_put_contents(public_path('generate/UserSeeder.php'), $final_body);
    }

    public function generateCountry()
    {
        $results = Country::all();
        $final_body = "";
        foreach ($results as $result) {
            $final_body .= "DB::table('countries')->insert([
                'id' => '" . $result->id . "',
                'label' => \"" . $result->label . "\",
                'code' => '" . $result->code . "',
                'phone_indicator' => '" . $result->phone_indicator . "',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";
        }
        file_put_contents(public_path('generate/CountrySeeder.php'), $final_body);
    }

    public function generateLegalStatut()
    {
        $results = LegalStatus::all();
        $final_body = "";
        foreach ($results as $result) {
            $final_body .= "DB::table('legal_statuses')->insert([
                'id' => '" . $result->id . "',
                'label' => \"" . $result->label . "\",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()]);\n";
        }
        file_put_contents(public_path('generate/LegalStatuesSeeder.php'), $final_body);
    }
}
