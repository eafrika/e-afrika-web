<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\CompanyUsersController;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class CompanyUserWebController extends Controller
{
    public function my_company(Request $request)
    {
        $controller = new CompanyUsersController();
        $result = $controller->index();
        if ($result['data']['companies']->total() < 1)
            return redirect()->route('home');

        return view('pages.my-companies')
            ->with([
                'companies' => $result['data']['companies'],
                'company' => User::main_company()
            ]);
    }
}
