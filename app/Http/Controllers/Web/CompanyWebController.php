<?php

namespace App\Http\Controllers\Web;

use App\ActivityArea;
use App\Company;
use App\CompanyPackage;
use App\Conversation;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ConversationsController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use paymentCm\Dohone\Dohone;

class CompanyWebController extends Controller
{
    public function search_result(Request $request)
    {
        $controller = new CompanyController();
        $result = $controller->search($request->all(['k', 's']));
        $activityAreas = ActivityArea::all()->sortBy('label');
        return view('pages.company-search')
            ->with([
                'route'=>route("company.search"),
                'companies' => $result['data']['results'],
                'activityArea_part_1' => $activityAreas->split(2)[0],
                'activityArea_part_2' => $activityAreas->split(2)[1],
            ]);
    }

    public function search_wia(Request $request)
    {
        $controller = new CompanyController();
        $result = $controller->extSearch($request->all(['k', 's']),"WIA");
        $activityAreas = ActivityArea::all()->sortBy('label');
        return view('pages.company-search')
            ->with([
                'route'=>route("company.search.wia"),
                'companies' => $result['data']['results'],
                'activityArea_part_1' => $activityAreas->split(2)[0],
                'activityArea_part_2' => $activityAreas->split(2)[1],
            ]);
    }


    public function search_agm(Request $request)
    {
        $controller = new CompanyController();
        $result = $controller->extSearch($request->all(['k', 's']),"AGM");
        $activityAreas = ActivityArea::all()->sortBy('label');
        return view('pages.company-search')
            ->with([
                'route'=>route("company.search.agm"),
                'companies' => $result['data']['results'],
                'activityArea_part_1' => $activityAreas->split(2)[0],
                'activityArea_part_2' => $activityAreas->split(2)[1],
            ]);
    }

    public function search(Request $request)
    {
        $controller = new CompanyController();
        $result = $controller->search($request->all(['k', 's']))['data']['results'];
        $result->transform(function ($item, $key) {
            $model = json_decode($item);
            $model->id = route('company.detail', $item);//switch i
            return $model;
        });
        return response()->json([
            'companies' => $result
        ]);
    }

    public function detail(Company $company)
    {
        return view('pages.company-detail')->with([
            'company' => $company,
            'conversation' => $this->get_conversation($company)
        ]);
    }

    public function get_conversation(Company $company)
    {
        $controller = new ConversationsController();
        $conversation = null;
        if (Auth::check()) {
            $my_company = Auth::user()->main_company();
            if ($my_company == null)
                return redirect(url()->previous())->with('error', ['message' => 'Vous n\'avez pas d\'entreprise par défaut']);
            $conversation = Conversation::where('company2', $company->id)->where('company1', $my_company->id)->first();
            if ($conversation == null) {
                $conversation = Conversation::where('company2', $my_company->id)->where('company1', $company->id)->first();
            }
            if ($conversation == null) {
                $data['company2'] = $company->id;
                $data['company1'] = $my_company->id;
                $result = $controller->store($data);
                if (array_has($result, 'data'))
                    $conversation = $result['data'];
            }
        }
        return $conversation;
    }

    public function upload_logo(Request $request, Company $company)
    {
        $data = array();
        $logo = $request->file('logo');
        if ($logo != null) {
            $filename = 'logo.' . $logo->getClientOriginalExtension();
            $logo->move('assets/system-images/companies/' . $company->id, $filename);
            $data['logo'] = $filename;
        }
        $cropped_logo = Image::make($request->cropped_logo);
        $path = 'logo.' . explode('/', explode(';', $request->cropped_logo)[0])[1];
        $cropped_logo->save('assets/system-images/companies/' . $company->id . '/' . $path);
        $controller = new CompanyController();
        $data['cropped_logo'] = $path;
        $result = $controller->upload_logo($data, $company);
        if ($result['status'] == true) {
            $this->alertUpdate($result);
            return redirect(url()->previous())->with('result', $result);
        } else {
            $this->alertUpdate($result);
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }

    public function upload_logo_form(Request $request, Company $company)
    {
        return view('pages.company-logo')->with('company', $company);
    }

    public function upload_banner(Request $request, Company $company)
    {
        $data = array();
        $banner = $request->file('banner');
        if ($banner != null) {
            $filename = 'banner-' . time() . "." . $banner->getClientOriginalExtension();
            $banner->move('assets/system-images/companies/' . $company->id, $filename);
            $data['path'] = $filename;
        }

        $controller = new CompanyController();
        $result = $controller->upload_banner_image($data, $company);
        if ($result['status'] == true) {
            $this->alertUpdate($result);
            return redirect()->route('company.detail', $company)->with('result', $result);
        } else {
            $this->alertUpdate($result);
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }

    public function upload_banner_form(Request $request, Company $company)
    {
        return view('pages.company-banner')->with('company', $company);
    }

    public function show_contact_form(Company $company)
    {
        $user_def_company = User::main_company();
        if ($user_def_company == null)
            abort(404);
        return view('pages.contact-company')
            ->with([
                'target_company' => $company,
                'company' => $user_def_company
            ]);
    }

    public function send_contact(Request $request, Company $company)
    {
        $controller = new CompanyController();
        $result = $controller->send_contact_mail(array_merge($request->all(['receiver', 'message']), ['company' => $company]));
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect()->route('company.detail', $company)->with('result', $result);
        } else {
            $this->alertUpdate($result);
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }


    public function set_default_company(Company $company)
    {
        $controller = new CompanyController();
        $result = $controller->set_default($company);
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect(url()->previous())->with('result', $result);
        } else {
            $this->alertUpdate($result);
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }

    public function show_company_form()
    {
        return view('pages.company-registration');
    }

    public function show_company_update_form(Company $company)
    {
        return view('pages.company-update')->with('company', $company);
    }

    public function register(Request $request)
    {
        $controller = new CompanyController();
        $data = $request->all((new Company())->getFillable());
        $data['services'] = json_encode(array_filter($data['services']));
        $data['products'] = json_encode(array_filter($data['products']));
        $data['partners'] = json_encode(array_filter($data['partners']));
        $data['clients'] = json_encode(array_filter($data['clients']));
        $data['assets'] = json_encode(array_filter($data['assets']));
        $data['realisations'] = json_encode(array_filter($data['realisations']));
        $result = $controller->store($data, $request->all());
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect()->route('company.detail', $result['data'])->with('result', $result);
        } else {
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }

    public function update(Request $request, Company $company)
    {
        $controller = new CompanyController();
        $data = $request->all((new Company())->getFillable());
        $data['services'] = json_encode(array_filter($data['services']));
        $data['products'] = json_encode(array_filter($data['products']));
        $data['partners'] = json_encode(array_filter($data['partners']));
        $data['clients'] = json_encode(array_filter($data['clients']));
        $data['assets'] = json_encode(array_filter($data['assets']));
        $data['realisations'] = json_encode(array_filter($data['realisations']));
        $result = $controller->update($data, $request->all(), $company);
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect()->route('company.detail', $result['data'])->with('result', $result);
        } else {
            return redirect(url()->previous())->withErrors($result['errors']);
        }
    }

    public function payment_board()
    {
        return view('pages.company-payment')->with(['company' => Auth::user()->main_company()]);
    }

    public function payment_save(Request $request)
    {
        $token = time();
        CompanyPackage::create([
            'sysAccount' => config('dohone.start.rH'),
            'paymentToken' => $token,
            'paymentMode' => $token,
            'amount' => config('dohone.start.amount'),
            'devise' => config('dohone.start.rDvs'),
            'company_id' => Auth::user()->main_company()->id
        ]);
        $result = Dohone::init_payment(Auth::user()->phone, config('dohone.start.amount'), Auth::user()->email, $token);
        //return Dohone::verify(config('dohone.start.amount'), $token, $token);
        return $result;
    }

}
