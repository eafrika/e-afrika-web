<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NewsletterController;
use Illuminate\Http\Request;

class NewsletterWebController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.newsletter')->with(['email' => $request->email, 'phone' => $request->phone]);
    }

    public function create(Request $request)
    {
        $controller = new NewsletterController();
        $data = $request->all(['email', 'phone', 'new_company', 'activity_area', 'news', 'types', 'mail', 'whatsapp']);
        $data['mail'] = ($data['mail'] == '' ? false : true);
        $data['whatsapp'] = ($data['whatsapp'] == '' ? false : true);
        $data['news'] = ($data['news'] == '' ? false : true);
        $data['new_company'] = ($data['new_company'] == '' ? false : true);
        $result = $controller->store($data);
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect()->route('welcome')->with('result', $result);
        } else {
            $this->alertUpdate($result);
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }
}
