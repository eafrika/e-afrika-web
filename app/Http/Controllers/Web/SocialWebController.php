<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SocialController;
use App\Response\Builder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SocialWebController extends Controller
{
    public function index(Company $company)
    {
        return view('pages.company-connection')->with('company', $company);
    }

    public function connect(Request $request, Company $company)
    {
        //dd($request->all(), $company);
        $controller = new SocialController();
        $data['type_id'] = $request->type;
        $data['receiver_company_id'] = $company->id;

        if (Auth::user()->main_company() != null)
            $data['sender_company_id'] = Auth::user()->main_company()->id;
        else
            return redirect(url()->previous())->with('error', ['message' => 'Vous n\'avez pas d\'entreprise par défaut']);


        $result = $controller->store($data);
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect()->route('company.detail', $company)->with('result', $result);
        } else {
            return redirect(url()->previous())->with('error', $result);
        }
    }

    public function connection_request(Company $company)
    {
        $controller = new SocialController();
        $result = $controller->index($company);
        if (Builder::check($result['message'], config('code.request.NOT_AUTHORIZED')))
            abort(401);
        return view('pages.company-connection')
            ->with([
                'connexions' => $result['data'],
                'company' => User::main_company(),
                'list' => true
            ]);
    }

    public function accept(Company $company)
    {
        $data['sender_company_id'] = $company->id;
        $data['receiver_company_id'] = Auth::user()->main_company()->id;
        $controller = new SocialController();
        $result = $controller->update($data);
        (new CompanyWebController())->get_conversation($company);
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect(url()->previous())->with('result', $result);
        } else {
            return redirect(url()->previous())->with('error', $result);
        }
    }

    public function cancel(Company $company)
    {
        $data['sender_company_id'] = $company->id;
        $data['receiver_company_id'] = Auth::user()->main_company()->id;
        $controller = new SocialController();
        $result = $controller->destroy($data);
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect(url()->previous())->with('result', $result);
        } else {
            return redirect(url()->previous())->with('error', $result);
        }
    }

    public function board(Company $company)
    {
        return view('pages.company-board')
            ->with([
                'conversations' => $company->conversations(),
                'company' => User::main_company(),
                'conversation' => (new CompanyWebController())->get_conversation($company)
            ]);
    }
}
