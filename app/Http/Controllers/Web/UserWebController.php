<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserWebController extends Controller
{
    public function index()
    {
        return view('pages.user-profile');
    }

    public function update(Request $request)
    {
        $data = collect($request->all())
            ->reject(function ($value, $item) {
                return $value == null;
            });
        if (!Hash::check($data['old_password'], Auth::user()->getAuthPassword())) {
            return view('pages.user-profile')->withErrors(['old_password' => 'Mot de passe incorrect']);
        }
        $controller = new RegisterController();
        $result = $controller->update($data->toArray());
        if ($result['status'] == true) {
            $this->alertSave($result);
            return redirect()->route('my-companies')->with('result', $result);
        } else {
            return redirect(url()->previous())->with(['result' => $result]);
        }
    }
}
