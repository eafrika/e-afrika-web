<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class WelcomeWebController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(time(),Carbon::now()->subDays(40)->timestamp);
        return view('pages.welcome');
    }

    public function register()
    {
        return view('pages.welcome')->with('context', 'registration');
    }

    public function login()
    {
        return view('pages.welcome')->with('context', 'login');
    }

}
