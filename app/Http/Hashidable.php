<?php
/**
 * Created by PhpStorm.
 * User: master
 * Date: 18/12/18
 * Time: 16:56
 */

namespace App\Http;


use Vinkla\Hashids\Facades\Hashids;

trait Hashidable
{
    public function getRouteKey()
    {
        return Hashids::connection(get_called_class())->encode($this->getKey());
    }
}
