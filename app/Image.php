<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['path', 'company_id'];

    public function company(){
        return $this->belongsTo('App\Company');
    }
}
