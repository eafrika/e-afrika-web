<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = ['email', 'phone', 'new_company', 'activity_area', 'news', 'types', 'mail', 'whatsapp'];
}
