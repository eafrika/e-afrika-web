<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class SlackBugNotification extends Notification implements ShouldQueue
{
    use Queueable;
    protected $company;
    protected $isNew;

    /**
     * SlackBugNotification constructor.
     * @param $company
     * @param $isNew
     */
    public function __construct($company, $isNew = true)
    {
        $this->company = $company;
        $this->isNew = $isNew;
    }

    /**
     * Create a new notification instance.
     *
     * @return void
     */


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $url = route('company.detail', $this->company);
        $message = ($this->isNew ? "New company" : "Company update") . ' Edited by : ' . Auth::user()->name;

        return (new SlackMessage)
            ->success()
            ->content($message)
            ->attachment(function ($attachment) use ($url) {
                $attachment->title($this->company->name . ' :+1:', $url)
                    ->fields(json_decode($this->company, true));
            });
    }
}
