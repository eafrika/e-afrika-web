<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = ['receiver_company_id', 'sender_company_id', 'type_id'];


    public function sender()
    {
        return $this->belongsTo("App\Company", "sender_company_id");
    }

    public function receiver()
    {
        return $this->belongsTo("App\Company", "receiver_company_id");
    }

    public function type()
    {
        return $this->belongsTo("App\ConnexionType", "type_id");
    }
}
