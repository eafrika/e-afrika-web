<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubActivityArea extends Model
{
    //protected $table='sous_secteur_activites';

    public function activityArea()
    {
        return $this->belongsTo("App\ActivityArea");
    }
}
