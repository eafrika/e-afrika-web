<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['role', 'name', 'email', 'phone', 'fax', 'company_id'];
}
