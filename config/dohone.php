<?php

return [
    /*
     * Start transaction
     */
    'start' => [
        'rH' => '00000000',
        'rDvs' => 'XAF',
        'source' => env('APP_NAME'),
        'logo' => 'assets/images/logo.png',
        "endPage" => "https://www.e-afrika.fr",
        "notifyPage" => "https://www.e-afrika.fr",
        "cancelPage" => "https://www.e-afrika.fr",
        "cmd" => "start",
        'rLocale' => 'fr',
        'amount' => 1,
    ]
];
