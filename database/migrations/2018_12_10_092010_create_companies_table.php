<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            //base information
            $table->string('name');
            $table->string('logo')->default('bg-logo.png');
            $table->string('cropped_logo')->nullable();
            $table->string('size');
            $table->string('town');
            $table->string('phone');
            $table->string('email');
            $table->longText('description')->nullable();
            $table->string('zone');
            $table->string('agencies');
            $table->string('initials')->nullable();
            $table->string('slogan')->nullable();
            $table->string('code_postal')->nullable();
            $table->string('location');
            $table->longText('keywords')->nullable();
            $table->longText('more_information')->nullable();
            $table->date('founded_at')->nullable();
            //social network
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();
            $table->string('linkedin')->nullable();
            //services and products
            $table->longText('services');
            $table->longText('products')->nullable();
            //partners and clients
            $table->longText('partners')->nullable();
            $table->longText('clients')->nullable();
            //Economic Information
            $table->longText('business_turnover');
            $table->string('market')->nullable();
            //Assets and Achievements
            $table->longText('realisations')->nullable();
            $table->string('assets')->nullable();
            //settings
            $table->longText('views')->nullable();
            $table->enum('priority', [0, 1, 2, 3, 4, 5])->default(1);
            //foreign key
            $table->integer('legal_status_id')->unsigned()->index();//legal status
            $table->foreign('legal_status_id')
                ->references('id')
                ->on('legal_statuses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('country_id')->unsigned()->index();//country
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedInteger('main_activity_id')->index();
            $table->foreign('main_activity_id')
                ->references('id')
                ->on('activity_areas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
