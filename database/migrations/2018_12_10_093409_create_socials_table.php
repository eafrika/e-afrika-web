<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->unsignedBigInteger('sender_company_id')->index();
            $table->foreign('sender_company_id')->references('id')->on('companies');
            $table->unsignedBigInteger('receiver_company_id')->index();
            $table->foreign('receiver_company_id')->references('id')->on('companies');
            $table->primary(['sender_company_id', 'receiver_company_id']);

            $table->integer('type_id');
            $table->foreign('type_id')->references('id')->on('connexion_types');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}
