<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubActivityAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_activity_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->longText('keywords');
            $table->longText('competitors');
            $table->longText('clients');
            $table->longText('providers');
            $table->longText('partners');

            $table->unsignedInteger('activity_area_id');
            $table->foreign('activity_area_id')->references('id')->on('activity_areas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_activity_areas');
    }
}
