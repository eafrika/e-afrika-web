<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanySubActivityAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_sub_activity_areas', function (Blueprint $table) {
            $table->unsignedInteger('sub_activity_area_id');
            $table->foreign('sub_activity_area_id')
                ->references('id')
                ->on('sub_activity_areas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('company_id')->index();
            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->primary(['sub_activity_area_id', 'company_id'], 'fk_primary_company_sub_act');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_sub_activity_areas');
    }
}
