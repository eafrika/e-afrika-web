<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->nullable(true);
            $table->string('phone')->nullable(true);
            $table->boolean('new_company')->default(false);
            $table->integer('activity_area')->nullable(true);
            $table->boolean('news')->default(false);
            $table->integer('types')->nullable(true);
            $table->boolean('updates')->default(false);
            $table->boolean('mail')->default(false);
            $table->boolean('whatsapp')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
