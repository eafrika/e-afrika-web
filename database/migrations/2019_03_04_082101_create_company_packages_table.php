<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sysAccount');
            $table->string('paymentToken');
            $table->string('paymentMode');
            $table->double('amount');
            $table->enum('devise', ['XAF', 'EURO', 'USD']);
            $table->timestamp('verify_at')->nullable();
            $table->timestamp('buy_at')->useCurrent();
            $table->unsignedBigInteger('company_id');

            $table->foreign('company_id')->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_packages');
    }
}
