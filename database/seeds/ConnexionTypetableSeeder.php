<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConnexionTypetableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('connexion_types')->insert([
            'label' => "Comme Client ou Potentiel Client",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('connexion_types')->insert([
            'label' => "Comme Fournisseur ou Potentiel Fournisseur",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('connexion_types')->insert([
            'label' => "Comme Partenaire ou Potentiel Partenaire",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('connexion_types')->insert([
            'label' => "Comme Investisseurs ou Potentiels Investisseurs",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('connexion_types')->insert([
            'label' => "Comme Concurents ou Potentiels Concurents",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
