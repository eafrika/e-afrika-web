<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'id' => '1',
            'label' => "Cameroun",
            'code' => 'CM',
            'phone_indicator' => '(+237)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '2',
            'label' => "Congo",
            'code' => 'CG',
            'phone_indicator' => '(+242)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '3',
            'label' => "Côte d'Ivoire",
            'code' => 'CI',
            'phone_indicator' => '(+225)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '4',
            'label' => "Gabon",
            'code' => 'GA',
            'phone_indicator' => '(+241)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '5',
            'label' => "Tchad",
            'code' => 'TD',
            'phone_indicator' => '(+235)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '6',
            'label' => "Guinée Equatoriale",
            'code' => 'GN',
            'phone_indicator' => '(+226)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '7',
            'label' => "République de Centre Afrique",
            'code' => 'CF',
            'phone_indicator' => '(+236)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '8',
            'label' => "Afghanistan",
            'code' => 'AF',
            'phone_indicator' => '(+93)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '9',
            'label' => "Afrique du Sud",
            'code' => 'ZA',
            'phone_indicator' => '(+27)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '10',
            'label' => "Albanie",
            'code' => 'AL',
            'phone_indicator' => '(+355)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '11',
            'label' => "Algérie",
            'code' => 'DZ',
            'phone_indicator' => '(+213)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '12',
            'label' => "Allemagne",
            'code' => 'DE',
            'phone_indicator' => '(+49)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '13',
            'label' => "Andorre",
            'code' => 'AD',
            'phone_indicator' => '(+376)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '14',
            'label' => "Angola",
            'code' => 'AO',
            'phone_indicator' => '(+244)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '15',
            'label' => "Anguilla",
            'code' => 'AI',
            'phone_indicator' => '(+1264)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '16',
            'label' => "Arabie Saoudite",
            'code' => 'SA',
            'phone_indicator' => '(+966)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '17',
            'label' => "Argentine",
            'code' => 'AR',
            'phone_indicator' => '(+54)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '18',
            'label' => "Arménie",
            'code' => 'AM',
            'phone_indicator' => '(+374)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '19',
            'label' => "Aruba",
            'code' => 'AW',
            'phone_indicator' => '(+297)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '20',
            'label' => "Australie",
            'code' => 'AU',
            'phone_indicator' => '(+61)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '21',
            'label' => "Autriche",
            'code' => 'AT',
            'phone_indicator' => '(+43)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '22',
            'label' => "Azerbaïdjan",
            'code' => 'AZ',
            'phone_indicator' => '(+994)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '23',
            'label' => "Bahamas",
            'code' => 'BS',
            'phone_indicator' => '(+1242)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '24',
            'label' => "Bangladesh",
            'code' => 'BD',
            'phone_indicator' => '(+880)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '25',
            'label' => "Barbade",
            'code' => 'BB',
            'phone_indicator' => '(+1246)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '26',
            'label' => "Belgique",
            'code' => 'BE',
            'phone_indicator' => '(+32)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '27',
            'label' => "Belize",
            'code' => 'BZ',
            'phone_indicator' => '(+501)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '28',
            'label' => "Bermudes",
            'code' => 'BM',
            'phone_indicator' => '(+1441)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '29',
            'label' => "Bhoutan",
            'code' => 'BT',
            'phone_indicator' => '(+975)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '30',
            'label' => "Bolivie",
            'code' => 'BO',
            'phone_indicator' => '(+591)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '31',
            'label' => "Botswana",
            'code' => 'BW',
            'phone_indicator' => '(+267)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '32',
            'label' => "Brésil",
            'code' => 'BR',
            'phone_indicator' => '(+55)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '33',
            'label' => "Bulgarie",
            'code' => 'BG',
            'phone_indicator' => '(+359)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '34',
            'label' => "Burkina Faso",
            'code' => 'BF',
            'phone_indicator' => '(+226)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '35',
            'label' => "Burundi",
            'code' => 'BI',
            'phone_indicator' => '(+257)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '36',
            'label' => "Cambodge",
            'code' => 'KH',
            'phone_indicator' => '(+855)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '37',
            'label' => "Canada",
            'code' => 'CA',
            'phone_indicator' => '(+1)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '38',
            'label' => "Chili",
            'code' => 'CL',
            'phone_indicator' => '(+56)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '39',
            'label' => "Chine",
            'code' => 'CN',
            'phone_indicator' => '(+86)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '40',
            'label' => "Chypre",
            'code' => 'CY',
            'phone_indicator' => '(+357)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '41',
            'label' => "Colombie",
            'code' => 'CO',
            'phone_indicator' => '(+57)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '42',
            'label' => "Comores",
            'code' => 'KM',
            'phone_indicator' => '(+269)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '43',
            'label' => "Costa Rica",
            'code' => 'CR',
            'phone_indicator' => '(+506)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '44',
            'label' => "Croatie",
            'code' => 'HR',
            'phone_indicator' => '(+385)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '45',
            'label' => "Cuba",
            'code' => 'CU',
            'phone_indicator' => '(+53)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '46',
            'label' => "Danemark",
            'code' => 'DK',
            'phone_indicator' => '(+45)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '47',
            'label' => "Djibouti",
            'code' => 'DJ',
            'phone_indicator' => '(+253)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '48',
            'label' => "Dominique",
            'code' => 'DM',
            'phone_indicator' => '(+1767)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '49',
            'label' => "Egypte",
            'code' => 'EG',
            'phone_indicator' => '(+20)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '50',
            'label' => "El Salvador",
            'code' => 'SV',
            'phone_indicator' => '(+503)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '51',
            'label' => "Espagne",
            'code' => 'ES',
            'phone_indicator' => '(+34)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '52',
            'label' => "Estonie",
            'code' => 'EE',
            'phone_indicator' => '(+372)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '53',
            'label' => "États-Unis d'Amérique",
            'code' => 'US',
            'phone_indicator' => '(+1)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '54',
            'label' => "Finlande",
            'code' => 'FI',
            'phone_indicator' => '(+358)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '55',
            'label' => "France",
            'code' => 'FR',
            'phone_indicator' => '(+33)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '56',
            'label' => "Gambie",
            'code' => 'GM',
            'phone_indicator' => '(+220)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '57',
            'label' => "Ghana",
            'code' => 'GH',
            'phone_indicator' => '(+233)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '58',
            'label' => "Gibraltar",
            'code' => 'GI',
            'phone_indicator' => '(+350)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '59',
            'label' => "Grèce",
            'code' => 'GR',
            'phone_indicator' => '(+30)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '60',
            'label' => "Grenade",
            'code' => 'GD',
            'phone_indicator' => '(+1473)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '61',
            'label' => "Groenland",
            'code' => 'GL',
            'phone_indicator' => '(+299)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '62',
            'label' => "Guadeloupe",
            'code' => 'GP',
            'phone_indicator' => '(+(0)590)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '63',
            'label' => "Guatemala",
            'code' => 'GT',
            'phone_indicator' => '(+502)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '64',
            'label' => "Guinée",
            'code' => 'GN',
            'phone_indicator' => '(+224)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '65',
            'label' => "Guyana",
            'code' => 'GY',
            'phone_indicator' => '(+592)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '66',
            'label' => "Guyane Française",
            'code' => 'GF',
            'phone_indicator' => '(+(0)594)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '67',
            'label' => "Haïti",
            'code' => 'HT',
            'phone_indicator' => '(+509)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '68',
            'label' => "Honduras",
            'code' => 'HN',
            'phone_indicator' => '(+504)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '69',
            'label' => "Hongrie",
            'code' => 'HU',
            'phone_indicator' => '(+36)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '70',
            'label' => "Inde",
            'code' => 'IN',
            'phone_indicator' => '(+91)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '71',
            'label' => "Indonésie",
            'code' => 'ID',
            'phone_indicator' => '(+62)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '72',
            'label' => "Irlande",
            'code' => 'IE',
            'phone_indicator' => '(+353)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '73',
            'label' => "Islande",
            'code' => 'IS',
            'phone_indicator' => '(+354)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '74',
            'label' => "Israël",
            'code' => 'IL',
            'phone_indicator' => '(+972)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '75',
            'label' => "Italie",
            'code' => 'IT',
            'phone_indicator' => '(+39)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '76',
            'label' => "Jamaïque",
            'code' => 'JM',
            'phone_indicator' => '(+1876)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '77',
            'label' => "Japon",
            'code' => 'JP',
            'phone_indicator' => '(+81)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '78',
            'label' => "Jordanie",
            'code' => 'JO',
            'phone_indicator' => '(+962)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '79',
            'label' => "Kazakhstan",
            'code' => 'KZ',
            'phone_indicator' => '(+7)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '80',
            'label' => "Kenya",
            'code' => 'KE',
            'phone_indicator' => '(+254)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '81',
            'label' => "Kirghizistan",
            'code' => 'KG',
            'phone_indicator' => '(+996)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '82',
            'label' => "Kiribati",
            'code' => 'KI',
            'phone_indicator' => '(+686)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '83',
            'label' => "Lesotho",
            'code' => 'LS',
            'phone_indicator' => '(+266)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '84',
            'label' => "Lettonie",
            'code' => 'LV',
            'phone_indicator' => '(+371)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '85',
            'label' => "Liban",
            'code' => 'LB',
            'phone_indicator' => '(+961)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '86',
            'label' => "Liechtenstein",
            'code' => 'LI',
            'phone_indicator' => '(+423)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '87',
            'label' => "Lituanie",
            'code' => 'LT',
            'phone_indicator' => '(+370)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '88',
            'label' => "Luxembourg",
            'code' => 'LU',
            'phone_indicator' => '(+352)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '89',
            'label' => "Macao",
            'code' => 'MO',
            'phone_indicator' => '(+853)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '90',
            'label' => "Madagascar",
            'code' => 'MG',
            'phone_indicator' => '(+261)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '91',
            'label' => "Malaisie",
            'code' => 'MY',
            'phone_indicator' => '(+60)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '92',
            'label' => "Malawi",
            'code' => 'MW',
            'phone_indicator' => '(+265)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '93',
            'label' => "Maldives",
            'code' => 'MV',
            'phone_indicator' => '(+960)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '94',
            'label' => "Mali",
            'code' => 'ML',
            'phone_indicator' => '(+223)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '95',
            'label' => "Malte",
            'code' => 'MT',
            'phone_indicator' => '(+356)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '96',
            'label' => "Maroc",
            'code' => 'MA',
            'phone_indicator' => '(+212)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '97',
            'label' => "Martinique",
            'code' => 'MQ',
            'phone_indicator' => '(+(0)596)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '98',
            'label' => "Mauritanie",
            'code' => 'MR',
            'phone_indicator' => '(+222)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '99',
            'label' => "Mayotte",
            'code' => 'YT',
            'phone_indicator' => '(+269)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '100',
            'label' => "Mexique",
            'code' => 'MX',
            'phone_indicator' => '(+52)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '101',
            'label' => "Mexique",
            'code' => 'MX',
            'phone_indicator' => '(+52)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '102',
            'label' => "Monaco",
            'code' => 'MC',
            'phone_indicator' => '(+377)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '103',
            'label' => "Mongolie",
            'code' => 'MN',
            'phone_indicator' => '(+976)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '104',
            'label' => "Montserrat",
            'code' => 'MS',
            'phone_indicator' => '(+1664)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '105',
            'label' => "Mozambique",
            'code' => 'MZ',
            'phone_indicator' => '(+258)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '106',
            'label' => "Namibie",
            'code' => 'NA',
            'phone_indicator' => '(+264)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '107',
            'label' => "Nauru",
            'code' => 'NR',
            'phone_indicator' => '(+674)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '108',
            'label' => "Népal",
            'code' => 'NP',
            'phone_indicator' => '(+977)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '109',
            'label' => "Nicaragua",
            'code' => 'NI',
            'phone_indicator' => '(+505)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '110',
            'label' => "Niger",
            'code' => 'NE',
            'phone_indicator' => '(+227)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '111',
            'label' => "Norvège",
            'code' => 'NO',
            'phone_indicator' => '(+47)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '112',
            'label' => "Oman",
            'code' => 'OM',
            'phone_indicator' => '(+968)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '113',
            'label' => "Ouganda",
            'code' => 'UG',
            'phone_indicator' => '(+256)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '114',
            'label' => "Pakistan",
            'code' => 'PK',
            'phone_indicator' => '(+92)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '115',
            'label' => "Panama",
            'code' => 'PA',
            'phone_indicator' => '(+507)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '116',
            'label' => "Paraguay",
            'code' => 'PY',
            'phone_indicator' => '(+595)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '117',
            'label' => "Pays-Bas",
            'code' => 'NL',
            'phone_indicator' => '(+31)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '118',
            'label' => "Pérou",
            'code' => 'PE',
            'phone_indicator' => '(+51)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '119',
            'label' => "Philippines",
            'code' => 'PH',
            'phone_indicator' => '(+63)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '120',
            'label' => "Pologne",
            'code' => 'PL',
            'phone_indicator' => '(+48)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '121',
            'label' => "Polynésie Française",
            'code' => 'PF',
            'phone_indicator' => '(+689)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '122',
            'label' => "Porto Rico",
            'code' => 'PR',
            'phone_indicator' => '(+1787)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '123',
            'label' => "Portugal",
            'code' => 'PT',
            'phone_indicator' => '(+351)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '124',
            'label' => "Qatar",
            'code' => 'QA',
            'phone_indicator' => '(+974)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '125',
            'label' => "République Démocratique du Congo",
            'code' => 'CD',
            'phone_indicator' => '(+243)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '126',
            'label' => "République Dominicaine",
            'code' => 'DO',
            'phone_indicator' => '(+1809)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '127',
            'label' => "Réunion",
            'code' => 'RE',
            'phone_indicator' => '(+(0)262)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '128',
            'label' => "Roumanie",
            'code' => 'RO',
            'phone_indicator' => '(+40)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '129',
            'label' => "Royaume Unis",
            'code' => 'GB',
            'phone_indicator' => '(+44)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '130',
            'label' => "Rwanda",
            'code' => 'RW',
            'phone_indicator' => '(+250)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '131',
            'label' => "Sénégal",
            'code' => 'SN',
            'phone_indicator' => '(+221)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '132',
            'label' => "Seychelles",
            'code' => 'SC',
            'phone_indicator' => '(+248)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '133',
            'label' => "Sierra Leone",
            'code' => 'SL',
            'phone_indicator' => '(+232)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '134',
            'label' => "Singapour",
            'code' => 'SG',
            'phone_indicator' => '(+65)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '135',
            'label' => "Slovaquie",
            'code' => 'SK',
            'phone_indicator' => '(+421)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '136',
            'label' => "Slovénie",
            'code' => 'SI',
            'phone_indicator' => '(+386)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '137',
            'label' => "Somalie",
            'code' => 'SO',
            'phone_indicator' => '(+252)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '138',
            'label' => "Soudan",
            'code' => 'SD',
            'phone_indicator' => '(+249)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '139',
            'label' => "Sri Lanka",
            'code' => 'LK',
            'phone_indicator' => '(+94)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '140',
            'label' => "Suède",
            'code' => 'SE',
            'phone_indicator' => '(+46)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '141',
            'label' => "Suisse",
            'code' => 'CH',
            'phone_indicator' => '(+41)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '142',
            'label' => "Swaziland",
            'code' => 'SZ',
            'phone_indicator' => '(+268)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '143',
            'label' => "Tadjikistan",
            'code' => 'TJ',
            'phone_indicator' => '(+992)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '144',
            'label' => "Taïwan",
            'code' => 'TW',
            'phone_indicator' => '(+886)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '145',
            'label' => "Togo",
            'code' => 'TG',
            'phone_indicator' => '(+228)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '146',
            'label' => "Tonga",
            'code' => 'TO',
            'phone_indicator' => '(+676)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '147',
            'label' => "Tunisie",
            'code' => 'TN',
            'phone_indicator' => '(+216)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '148',
            'label' => "Turquie",
            'code' => 'TR',
            'phone_indicator' => '(+90)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '149',
            'label' => "Turquie",
            'code' => 'TR',
            'phone_indicator' => '(+90)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '150',
            'label' => "Tuvalu",
            'code' => 'TV',
            'phone_indicator' => '(+688)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '151',
            'label' => "Ukraine",
            'code' => 'UA',
            'phone_indicator' => '(+380)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '152',
            'label' => "Uruguay",
            'code' => 'UY',
            'phone_indicator' => '(+598)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '153',
            'label' => "Vanuatu",
            'code' => 'VU',
            'phone_indicator' => '(+678)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '154',
            'label' => "Venezuela",
            'code' => 'VE',
            'phone_indicator' => '(+58)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '155',
            'label' => "Wallis et Futuna",
            'code' => 'WF',
            'phone_indicator' => '(+681)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '156',
            'label' => "Zambie",
            'code' => 'ZM',
            'phone_indicator' => '(+260)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('countries')->insert([
            'id' => '157',
            'label' => "Zimbabwe",
            'code' => 'ZW',
            'phone_indicator' => '(+263)',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

    }
}
