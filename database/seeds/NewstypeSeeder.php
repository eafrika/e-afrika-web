<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewstypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('newstypes')->delete();

        \App\Newstype::create([
            'label' => 'Economie Africaines',
            'keywords' => null,
        ]);

        \App\Newstype::create([
            'label' => 'Economie Mondiale',
            'keywords' => null,
        ]);

        \App\Newstype::create([
            'label' => 'Tendances',
            'keywords' => null,
        ]);

        \App\Newstype::create([
            'label' => 'Nouvelles Idées',
            'keywords' => null,
        ]);
    }
}
