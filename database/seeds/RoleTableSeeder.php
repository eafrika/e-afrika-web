<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        \App\Role::create([
            'label' => 'Root',
            'rank' => 1000,
        ]);

        \App\Role::create([
            'label' => 'Admin',
            'rank' => 100,
        ]);

        \App\Role::create([
            'label' => 'Brand',
            'rank' => 2,
        ]);

        \App\Role::create([
            'label' => 'Client',
            'rank' => 1,
        ]);
    }
}
