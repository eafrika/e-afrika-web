#!/bin/bash
path=e-afrika-fake;
cd $path
shopt -s extglob
mv index.php public
mv public public_html
mkdir project
mv !(public_html|project) project
mv .env.production project/.env
rm -f -r .*
