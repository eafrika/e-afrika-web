$(document).ready(function () {
    var $uploadCrop;

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 300,
            height: 300
        },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    $("#logo-editor").click(function (event) {
        event.preventDefault();
        $uploadCrop.croppie('bind', {
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            },
            url: $('#logo-origin').attr('data-content'),
        });
        $(".logo-editor").fadeToggle("fast");
    });


    $("#close-logo-editor").click(function () {
        $(".logo-editor").fadeToggle("fast");
    });

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }


    $('#upload').on('change', function () {
        readFile(this);
    });

    $('#save-logo').on('click', function (ev) {
        event.preventDefault();
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
            $('#base64Image').val(resp);
            //console.log(resp);
            $('#logo-form').submit();
        });
    });

});
