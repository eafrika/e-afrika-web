$(document).ready(function () {

    var config = {
        type: 'line',
        data: {
            labels: ["6 derniers mois", "3 derniers mois", "mois en cours"],
            datasets: [{
                label: "Usine APRAC",
                data: "", // [300, 150, 400],
                fill: false,
                borderColor: "rgb(6,119,144)",
                backgroundColor: "rgb(6,119,144)",
            }, {
                label: "Toutes Entreprises",
                data: "", //[1000, 500, 1000],
                fill: false,
                borderColor: "rgb(0,255,255)",
                backgroundColor: "rgb(0,255,255)"
            }, {
                label: "Entreprises du même secteurs",
                data: "", //[500, 250, 700],
                fill: false,
                borderColor: "rgb(3,34,76)",
                backgroundColor: "rgb(3,34,76)"
            }]
        },
        options: {
            responsive: true,
        }
    };

    var data1 = {
        labels: ["Usine APRAC", "Toutes les Entreprises"],
        datasets: [{
            label: "TeamA Score",
            data: "", //[500, 1500],
            backgroundColor: [
                "#067790",
                "#00FFFF",

            ],
            borderColor: [
                "#067790",
                "#00FFFF",
            ],
            borderWidth: [0, 0]
        }]
    };

    //pie chart data
    var data2 = {
        labels: ["Usine APRAC", "Toutes les Entreprises"],
        datasets: [{
            label: "TeamB Score",
            data: "", //[800, 1500],
            backgroundColor: [
                "#067790",
                "#00FFFF",
            ],
            borderColor: [
                "#067790",
                "#00FFFF",
            ],
            borderWidth: [0, 0]
        }]
    };

    var options1 = {
        responsive: true,
        title: {
            display: true,
            position: "bottom",
            text: "6 derniers mois",
            fontSize: 10,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 10
            }
        }
    };
    var options2 = {
        responsive: true,
        title: {
            display: true,
            position: "bottom",
            text: "3 derniers mois",
            fontSize: 10,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 10
            }
        }
    };


    $.get("../visibility",
        function (data) {

            $('#note').text('Note Moyenne: ' + Math.ceil(data['render']['stats']["noteMoyenne"]));
            $('#vues').text('Nombre de Vues: ' + data['render']['stats']["visibilite"]);
            $('#vuesMois').text('Nombre de Vues ce Mois: ' + data['render']['stats']["visibiliteMois"]);
            $('#totalSecteur').text(data['render']["totalSecteur"]);

            //console.log(configNombre de Vuess ce Mois: [+'data']['datasets'][0]['label'] );
            config['data']['datasets'][0]['label'] = data['render']['nom'];
            config['data']['datasets'][0]['data'] = [
                data['render']['entreprise'][6],
                data['render']['entreprise'][3],
                data['render']['entreprise']['current']
            ];

            config['data']['datasets'][1]['data'] = [
                data['render']['touts'][6],
                data['render']['touts'][3],
                data['render']['touts']['current']
            ];

            config['data']['datasets'][2]['data'] = [
                data['render']['secteurs']['6'],
                data['render']['secteurs']['3'],
                data['render']['secteurs']['current']
            ];

            data1['labels'][0] = data['render']['nom'];
            data2['labels'][0] = data['render']['nom'];
            data1['datasets'][0]['data'] = [data['render']['entreprise']['current'], data['render']['touts']['current']];
            data2['datasets'][0]['data'] = [data['render']['entreprise']['current'], data['render']['touts']['current']];

            /* var owner    = [
                 data.render.entreprises['6'],data.render.entreprises['3'],data.render.entreprises['current']
             ];
             var touts    = [data.render.touts['6'],data.render.touts['3'],data.render.touts['current']];
             var secteurs = [data.render.secteurs['6'],data.render.secteurs['3'],data.render.secteurs['current']];

             console.log(config['data'])
            /* config->data->datasets[0]['data'] = owner;
             config->data->datasets[1]['data'] = touts;
             config->data->datasets[2]['data'] = secteurs;*/

            var myChart;
            var ctx1 = $("#pie-chartcanvas-1");
            var ctx2 = $("#pie-chartcanvas-2");

            var chart1 = new Chart(ctx1, {
                type: "pie",
                data: data1,
                options: options1

            });

            //create Chart class object
            var chart2 = new Chart(ctx2, {
                type: "pie",
                data: data2,
                options: options2

            });

            $("#line").click(function () {
                change('line');
            });

            $("#bar").click(function () {
                change('bar');
            });

            function change(newType) {
                var ctx = document.getElementById("canvas").getContext("2d");

                // Remove the old chart and all its event handles
                if (myChart) {
                    myChart.destroy();
                }

                // Chart.js modifies the object you pass in. Pass a copy of the object so we can use the original object later
                var temp = jQuery.extend(true, {}, config);
                temp.type = newType;
                myChart = new Chart(ctx, temp);

            }

            change('bar');
        })
});
