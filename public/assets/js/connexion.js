class Connexion {

    constructor() {
        this.config = JSON.parse($("#config").text());
    }

    find(param) {

        let $this = this;

        $.ajax({
            url: $this.config["api.find"],
            type: "POST",
            dataType: "JSON",
            data: {
                word: param.type,
                token: $this.config["token"]
            }

        }).done(function (data) {
            $(param.id).text(data.number);
        }).fail(function (jqXHR, textStatus) {
            $(param.id).text("undeternined");
        });

    }

}
