$(document).ready(function () {
    $('img.image-entreprise').hover(
        function () {
            $(this).attr('src', window.location.protocol + '//' + window.location.host + '/assets/images/icon-entreprise-2.png');
        },
        function () {
            $(this).attr('src', window.location.protocol + '//' + window.location.host + '/assets/images/icon-entreprise.png');
        }
    );

    $('img.image-reseau').hover(
        function () {
            $(this).attr('src', window.location.protocol + '//' + window.location.host + '/assets/images/reseau-2.png');
        },
        function () {
            $(this).attr('src', window.location.protocol + '//' + window.location.host + '/assets/images/reseau.png');
        }
    );

    $('img.image-e-afrika').hover(
        function () {
            $(this).attr('src', window.location.protocol + '//' + window.location.host + '/assets/images/icon-e-afrika-2.png');
        },
        function () {
            $(this).attr('src', window.location.protocol + '//' + window.location.host + '/assets/images/icon-e-afrika.png');
        }
    );
});


