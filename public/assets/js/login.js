$(document).ready(function () {
    $("#login-page").click(function (event) {
        event.preventDefault();
        $(".login").fadeToggle("fast");
    });

    $("#to-register-page").click(function (event) {
        event.preventDefault();
        $(".login").fadeToggle("fast", function () {
            $(".registration").fadeToggle("fast");
        });

    });

    $("#close-login").click(function () {
        $(".login").fadeToggle("fast");
    });

    $(document).click(function (e) {
        if ($(e.target).is('#login-form')) {
            $('#login-form').fadeToggle(500);
        }
    });


    setTimeout(function () {
        $(".alert").hide('fade');
    }, 5000);

    $("#loginForm").submit(function (e) {

        e.preventDefault();
        $('input+small').text('');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("#loginForm input[name='_token']").val()
            }
        });

        var btnSubmit = document.getElementById("btn-submit");
        btnSubmit.disable = true;

        var data = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: data,
            dataType: "json",
            cache: false,
            processData: false, // Don't process the files
            contentType: false,
            error: function (data) {

                $.each(data.responseJSON, function (key, value) {
                    if (key === 'errors')
                        $.each(value, function (inputName, inputValue) {
                            var input = '#loginForm input[name=' + inputName + ']';
                            //toastr.error(inputValue);
                            $(input + '+small').text(inputValue);
                        });
                });
                btnSubmit.disabled = false;
            },
        }).done(function (data) {
            if (data.status && data.data !== undefined) {
                window.location = data.data;
            } else {
                window.location = '/home';
            }
        });


        return false;
    });
});
