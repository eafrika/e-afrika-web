// This is the function.
String.prototype.format = function (args) {
    var str = this;
    return str.replace(String.prototype.format.regex, function (item) {
        var intVal = parseInt(item.substring(1, item.length - 1));
        var replace;
        if (intVal >= 0) {
            replace = args[intVal];
        } else if (intVal === -1) {
            replace = "{";
        } else if (intVal === -2) {
            replace = "}";
        } else {
            replace = "";
        }
        return replace;
    });
};
String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

$('#search').autocomplete({
    minChars: 2,
    source: function (term, response) {
        let key = {
            k: $('#search').val()
        };
        $.ajax({
            url: 'company/search',
            method: 'GET',
            dataType: 'json',
            data: key,
            error: function (data) {
            },
            success: function (data) {
                let total = data.companies.total;
                if (total > 0) {
                    let count = "<span>{0}</span>résultats";
                    document.getElementById("result-count").innerHTML = count.format([total]);
                } else {
                    $('#result-count').empty();
                }
                response($.map(data.companies.data, function (obj) {
                    let url = "<a href='{0}'>{0}</a>";
                    return obj.name;
                }));
            }
        })
    }
});

function showResult(str) {
    if (str.length === 0) {
        $('#result-count').empty();
    }
}
