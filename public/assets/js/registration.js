$(document).ready(function () {
    $("#registration-page").click(function (event) {
        event.preventDefault();
        $(".registration").fadeToggle("normal");
    });

    $("#to-login-page").click(function (event) {
        event.preventDefault();
        $(".registration").fadeToggle("fast", function () {
            $(".login").fadeToggle("fast");
        });

    });

    $("#close-registration").click(function () {
        $(".registration").fadeToggle("slow");
    });


    $(document).click(function (e) {
        if ($(e.target).is('#registration-form')) {
            $('#registration-form').fadeToggle(500);
        }
    });


    setTimeout(function () {
        $(".alert").hide('fade');
    }, 5000);

    $("#registrationForm").submit(function (e) {

        e.preventDefault();
        $('input+small').text('');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("#registrationForm input[name='_token']").val()
            }
        });

        let btnSubmit = document.getElementById("btn-registration");
        btnSubmit.disable = true;

        let data = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: data,
            dataType: "json",
            cache: false,
            processData: false, // Don't process the files
            contentType: false,
            error: function (data) {
                console.error(data);
                if (data.status === 200)
                    window.location.reload();
                $.each(data.responseJSON, function (key, value) {
                    if (key === 'errors')
                        $.each(value, function (inputName, inputValue) {
                            var input = '#registrationForm input[name=' + inputName + ']';
                            //toastr.error(inputValue);
                            $(input + '+small').text(inputValue);
                            let select = '#registrationForm select[name=' + inputName + ']';
                            $(select + '+small').text(inputValue);
                        });
                });
                btnSubmit.disabled = false;
            },
        }).done(function (data) {
            window.location.reload();
        });
        return false;
    });
});
