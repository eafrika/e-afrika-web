class Suggestion {

    constructor() {
        this.config = JSON.parse($("#config").text());
        this.request = this.config["request"];

        this.binding();
    }

    binding() {
        var $this = this;

        $("#preloader-wrapper-find").css("display", "block");
        if ($("#suggestion option:selected").text() != "")
            $("#suggestion").on("change", this.change.bind($this));
    }

    change() {
        $("#preloader-wrapper-find").css("display", "block");

        if ($("#suggestion option:selected").text() != "") {
            this.find($("#suggestion").val());
            $("#result").html("");
            $("#title-render").html(
                `<span class = "glyphicon glyphicon-search form-control-feedback"> </span> Recherche en cours .... <strong>${$(
                    "#suggestion option:selected"
                ).text()}</strong>`
            );
        }
    }

    remove() {
        console.log($("#preloader-wrapper-find"));
        $("#preloader-wrapper-find").toggle();
        $("body").removeClass("preloader-site");
    }

    find(param) {
        let $this = this;
        if (param != "")
            $.ajax({
                url: this.config["api.find"][this.request],
                type: "GET",
                dataType: "JSON",
                data: {
                    word: param,
                    token: this.config["token"]
                }
            })
                .done(function (data) {
                    let render = data.entreprises;
                    let out = "";
                    render.forEach(entreprise => {
                        let items = `
            <tr>
              <td width="10%">
                <a href = "${$this.config["api.route"]}${entreprise.id}"> <img src="${$this.config["path.img"]}/${entreprise.id}/${entreprise.logo}" width="160px" height="85px"/></a>
                  <div>
                    <p> 
                      <a href = "${$this.config["api.route"]}${entreprise.id}"> 
                      <strong> ${entreprise.nom} </strong></a> 
                    </p>
                    <p>${entreprise.activites_principales}</p>
                    <p>${entreprise.ville}</p>
                    <p><a href = "${$this.config["api.route"]}${entreprise.id}"> <i class="fa fa-eye"></i> En savoir plus ...</a></p>
                    </div>
                    </td>
            </tr>`;
                        out = out + items;
                    });
                    $("#result").html("");

                    if (data.count)
                        $("#title-render").html(
                            `<span class = "glyphicon glyphicon-search form-control-feedback"> </span> Environ  <strong>${data.count}</strong> résultats trouvé(s) pour <strong>${$("#suggestion option:selected"
                            ).text()}</strong>`
                        );
                    else
                        $("#title-render").html(
                            `<span class = "glyphicon glyphicon-search form-control-feedback"> </span> Aucun résultat <strong>${$(
                                "#suggestion option:selected"
                            ).text()}</strong>`
                        );

                    $("#preloader-wrapper-find").css("display", "none");
                    $("#result").append(out);
                })
                .fail(function (jqXHR, textStatus) {
                    $("#result").html("");
                    $("#preloader-wrapper-find").css("display", "none");
                    $("#title-render").html(
                        `<span class = "glyphicon glyphicon-search form-control-feedback"> </span> Aucun résultat pour <strong>${$(
                            "#suggestion option:selected"
                        ).text()}</strong>`
                    );
                    console.log("Request failed: " + textStatus);
                });
        else
            $("#title-render").html(`<span class = "glyphicon glyphicon-search form-control-feedback"> </span> Aucun résultat pour ce sous - secteur`);
    }
}
