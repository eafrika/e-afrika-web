DB::table('activity_areas')->insert([
'id' => '1',
'label' => 'Alimentation',
'keywords' => "alimentaire, nutrition, consommation, nourriture, végétal, diététique, céréale, agriculture, bétail, viande, mal nutrition, carence, vitamine, organisme, santé, aliment, FAO, mais, lait, légume, nutritionnel, végétarien, glucide, amidon, approvisionnement, alimenter, gastronomie, appétit, vitamine, diète, sport, agroalimentaire, huile, cholestérol, pois, poids, agricole, intestin, sucre, Commerce et distribution, Alimentation",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '2',
'label' => 'Agriculture et agro-industrie',
'keywords' => "agricole,élevage,agronomie,agroalimentaire,agriculteur,industrie,céréale,forêt,rural,blé,secteur,alimentaire,engrais,sylviculture,exploitation,irrigation,biologique,arboriculture,bio,culture,tourisme,alimentation,exportation,champ,horticulture,pêche,machinisme,ressource,artisanat,économie,jardinage,subvention,agraire,bovin,naturel,emploi,FAO,industrialisation,pesticide,plantation,biodiversité,hectare,intensification,richesse,sol,écologique,intensif,ovin,Cérès,ministère,moisson,productivité,rendement,semence,tertiaire,urbanisation,bétail,Chasseur-cueilleur,coton,cultivateur,déforestation,mouton,néolithique,OGM,subsistance,textile,betterave,chasse,cueillette,défrichement,légume,manufacture",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '3',
'label' => 'Industrie et Mines',
'keywords' => "industrie, fabrication, transformation, chimique, mécanique, électronique, produits de nettoyage, moteur, câble, atome, circuit, détergent, eau de javel, appareil, machine, produits, composantes, industrie, machine, réparation, panne, dépannage, génie industrielle, entretien,or, diamant, saphir, rubi, charbon, ingénieur, forge, métallurgie, fer, gisement, forage, carrière, carrière, pierre, sable, houille, bijou, gravier, construction, sous-sol, extraction, usine, fabrication, transformation, cours d'eau, bas fond, profondeur, mer, océan, creuser, rocher, cailloux, industrie et mines, caoutchouc, recyclage, plasturgie, additif, mode, fabriquer, matière, pétole, plastique, cuir, papier, recyclage, pollution, couche d'ozone, rechauffement climatique, embellage, société, environnement, destruction, biodégradable, industrie, plastique, forêt, agriculture, bois, espèce ligneuse, sylviculture, import export, papier, feuille, cahier, menuiserie, meuble, raboteur, arbre, tronçonneuse, camion, électricité, secteur, tranformation, tronc, exploitation, racine, industrie et mines, industriel, textile, métallurgie, forgeron, artisanat, charbon, mécanique, PME, matière prmière, menuiserie, acier, fabrique, industrialiser, conception, machine, aluminim, fer, forgeron, armement, firme, chaine de production, chaudrenerie, modernisation, import, export, coût, métal, bronze, mine, industrie, étude, ingénieur, agence, projet, contrôle, investigation, faisabilité, travail, geni civil, entrepreuneuriat, enquête, statistique, lieu de travail, industrie, conseil, calcul,commerce, négocier, vente, achat, discount, réduction, banque, négociateur, accord, contrat, deal, comprois, drogue, commercial, import, export, taille, business, négociable, industrielle, transaction, transfert, courtier, intermédiaire, marchand, avocat, industrie, négoces, tissu, textile, coudre, broder, broderie, métallurgie, bâtiment, coton, agriculture, secteur, fabrication, manufacture, industrialisation, atelier, transformation, soie, teinture, cuir, chaussure, drap, papier, mode, import, export, outil, magasin, savoi-faire, cotonière, usine, emploi, agroalimentaire, artisanat, tissage, industrie, produits de beauté, parfumerie, alimentaire, fabricant, produit, chimique, beauté, fille, garçon, beau, belle, glycerine, hydroquinone, alcool, eau, huile essentielle, lait de beauté, crème, étiquette, bouteille, carton, emballage, traitement, peau, usine, entrepôt, livraison, fabrication, biopharma, décapage, éclaircir, industrie, médecin, médicament, pharmacie, ministre de la santé, patient, privé, comptable, épidémie, générique, laboratoire, chimie, production, cosmétique, clandestin, usine, marché noir, drogue, savoir-faire, docteur, pharmacien, délégué médical, grossiste, fournisseur, économe, auxiliaire de pharmacie, caissier, hôptal, soigner, ordonnance, dentiste, cabinet, industrie",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '4',
'label' => 'Eau et énergie',
'keywords' => "énergétique,électricité,nucléaire,pétrole,électrique,éolien,cinétique,photosynthèse,puissance,gaz,joule,masse,biomasse,flux,force,thermodynamique,hydroélectricité,hydroélectrique,rendement,atomique,charbon,combustible,géothermie,machine,consommation,photon,rayonnement,moteur,physique,quantité,carbone,environnement,lac,pluie,barrage,inondation,fleuve,océan,source,rivière,bassin,glace,irrigation,pollution,égout,fontaine,mer,aqueduc,affluent,cascade,érosion,étang,nappe,vapeur,puits,écluse, transformateur, changement, processus, évolution, chimie, conversion, industrielle, rénovation, digestion, progrès, extraction, assainissement, amélioration, transformation, eau et énergie, électricité, commercial, vente, partage, livraison, connexion, abonnement, branchement, distribuer, logiciel, système, diffusion, fourniture, fournisseur, plateforme, installation, source, eau et énergie, électricité, Groupe électrogène, Câble électrique, onduleur, générateur, interrupteur, ampoule, Bouteille de gaz, cuisine, dépôt de gaz, éolienne, solaire, soleil, vent, énergie, barrage, eau, nucléaire, joule, photosynthèse, force, électrique, transformation, hydroélectrique, mécanique, combustible, atomique, moteur, batterie, radioactivité, potentiel, température, fission, kilowatt heure, chauffage, développement durable, géothermique, accélération, relativité, eau et énergie, électricité, sources alternatives, creuser, forer, pétrole, eau, puits, gaz, plateforme, nappe, géothermique, trou, tube, tige, pompage, nappe phréatique, perçage, tunnel, sous-sol, sonde, hydraulique, robinet, mètre, travaux, explorer, profondeur, fluide, liquide, extraction, eau et énergie, électricité, forages",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '5',
'label' => 'Infrastructures et BTP ',
'keywords' => "transport,réseau,investissement,tourisme,autoroute,budget,internet,public,routière,aéroport,équipement,ferroviaire,gestionnaire,modernisation,urbain,exploitation,télécommunication,fer,bâtiment,ligne,secteur,service,urbanisation,amélioration,financement,réhabilitation,technologie,tramway,cloud,échangeur,chemin,travaux,programme,trafic,aménagement,route, construction, façade, architecture, immeuble, toiture, béton, local, appartement, studio, chambre, sable, gravier, matériau, maçon, génie civil, ingénieur, technicien, manœuvre, fer, charpente, menuiserie, terrain, peinture, pelle, brouette, eau, moule, ciment, poutre, marbre, fondation, bois, lustre, plomberie, électricité, cuisine, étagère, travaux publics, carreaux, véranda, plan, plafond, lambris, staff, toilette, WC, infrastructures et BTP, ingénieur, polytechnicien, construction, pont, ciment, dalle, chemin, étage, circulation, travaux publics, canal, carrefour, terre-plein, bord, tramway, rocade, tablier, ruisseau, infrastructures et BTP, cabinet, études, ingénieur, travaux publics, ingénieure, infrastructures et BTP, conseil, consulting, consultation, calcul, agent, immobilier, maison, agence, immeuble, appartement, terrain, peindre, bâtiment, peinture, pinceau, rouleau, travaux publics, mur, pantex, seau, infrastructures et BTP",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '6',
'label' => 'Logistique et transport',
'keywords' => "transport,groupement,militaire,tactique,logisticien,colis,approvisionnement,entrepôt,autobus,camion,commercialisation,flux,voiture,conteneur,cargo,marchandise,opération,appui,ravitaillement,brigade,expédition,armement,front,infrastructure,marketing,stockage,courrier,automobile,conteneurisation,coût,fret,autocar,munition,bus,portuaire,terminal, voyagiste, affaire, tourisme, tour-opérateur, campagne, tourisme, voyage, transport, avion, bus, billet, réservation, train, bateau, taxi, agences de voyage, logistique et transport, location de véhicules, agence, voiture, tourisme, louage, réservation, transport, logistique et transport, déménageur, déménager, logistique, camion, voiture, transport, meuble, carton, ménage, emménagement, caserne, décorer, délocalisation, louer, transfert, local, réinstallation, locataire, préparer, logistique et transport, transport, marchandise, logistique, ravitaillement, port, approvisionnement, contrebande, fret, entrepôt, maritime, stock, vente, douane, cargaison, marchand, marché, commerce, colis, négoce, import, export, conteneur",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '7',
'label' => 'Banque Finance',
'keywords' => "bancaire,crédit,banquier,billet,monnaie,chèque,emprunt,financier,paiement,prêt,investissement,monétaire,finance,dépôt,inflation,dette,devise,taux,banco,coupure,faillite,capital,compte,change,escompte,remboursement,succursale,argent,dollar,entreprise,financement,FMI,institution,transaction,bourse,épargne,liquidité,portefeuille,emprunteur,endettement,assurance,opération,BCE,braquage,établissement,euro,comptoir,guichet,risque,trésor,caissier,client,commerçant,placement,courtier,directeur,économiste,fed,intérêt,krach,service,virement,agence,commission,fiduciaire,garantie,mondial,réserve",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '8',
'label' => 'Communication et médias',
'keywords' => "bancaire,crédit,banquier,billet,monnaie,chèque,emprunt,financier,paiement,prêt,investissement,monétaire,finance,dépôt,inflation,dette,devise,taux,banco,coupure,faillite,capital,compte,change,escompte,remboursement,succursale,argent,dollar,entreprise,financement,FMI,institution,transaction,bourse,épargne,liquidité,portefeuille,emprunteur,endettement,assurance,opération,BCE,braquage,établissement,euro,comptoir,guichet,risque,trésor,caissier,client,commerçant,placement,courtier,directeur,économiste,FED,intérêt,krach,service,virement,agence,commission,fiduciaire,garantie,mondial,réserve, banque, crédit, financier, microfinance, emprunteur, épargne, compte, prêteur, bénéficiaire, finance, pauvre, pauvreté, chèque, billet, carte de crédit, virement, assurance, endettement, FMI, blanchiment, débiteur, rembourser, lingot, négoce, intérêt, banque, crédit, financier, microfinance, emprunteur, épargne, compte, prêteur, bénéficiaire, finance, micro financier, pauvre, pauvreté, devise, euro, dollar, monnaie, BEAC, changer, convertir, banque, crédit, financier, microfinance, emprunteur, épargne, compte, prêteur, bénéficiaire, finance, pauvre, pauvreté, transaction, bancaire, dépense, mandat, versement, commerce, fonds",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '9',
'label' => 'Tourisme et loisirs',
'keywords' => "sportif, amateur, aviron, olympique, golf, tennis, handball, surf, loisir, athlétisme, football, judo, saut, jeu, boxe, olympique, haltérophilie, kayak, volley-ball, golf, entrainement, athlète, pratiquant, escrime, musculation, discipline, pétanque, professionnel, polo, trampoline, stade, amateur, automobile, pratiqué, petit goal, grand goal,karaté, jogging, coupe, rouler, tournoi, physique, piste, gymnase, promenade, touriste, voyage, nocturne, randonné, navigation, balade, déplacement, écolier, clan, véhicule, amour, vacances, école, lycée, faculté, université, écran, film, cinéaste, salle, spectacle, cinéphile, documentaire, festival, acteur, projection, maison, télé, câble, câblo-distribution, ordinateur, tournage, mise en cène, acteur, principal, avant-première, rediffusion, salle, fairmont, hôtelière, chambre, restaurant, résidence, mairie, palace, villa, rue, casino, Auberge, Motel, ville, prostitué, nuit, boite de nuit, ampoule rouge, circuit, menu, plat, couvert, petit-déjeuné, souper, lait, pain, salade, entrée, sortie, client, gérant, ménage, ménagère, cuisinier, directeur, responsable, femme, homme, VIP, snack, tourne-dos, cafétéria, glacier, soya, échoppe, bar, chef, cordon-bleu, domestique, service traiteur, cérémonie, mariage, anniversaire, cocktail, brasserie, boisson, jus, servante, barman, videur, danseuses, cabarets, nuit, guéridon, toilette, disco, nightclub club, club",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '10',
'label' => 'Bois et forêt',
'keywords' => "hêtre,menuiserie,charpente,forêt,acajou,pin,ébène,chauffage,chêne,contreplaqué,poutre,santal,taillis,cerf,charbon,frêne,tronc,arbre,bûcheron,ramure,scierie,bouleau,buis,lisière,sapin,combustible,parquet,métal,teck,cèdre,essence,planche,rabot,bosquet,chalet,tilleul,balsa,bille,billot,érable,fagot,marqueterie,poteau,quille",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '11',
'label' => 'Santé et bien-être',
'keywords' => "chirurgie, vétérinaire, médicale, pathologie, science, psychiatrie, Hippocrate, docteur, pharmacie, physiologie, faculté, santé, remède, Nobel, thérapeutique, anatomie, étudiant, chimie,hôpital, diplôme, doctorat, Gallien, obstétrique, gynécologie, gériatrie, maladie, biologie, pédiatrie, diagnostic, hygiène, pharmacologie, ayurveda, laboratoire, urologie, professeur, gériatrie, patient, malade, dentaire, généraliste, pharmacien, carabin, thèse, chirurgien, dissection, homéopathie, exercer, neurologie, Avicenne, clinique, étude, institut, scientifique, traitement, belladone, interne, pharmacopée, radiologie, orthopédie, réadaptation, dermatologie, réadaptation, santé publique, spécialisation, thérapie, interne, agrégé, chiropratique, médecin légiste, médecin légale, pharmacopée, radiologie, Asclépiades, chaire, codéine, prescrire, sage-femme, spécialisation, spécialiste, vaccination, Académie de médecine, ayurvédique, astrologie, chinoise, conventionnelle, dentiste, espérance de vie, gastro-entérologie, pouls, prescription, psychologie, traditionnelle, biochimie, cancérologie, hématologie, holistique, pneumologie, prix Nobel, allopathie, Claude Bernard,  dentiste, Institut Pasteur, épidémiologie, empirisme, connaissance, zoologie,université, kinésithérapie, réadaptation, ergothérapie, clinique, physiothérapeute, rééducation, chirurgie, traitement, pratiquer, exercice, physique, articulation, psychologie, ostéopathie, ankylose, massage, rhumatologie, orthophonie, centre, chaleur, thérapie, douleur, soin, nutrition, agent, CMA, santé, Ministère, interné, laboratoire, chimie,académie de médecine ",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '12',
'label' => 'Equipement et immobilier',
'keywords' => "mobilier,tiroir,armoire,coffre,lit,commode,table,ébéniste,immeuble,ameublement,buffet,laque,chaise,étagère,siège,cabinet,déménagement,antiquaire,classeur,rangement,placard,bien,tapissier,crédence,marqueterie,menuiserie,menuisier,rotin,tapisserie,argenterie,bibliothèque,Boule,canapé,ébène,
garde-meuble,panetière,vaisselle,acajou,ébénisterie,encoignure,rayonnage,saisie,tabouret,tenture",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '13',
'label' => 'Commerce et Distribution ',
'keywords' => "consommateur,répartition,commercialisation,distributeur,électricité,logiciel,secteur,gaz,disponible,magasin,exploitation,circuit,Ubuntu,alimentaire,dividende,tract,réseau,commercial,marketing,production,potable,soupape,connexion,Disney,échappement,client,jeu,producteur,usage,casting,vente,équilibre,histogramme,hypermarché,livraison,machine,commerce,marché,système,assainissement,classification,coupe,diffusion,distribuer,EDF,réseau,géographie,intermédiaire,licence,méthode,paquet,parti,raffinage,serveur,variance,approvisionnement,bielle,denrée,partage,film,fonction,fournisseur,fourniture,géographique,négoce,installation,monophasé,service",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '14',
'label' => 'Poste et télécommunications',
'keywords' => "Internet, Téléphone, MTN, Orange, Nextel, Camtel, ART, coupure, électrique, cyber-café,cable, intranet, domaine, sous-domaine,  réseau sociaux, Facebook, Yahoo, Google, badoo, recherche, étude, travaux, conférence, site web, web, page web, illimité, débit, limité, école, formation, institut, université, CISCO, bureau, SMS, WhatsApp, forfait, internet, téléphone, MTN, Orange, Nextel, Camtel, ART, coupure, électrique, cyber-café,cable, intranet, domaine, sous-domaine,  réseau sociaux, facebook, yahoo, google, badoo, recherche, étude, travaux, conférence, site web, web, page web, ilimité, débit, limité, école, formation, institut, université, CISCO, bureau, Orange money, Mobil money, call-box, appel, international, SMS, WhatsApp, forfait",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '15',
'label' => 'Mode et habillement ',
'keywords' => "vêtement,textile,chaussure,habit,ajustement,costume,coiffure,mode,jupe,porter,tenue,industrie,secteur,accoutrement,mettre,pantalon,pourpoint,styliste,corset,soutien,parure,tunique,uniforme,veste,bonnet,ceinture,divertissement,laine,perruque,habiller,intendance,adaptation,swag,gant,caleçon,biactives,bord,bracelet,braverie,broderie,collet,commandant,complet,corps,décolleté,déguisement,robe,effets,élégance,fixe-chaussette,fraise,fringue",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '16',
'label' => 'Consulting',
'keywords' => "expert, contrôleur, expertise, audit, bilan, compte, gestion, trésorier, fiscal, budget, flux, crédit, fiduciaire, écriture, comptabilité, impôt, apurement, amortissement, progiciel, logiciel, bureau, facture, service, exercice, conseil, consulting, manager, gestion, stratégie, gouvernance, innovation, organisation, entrepreneuriat, marketing, environnemental, communication, master, dirigeant, dirigeante, audit, managérial, leadership, risque, économie, diplôme, indicateur, partenaire, business, planification, service, bureau, conseil, consulting, employé, employeur, salaire, redevance, paiement, service, développement, écologique, environnement, connaissance, stratégie, relation, partage de connaissance, messagerie instantanée, conseil, consultation, conseil, justice, droit, tribunal, civil, pénal, défense, contentieux, biens, dommages, prison, avocat, huissier, notaire, juge, consulting",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '17',
'label' => 'Travaux et Services Divers',
'keywords' => "événement, agence, culture, sportif, tourisme, salon, accessoire, fête, organisation, cérémonie, mariage, anniversaire, baptême, funéraille, deuil, cadeau, don, réception, bâche, chaise, location, billet, invitation, boisson, décoration,baby-sitting, surveillance, détective, armistice, garde d'enfant, économie, révolution, usine, textile, mécanique, industrialisation, pressing, machine à laver, grand ménage,logement, habitation, revenu, perquisition,ménagère, couple, consommation, dépense, panne, conducteur, auto-école, dépanneur, monteur, chauffagiste, bois, menuiserie, charpente, ébénisterie, serrurerie, moulure, panneau, papeterie, buffet, huisserie, métallique, charpente, chantier, barrière, clôture, soudeur, fer, planche, rabot, raboter, poncé, lisser, papier-vert, clou, marteau, perforeuse, entreprise, plafond, chêne, travail du bois, plancher, cadre, sculpture, meuble, assemblage, atelier, canapé, chaise, magnétique, conducteur, générateur, tension, chauffage, circuit, éclairage, usine, coupure, électrique, gaz, courant, générateur, chauffage, statique, tension,  distribution, éclairage, lampadaire, compteur, panne, transport, central, coulomb, pile, barrage, charge,sécurité, garde du corps, camera, surveiller, vol, agression, arme, protéger, garder",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '18',
'label' => 'Informatique et Design',
'keywords' => "information,programmation,logiciel,ordinateur,informaticien,application,électronique,fichier,périphérique,infographie,interface,matériel,bit,bug,technologie,système,télécommunication,hardware,internet,numérique,outil,automatique,réseau,stockage,bureautique,hacker,langage,multimédia,utilisateur,algorithme,calcul,piratage,programmeur,binaire,cybernétique,imprimante,télématique,bull,communication,disquette,document,traitement,microprocesseur,support,ingénierie,micro-ordinateur,technique,développeur,Ethernet,programme,bogue,équipement,intelligence,machine,octet,progiciel",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '19',
'label' => 'Education et Formation',
'keywords' => "club, recrutement, apprentissage, entraineur, formateur, métier, encadrement, savoir-faire, parfaire, partenaire, certificat, certification, sportif, professionnel, job, université, institut, école, centre, apprendre, diplôme, éducation et formation, centre de formation, université, formation, école, diplôme, former, professeur, étudiant, étude, étudier, ingénieur, master, licence, doctorat, brevet, universitaire, faculté, technicien, diplômé, soutenir, soutenance, salle de cours, amphi, conférence, séminaire, campus, enseignement, module, crédit, unité d'enseignement, épreuve, examen, babillard, résultats, éducation et formation, écoles supérieures,  socialisation, recréation, pause, cantine, programmes, séquence, trimestre, bulletin, épreuve, examen, résultats, kermesse, éducation et formation, écoles secondaires",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '20',
'label' => 'Elevage',
'keywords' => "bétail,agriculture,apiculture,porc,animal,bovin,volaille,pisciculture,ostréiculture,aviculture,mouton,viande,céréale,éleveur,aquaculture,agricole,cheval,intensif,sériciculture,troupeau,cheptel,domestication,race,zootechnie,blé,arboriculture,chèvre,engraissement,ovin,sauvage,agroalimentaire,domestique,exploitation,polyculture,soie,agriculteur,culture,lait,laitier,laitière,manade,sylviculture,transhumance,captivité,céréaliculture,chinchilla,fruitière,huître,jument,maraîchage,pastoralisme,pâturage,prairie,Rani culture,vétérinaire,aquariophilie,barrique,chasse,conchyliculture,Deux-Sèvres,haras,maïs,soja,trutticulture,veau,batterie,brebis,équin, consommation, alimentaire, élevage, huile, biomasse, bovine, croisement, Agriculture et élevage, animal, domestique, arrière-cour, ferme",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '21',
'label' => 'Assurances',
'keywords' => "assureur,contrat,maladie,police,assurance-vie,assuré,banque,crédit,risque,compagnie,indemnisation,mutuelle,actuaire,paiement,prime,santé,vieillesse,dommage,financement,chômage,immobilier,remboursement,responsabilité,secteur,sécurité,cotisation,travailleur,accident,courtier,coût,financier,confiance,assurer,caisse,profession,soin,prestation,social,timide,allocation,aplomb,fermeté,garantie,invalidité,sinistre,foi,mutualiste,obligation,réparation,actif,attestation,professionnel,service,versement,assurance-maladie,bancaire,complémentaire,contrôle,emploi,prévoyance,solidarité,certitude,courtage,employeur,franchise,médical,obligatoire,salaire",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '22',
'label' => 'Hôtellerie et Restauration',
'keywords' => "auberge,hôtelier,restauration,tourisme,gîte,luxe,restaurant,hôtel,hôtelière,hébergement,hôte,secteur,bachelier,clientèle,logis,métier,meublé,palace,accueil,camping,vacance,location,voyageur,hospitalité,nuitée,hostellerie,Accor,pension,Sofitel,touristique,chambre,cloître,réfectoire,cafeteria,cabaret,centre,bistrot,salle,bar,taverne,boutique,buffet,fast-food,hôtellerie,magasin,pub,snack,addition,bouchon,cuisine,cuisinier,discothèque,self-service,boulevard,gastronomie,pizza,restoroute,routier,bouillon,hébergement,restauration,établissement,resto,spécialité,hôtel,hôtelière,mulet,réhabilitation",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '23',
'label' => 'Artisanat',
'keywords' => "artisan,agriculture,poterie,métier,cuir,industrie,tissage,bijou,céramique,tourisme,artisanal,savoir-faire,textile,traditionnel,commerce,atelier,économie,essor,soie,souk,broderie,local,orfèvrerie,secteur,tapis,élevage,fabrication,laque,Shang,tannerie,touriste,industrialisation,vannerie,agricole,culturel,décoratif,produit,terroir,négoce,patron,dentelle,métallurgie,Orlon,Avalon,panier,PME,apprenti,art,commercial,employer,entreprise,ferronnerie,laine,manufacture,orfèvre,potier,profession,salarié,spécificité,tisserand",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '24',
'label' => 'Services publics et diplomatiques',
'keywords' => "ministre, cabinet, administration, éducation, budget, justice, ministériel, santé, finance, défense, enseignement, bureau, démission, gouvernement, fonctionnaire, tutelle, procureur, affaires étrangères, étrangers, instruction, agriculture, nomination, parlementaire, communication, public, emploi, travail, MINEPAT, MINFI, éducation de base, Télécommunication, sport et éducation physique, élevage et pêche, eau et énergie, Administration territoriale, ambassadeur, consulat, diplomatie, légation, diplomatie, attaché, passeport, chancellerie, étranger, réfugié, consul, conseil, devise, négocier, asile, pays, hotte, diplomatie, ambassadrice, secrétaire, affaire, envoie, visa, visite, autorité, délégation",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '25',
'label' => 'Beauté et esthétique',
'keywords' => "salon de coiffure, salon de massage, cosmétique, remise en forme, soins corporels, soins du visage",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('activity_areas')->insert([
'id' => '26',
'label' => 'Environnement et protection de la nature',
'keywords' => "Pollution, écologie, protection, impact, préservation, dégradation environnement, environnemental, eau, recyclage,conseil, consulting, environnement, cabinet, protection , nature, faune, flore, dégradation, déforestation,Écologie, biodiversité, écosystème, durable,vert, biotope, éthologie, biosphère,climat, climatologue, météorologie, géographie, physique, topographie, observation, étude",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
