DB::table('legal_statuses')->insert([
'id' => '1',
'label' => "SARL",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '2',
'label' => "EURL",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '3',
'label' => "SELARL",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '4',
'label' => "SA",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '5',
'label' => "SAS",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '6',
'label' => "SASU",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '7',
'label' => "SNC",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '8',
'label' => "SCP",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '9',
'label' => "ETS",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '10',
'label' => "Association",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '11',
'label' => "GIC",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '12',
'label' => "ONG",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '13',
'label' => "Etablissement Public",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '14',
'label' => "Etablissement Para-Public",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '15',
'label' => "TPE",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '16',
'label' => "SCI",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
DB::table('legal_statuses')->insert([
'id' => '17',
'label' => "Institution étatique",
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()]);
