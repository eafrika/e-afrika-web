RENAME TABLE `e-afrika`.`photos` TO `e-afrika`.`images`;
ALTER TABLE `images` CHANGE `entreprise_id` `company_id` INT(10) UNSIGNED NOT NULL, CHANGE `fichier` `path` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
