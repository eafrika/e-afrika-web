<div id="login-form" class="overlay login" style="display: none;">
    <div class="login-wrapper">
        <div class="login-content" id="loginTarget">
            <a class="close" id="close-login">X</a>
            <h3><img src="{{ asset('assets/images/logo.png') }}" alt="e-afrika logo" width="100" height="50"
                     align="left"> CONNEXION</h3>
            <h6>Connectez-vous pour avoir accès à tous les services de e-Afrika</h6>
            <form method="POST" action="{{ route('login') }}" id="loginForm">
                {{ csrf_field() }}

                <label for="email">
                    Email:
                    <input type="text" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{ old('email') }}" required autofocus/>
                    <small class="help-block"></small>
                </label>
                <label for="password">
                    Mots de passe:
                    <input type="password" name="password" id="password" required/>
                    @if ($errors->has('password'))
                        <label id="password-error" class="error" for="password">{{ $errors->first('password') }}</label>
                    @endif
                </label>


                <button type="submit" id="btn-submit">Se Connecter</button>

                <div style="text-align: center;">
                    <br/>
                    <p><a href="{{ route('password.request') }}">Mot de passe oublié ?</a>
                        <br/>Pas encore inscrit ? <a href="{{route('register')}}">S’inscrire sur e-Afrika</a>
                </div>

            </form>
        </div>
    </div>
</div>
