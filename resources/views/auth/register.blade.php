<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>S'inscrire</title>

    <!-- Fav Icon -->
    <link rel="shortcut icon" href="#">

    <link rel="apple-touch-icon" href="#">
    <link rel="apple-touch-icon" sizes="114x114" href="#">
    <link rel="apple-touch-icon" sizes="72x72" href="#">
    <link rel="apple-touch-icon" sizes="144x144" href="#">

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/user.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/script2.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#inscriptionForm").validate({
                rules: {
                    "name": "required",
                    "email": {
                        required: true,
                        email: true
                    },
                    "nom_utilisateur": "required",
                    "telephone": "required",
                    "adresse": "required",
                    "password": "required",
                    "last_password": "required",
                    "password_confirmation": "required",
                    "nous_connaitre": "required"
                },
                messages: {
                    "name": "Ce champ est obligatoire.",
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "nom_utilisateur": "Ce champ est obligatoire.",
                    "telephone": "Ce champ est obligatoire.",
                    "adresse": "Ce champ est obligatoire.",
                    "password": "Ce champ est obligatoire.",
                    "last_password": "Ce champ est obligatoire.",
                    "password_confirmation": "Ce champ est obligatoire.",
                    "nous_connaitre": "Ce champ est obligatoire."
                }
            });
            $('#nous_connaitre').val('<?php if (isset($current_user)) {
                echo $current_user->nous_connaitre;
            } ?>');
        });
    </script>

</head>
<body>

<div class="container">
    <div class="row main">
        <div class="logo" style="background-color: rgba(255, 255, 255, 0.5); ">
            <center><img src="{{ asset('assets/images/logo.png') }}" width="200" height="100"></center>
        </div>
        <div class="main-login main-center">
            <h5 style="color:#071f42;">Le remplissage de tous les champs est obligatoire.</h5>

            <form class="" method="POST" action="{{ route('register') }}" id="inscriptionForm">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Nom complet</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name"
                                   placeholder="Entrez votre Nom complet" required autofocus/>
                            @if ($errors->has('name'))
                                <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="telephone" class="cols-sm-2 control-label">Téléphone</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                                   id="telephone" placeholder="Entrez votre Téléphone"  autofocus/>
                            @if ($errors->has('phone'))
                                <label id="telephone-error" class="error"
                                       for="telephone">{{ $errors->first('telephone') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="adresse" class="cols-sm-2 control-label">Adresse</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}"
                                   id="adresse" placeholder="Entrez votre Adresse" autofocus/>
                            @if ($errors->has('address'))
                                <label id="adresse-error" class="error"
                                       for="adresse">{{ $errors->first('adresse') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Email</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" id="email"
                                   placeholder="Entrez votre Email" required/>
                            @if ($errors->has('email'))
                                <label id="email-error" class="error" for="email">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Mot de passe</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <!--<input type="password" class="form-control" required="required" name="password" id="password" placeholder="Entrez votre Mots de passe"/>-->
                            <input type="password" name="password" class="form-control" id="password"
                                   placeholder="Mot de passe" required/>
                            @if ($errors->has('password'))
                                <label id="password-error" class="error"
                                       for="password">{{ $errors->first('password') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Confirmer votre mot de passe</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password_confirmation" id="confirm"
                                   placeholder="Confirmer votre mot de passe" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nous_connaitre" class="cols-sm-2 control-label">Comment avez-vous connu e-Afrika
                        ?</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                            <select id="survey" name="survey" class="form-control">
                                <option value="">Comment avez-vous connu e-Afrika ?</option>
                                <option value="AGM">AGM</option>
                                <option value="WIA">WIA</option>
                                <option value="Par CICP">Par CICP</option>
                                <option value="A travers une prospection">A travers une prospection</option>
                                <option value="A travers un moteur de recherche">A travers un moteur de recherche
                                </option>
                                <option value="A travers les réseaux sociaux">A travers les réseaux sociaux</option>
                                <option value="A travers les médias">A travers les médias</option>
                                <option value="Par un ami / un collègue / un proche">Par un ami / un collègue / un
                                    proche
                                </option>
                            </select>
                            @if ($errors->has('survey'))
                                <label id="nous_connaitre_error" class="error"
                                       for="nous_connaitre">{{ $errors->first('survey') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <input type="submit" name="inscription" value="Inscription"
                           class="btn btn-primary btn-lg btn-block login-button">
                </div>

                <!--<center><a href="index.html" style="color:#fff; font-size:0.8em;">Retour à l'Acceuil</a></center>-->
                <a href="{{ route('home') }}" type="button" id="button"
                   class="btn btn-primary btn-lg btn-block login-button" style="font-size:0.9em;">Retour à
                    l'Acceuil</a></br>
            </form>
        </div>
    </div>
</div>

</body>
</html>
