<div class="list-group">
    <h3 class="list-group-item list-group-item-action active">
        Classé par secteur d'activités
    </h3>

    @foreach($activityArea_part_1 as $activity)
        <a href="{{ route('company.search',['s'=>$activity->label]) }}"
           class="list-group-item list-group-item-action">
            {{$activity->label}}
            <span
                class="badge badge-info" style="background: #2e6da4">{{$activity->companies->count()}}</span>
        </a>
    @endforeach

    <div class="collapse multi-collapse" id="more">
        @foreach($activityArea_part_2 as $activity)
            <a href="{{ route('company.search',['s'=>$activity->label]) }}"
               class="list-group-item list-group-item-action">
                {{$activity->label}}
                <span
                    class="badge badge-secondary badge-pill "
                    style="background: #2e6da4">{{$activity->companies->count()}}</span>
            </a>
        @endforeach
    </div>
    <div class="list-group-item list-group-item-action active" data-toggle="collapse"
         href="#more"
         aria-expanded="false"
         aria-controls="more">
        <i class="fa fa-plus-circle"></i> Plus
    </div>
</div>
