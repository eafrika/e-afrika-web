<li class="right clearfix">
    <span class="chat-img pull-right">
        <img
            src="{{ asset('assets/system-images/companies') }}/{{ $message->company->id }}/{{ $message->company->logo }}"
            alt="User Avatar" class=""/>
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            <strong class="primary-font">{{$message->company->name}}</strong>
            <small class="pull-right text-muted">
                <span class="glyphicon glyphicon-time"></span>{{$message->created_at}}</small>
        </div>
        <p>
            {{$message->text}}
        </p>
    </div>
</li>
