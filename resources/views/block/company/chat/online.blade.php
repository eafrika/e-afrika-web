<div class="chatbox chatbox--empty">
    <div class="chatbox__title">
        <h5><a href="#">Communiquer avec un Téléconseiller</a></h5>
        <button class="chatbox__title__tray">
            <span></span>
        </button>
        <button class="chatbox__title__close">
              <span>
                  <svg viewBox="0 0 12 12" width="12px" height="12px">
                      <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                      <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                  </svg>
              </span>
        </button>
    </div>
    {{--
        <div class="chatbox__body">

            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Nulla vel turpis vulputate, tincidunt lectus sed, porta arcu.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--left">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture">
                <p>Curabitur consequat nisl suscipit odio porta, ornare blandit ante maximus.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Cras dui massa, placerat vel sapien sed, fringilla molestie justo.</p>
            </div>
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
                <p>Praesent a gravida urna. Mauris eleifend, tellus ac fringilla imperdiet, odio dolor sodales libero, vel
                    mattis elit mauris id erat. Phasellus leo nisi, convallis in euismod at, consectetur commodo urna.</p>
            </div>
        </div>
    --}}

    <div class="panel panel-primary">
        <div class="panel-collapse collapse in" id="collapseOne">
            <div class="panel-body">
                <section class="chat" id="output-chat">

                </section>

            </div>

            <div class="panel-footer">
                <div class="input-group">
                    <input type="hidden" id="csrf_token_input" value="{{csrf_token()}}"/>
                    @if (Auth::check())
                        <input type="text" name="message" id="msg"
                               data-id="{{ \Illuminate\Support\Facades\Auth::user()->email }}"
                               class="form-control input-sm" placeholder="Bien vouloir vous connecter"
                               value="{{ \Illuminate\Support\Facades\Auth::user()->email }}">
                    @else
                        <input type="text" name="message" id="msg"
                               data-id="{{ \Illuminate\Support\Facades\Auth::user()->email }}"
                               class="form-control input-sm" placeholder="Bien vouloir vous connecter">
                    @endif
                    <span class="input-group-btn">
                          <button class="btn btn-primary btn-sm" id="btn-chat">Envoyer</button>
                      </span>
                </div>
            </div>
        </div>
    </div>
</div>
