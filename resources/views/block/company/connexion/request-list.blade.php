<div class="row row-social">
    <ul>
        @foreach ($connexions as $connexion)
            <li><p>Invitation "{{ $connexion->sender->name }}" comme {{ $connexion->type->label }} ou
                    Potentiel {{ $connexion->type->label }}</p> <a
                    href="{{ route('company.connect.request.accept', $connexion->sender) }}"><i
                        class="fa fa-plus-square"></i> Confirmer ...</a> <a
                    href="{{ route('company.connect.request.cancel', $connexion->sender) }}" class="text-warning"><i
                        class="fa fa-minus-square"></i> Annuler ...</a> <a
                    href="{{ route('company.detail', $connexion->sender) }}" target="_blank" class="text-info"><i
                        class="fa fa-eye"></i> En savoir plus ...</a></li>
        @endforeach
    </ul>
</div>
