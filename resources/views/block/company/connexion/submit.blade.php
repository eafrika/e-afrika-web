<div class="row row-social">
    <form method="POST" action="{{ route('company.connect',$company) }}" role="form" id="socialForm">
        {{ csrf_field() }}
        @foreach(\App\ConnexionType::all() as $type)
            <div class="form-group">
                <div class="radio radio-primary">
                    <input type="radio" name="type" value="{{$type->id}}" id="radio{{$type->id}}">
                    <label for="radio{{$type->id}}">{{$type->label}}</label>
                </div>
            </div>
        @endforeach
        <div class="form-group">
            <div class="col-md-10">
                <button type="submit" class="btn btn-default"><i class="fa fa-play"></i>Envoyer</button>
            </div>
        </div>
    </form>
</div>
