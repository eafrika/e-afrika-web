<div class="row row-entreprise">
    <div class="col-md-12">
        <form method="POST" action="{{ route('company.store') }}" id="entrepriseForm">
            <!-- Form start -->
            {{ csrf_field() }}
            <div class="tab-content">
                <div id="menu1" class="tab-pane fade active in">
                    <h3><img src="{{ asset('assets/images/organisation.png') }}"/> Organisation et Contacts</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="nom">Dénomination Sociale <span
                                                    class="text-warning">*</span></label>
                                            <input id="nom" name="name" type="text" placeholder="Dénomination Sociale"
                                                   class="form-control input-md">
                                            <ul id="log" class="ui-widget-content" style="display: none;"></ul>
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="statut_juridique_id">Statut Juridique
                                                <span class="text-warning">*</span></label>
                                            <select id="statut_juridique_id" name="legal_status_id"
                                                    class="form-control">
                                                <option value="">Choisir un Statut Juridique</option>
                                                @foreach (\App\LegalStatus::orderBy('label', 'ASC')->get() as $key=>$legal_status)
                                                    <option
                                                        value="{{ $legal_status->id }}" @if($key==0) selected @endif>{{ $legal_status->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="secteur_activite_id">Activité principale
                                                <span class="text-warning">*</span></label>
                                            <select id="secteur_activite_id" name="main_activity_id"
                                                    class="form-control">
                                                <option value="">Choisir une activité principale</option>
                                                @foreach (\App\ActivityArea::orderBy('label', 'ASC')->get() as $activity)
                                                    <option
                                                        value="{{ $activity->id }}">{{ $activity->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="secteur_activite">Secteur d'Activité <span
                                                    class="text-warning">*</span></label>
                                            <select id="secteur_activite" name="activities[]" class="form-control"
                                                    multiple size="8">
                                                <option value="" disabled="disabled">Choisir un Secteur d'Activité
                                                </option>
                                                @foreach (\App\ActivityArea::orderBy('label', 'ASC')->get() as $activity)
                                                    <optgroup label="{{ $activity->label }}">
                                                        @foreach ($activity->subActivityAreas()->orderBy('label', 'asc')->get() as $key=>$sub_activity)
                                                            <option
                                                                value="{{ $sub_activity->id }}" @if($key==0) selected @endif>{{ $sub_activity->label }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Input Basic -->
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="date_creation">Date de Création <span
                                                    class="text-warning">*</span></label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input id="date_creation" name="founded_at" type="text" value="{{Carbon\Carbon::now()->subYear()}}"
                                                       placeholder="Date de Création" class="form-control input-md"/>
                                                <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="nombre_employes">Nombre d'Employés <span
                                                    class="text-warning">*</span></label>
                                            <select id="nombre_employes" name="size" class="form-control">
                                                <option value="">Choisir un Nombre d'Employés</option>
                                                <option value="Moins de 10" selected>Moins de 10</option>
                                                <option value="De 10 à 100">De 10 à 100</option>
                                                <option value="De 100 à 500">De 100 à 500</option>
                                                <option value="De 500 à 1000">De 500 à 1000</option>
                                                <option value="Plus de 1000">Plus de 1000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12" >
                                        <div class="form-group">
                                            <label class="control-label" for="org">Partenaire<span
                                                    class="text-warning">*</span></label>
                                            <select id="org" name="organisation" class="form-control" required>
                                                <option value="">Choisir un partenaire</option>
                                                <option value="E-AFRIKA">e-Afrika</option>
                                                <option value="WIA">WOMEN IN AFRICA</option>
                                                <option value="AGM">AFRIQUE GRENIER DU MONDE</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="pays_id">Pays <span
                                                    class="text-warning">*</span></label>
                                            <select id="pays_id" name="country_id" class="form-control">
                                                <option value="">Choisir un Pays</option>
                                                @foreach (\App\Country::orderBy('label', 'ASC')->get() as $country)
                                                    <option value="{{ $country->id }}">{{ $country->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="nom_directeur_general">Nom du Directeur
                                                Général <span class="text-warning">*</span></label>
                                            <input id="nom_directeur_general" name="gm_name" type="text"
                                                   placeholder="Nom du Directeur Général" class="form-control input-md" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="ville">Ville <span
                                                    class="text-warning">*</span></label>
                                            <input id="ville" name="town" type="text" placeholder="Ville"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="telephone">Téléphone <span
                                                    class="text-warning">*</span></label>
                                            <input id="telephone" name="phone" type="text" placeholder="Téléphone"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="email">E-mail <span
                                                    class="text-warning">*</span></label>
                                            <input id="email" name="email" type="email" placeholder="E-mail"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="siteweb">Site Web</label>
                                            <input id="siteweb" name="website" type="text" placeholder="Site Web"
                                                   class="form-control input-md">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="email_directeur_general">E-mail du
                                                Directeur Général <span class="text-warning">*</span></label>
                                            <input id="email_directeur_general" name="gm_email"
                                                   type="text" placeholder="E-mail du Directeur Général"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="adresse_localisation">Adresse /
                                                Localisation <span class="text-warning">*</span></label>
                                            <input type="hidden" name="lat">
                                            <input type="hidden" name="lng">
                                            <input id="adresse_localisation" name="location" type="text" value="Non spécifié"
                                                   placeholder="Adresse / Localisation" class="form-control input-md">
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="zone">Zone <span
                                                    class="text-warning">*</span></label>
                                            <select id="zone" name="zone" class="form-control">
                                                <option value="">Choisir une Zone</option>
                                                <option value="Dans la ville" selected>Dans la ville</option>
                                                <option value="Dans 2 villes">Dans 2 villes</option>
                                                <option value="Dans plus de 2 villes">Dans plus de 2 villes</option>
                                                <option value="Dans tout le Pays">Dans tout le Pays</option>
                                                <option value="Dans 1 à 3 pays">Dans 1 à 3 pays</option>
                                                <option value="Dans plus de 3 pays">Dans plus de 3 pays</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" hidden>
                                        <div class="form-group">
                                            <label class="control-label" for="nombre_agences">Nombre d'Agences <span
                                                    class="text-warning">*</span></label>
                                            <input id="nombre_agences" name="agencies" type="text" value="1"
                                                   placeholder="Nombre d'Agences" class="form-control input-md">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="submit" class="btn btn-success"><i
                                    class="fa fa-check"></i> Terminer
                            </button>
                        </li>
                    </ul>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/produits.png') }}"/> Services et Produits</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-services">
                                            <label class="control-label" for="services">Services <span
                                                    class="text-warning">*</span></label>
                                            <div class="form-group input-group">
                                                <input name="services[]" type="text" placeholder="Service" value="Non Specified"
                                                       class="services form-control input-md">
                                                <span class="input-group-btn"><button type="button"
                                                                                      class="btn btn-default btn-add">+</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-produits">
                                            <label class="control-label" for="produits">Produits</label>
                                            <div class="form-group input-group">
                                                <input name="products[]" type="text" placeholder="Produit"
                                                       class="produits form-control input-md">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-add">+</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/partenaires.png') }}"/> Partenaires et Clients</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-partenaires">
                                            <label class="control-label" for="partenaires">Partenaires</label>
                                            <div class="form-group input-group">
                                                <input name="partners[]" type="text" placeholder="Partenaire"
                                                       class="partenaires form-control input-md">
                                                <span class="input-group-btn"><button type="button"
                                                                                      class="btn btn-default btn-add">+</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-clients">
                                            <label class="control-label" for="clients">Clients</label>
                                            <div class="form-group input-group">
                                                <input name="clients[]" type="text" placeholder="Client"
                                                       class="clients form-control input-md">
                                                <span class="input-group-btn"><button type="button"
                                                                                      class="btn btn-default btn-add">+</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu4" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/organisation.png') }}"/> Informations Économiques</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="chiffre_affaires">Chiffre d'Affaires <span
                                                    class="text-warning">*</span></label>
                                            <select id="chiffre_affaires" name="business_turnover" class="form-control">
                                                <option value="">Choisir un Chiffre d'Affaires</option>
                                                <option value="De 0 à 10 millions" selected>De 0 à 10 millions</option>
                                                <option value="De 10 à 100 millions">De 10 à 100 millions</option>
                                                <option value="De 100 à 500 millions">De 100 à 500 millions</option>
                                                <option value="De 500 millions à 1 milliard">De 500 millions à 1
                                                    milliard
                                                </option>
                                                <option value="De 1 à 5 milliards">De 1 à 5 milliards</option>
                                                <option value="De 5 à 10 milliards">De 5 à 10 milliards</option>
                                                <option value="Plus de 10 milliards">Plus de 10 milliards</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="marche">Parts de Marché</label>
                                            <input id="marche" name="market" type="text" placeholder="Parts de Marché"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu5" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/atouts.png') }}"/> Atouts et Réalisations</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-atouts">
                                            <label class="control-label" for="atouts">Atouts</label>
                                            <div class="form-group input-group">
                                                <input name="assets[]" type="text" placeholder="Atout"
                                                       class="atouts form-control input-md">
                                                <span class="input-group-btn"><button type="button"
                                                                                      class="btn btn-default btn-add">+</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-realisations">
                                            <label class="control-label" for="realisations">Réalisations</label>
                                            <div class="form-group input-group">
                                                <input name="realisations[]" type="text" placeholder="Réalisation"
                                                       class="realisations form-control input-md">
                                                <span class="input-group-btn"><button type="button"
                                                                                      class="btn btn-default btn-add">+</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu6" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/autres.png') }}"/> Autres Informations</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="autre_personne">Autre Personne à
                                                Contacter</label>
                                            <input id="autre_personne" name="team_member2[]" type="text"
                                                   placeholder="Autre Personne à Contacter"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="autre_email">E-mail</label>
                                            <input id="autre_email" name="team_member2[]" type="text"
                                                   placeholder="E-mail"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="autre_telephone">Téléphone</label>
                                            <input id="autre_telephone" name="team_member2[]" type="text"
                                                   placeholder="Téléphone" class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="facebook">Facebook</label>
                                            <input id="facebook" name="facebook" type="text" placeholder="Facebook"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="youtube">Youtube</label>
                                            <input id="youtube" name="youtube" type="text" placeholder="Youtube"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="autres_informations">Autres
                                                Informations</label>
                                                <input name="more_information" type="text" placeholder="Information"
                                                       class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="slogan">Slogan</label>
                                            <input id="slogan" name="slogan" type="text" placeholder="Slogan"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="autre_fax">Fax</label>
                                            <input id="autre_fax" name="team_member2[]" type="text" placeholder="Fax"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="twitter">Twitter</label>
                                            <input id="twitter" name="twitter" type="text" placeholder="Twitter"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="linkedin">Linkedin</label>
                                            <input id="linkedin" name="linkedin" type="text" placeholder="Linkedin"
                                                   class="form-control input-md">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu7" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/organisation.png') }}"/> Enregistrement</h3>
                    <hr/>
                    <p>Renseigner les champs requis aux Formulaires 1 à 6 puis cliquer sur le bouton Suivant.</p>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="submit" class="btn btn-success" disabled="disabled"><i
                                    class="fa fa-check"></i> Terminer
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
        <!-- form end -->
    </div>
</div>

