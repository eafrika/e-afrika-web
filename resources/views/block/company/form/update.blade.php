<div class="row row-entreprise">
    <div class="col-md-12">

        <form method="POST" action="{{ route('company.update',$company) }}" id="entrepriseForm">
            <!-- Form start -->
            {{ csrf_field() }}
            <input type="hidden" name="serv">
            <input type="hidden" name="prod">
            <input type="hidden" name="part">
            <input type="hidden" name="clt">
            <input type="hidden" name="ats">
            <input type="hidden" name="real">
            <input type="hidden" name="info">
            <div class="process">
                <div class="process-row nav nav-tabs">
                    <div class="process-step">
                        <button type="button" class="btn btn-info btn-circle" data-toggle="tab" href="#menu1">
                            <span>1</span></button>
                        <p>
                            <small>Organisation et Contacts</small>
                        </p>
                    </div>
                    <div class="process-step">
                        <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu2">
                            <span>2</span></button>
                        <p>
                            <small>Services et Produits</small>
                        </p>
                    </div>
                    <div class="process-step">
                        <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu3">
                            <span>3</span></button>
                        <p>
                            <small>Partenaires et Clients</small>
                        </p>
                    </div>
                    <div class="process-step">
                        <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4">
                            <span>4</span></button>
                        <p>
                            <small>Informations Economiques</small>
                        </p>
                    </div>
                    <div class="process-step">
                        <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu5">
                            <span>5</span></button>
                        <p>
                            <small>Atouts et Réalisations</small>
                        </p>
                    </div>
                    <div class="process-step">
                        <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu6">
                            <span>6</span></button>
                        <p>
                            <small>Autres Informations</small>
                        </p>
                    </div>
                    <div class="process-step">
                        <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu7">
                            <span>7</span></button>
                        <p>
                            <small>Enregistrement</small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div id="menu1" class="tab-pane fade active in">
                    <h3><img src="{{ asset('assets/images/organisation.png') }}"/> Organisation et Contacts</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="nom">Dénomination Sociale <span
                                                    class="text-warning">*</span></label>
                                            <input id="nom" name="name" type="text" placeholder="Dénomination Sociale"
                                                   class="form-control input-md" value="{{$company->name}}">
                                            <ul id="log" class="ui-widget-content" style="display: none;"></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="sigle">Sigle</label>
                                            <input id="sigle" name="initials" type="text" placeholder="Sigle"
                                                   class="form-control input-md" value="{{$company->initials}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="description_entreprise">Description de
                                                l'Entreprise</label>
                                            <textarea id="description_entreprise" name="description"
                                                      placeholder="Description de l'Entreprise"
                                                      class="form-control input-md"
                                                      rows="3">{{$company->description}}</textarea>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="statut_juridique_id">Statut Juridique
                                                <span class="text-warning">*</span></label>
                                            <select id="statut_juridique_id" name="legal_status_id"
                                                    class="form-control">
                                                <option value="">Choisir un Statut Juridique</option>
                                                @foreach (\App\LegalStatus::orderBy('label', 'ASC')->get() as $legal_status)
                                                    <option value="{{ $legal_status->id }}"
                                                            @if($legal_status->id == $company->legal_status_id) selected @endif>{{ $legal_status->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="secteur_activite_id">Activité principale
                                                <span class="text-warning">*</span></label>
                                            <select id="secteur_activite_id" name="main_activity_id"
                                                    class="form-control">
                                                <option value="">Choisir une activité principale</option>
                                                @foreach (\App\ActivityArea::orderBy('label', 'ASC')->get() as $activity)
                                                    <option
                                                        value="{{ $activity->id }}"
                                                        @if($activity->id == $company->main_activity_id) selected @endif>{{ $activity->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="secteur_activite">Secteur d'Activité <span
                                                    class="text-warning">*</span></label>
                                            <select id="secteur_activite" name="activities[]" class="form-control"
                                                    multiple size="8">
                                                <option value="" disabled="disabled">Choisir un Secteur d'Activité
                                                </option>
                                                @foreach (\App\ActivityArea::orderBy('label', 'ASC')->get() as $activity)
                                                    <optgroup label="{{ $activity->label }}">
                                                        @foreach ($activity->subActivityAreas()->orderBy('label', 'asc')->get() as $sub_activity)
                                                            <option
                                                                value="{{ $sub_activity->id }}"
                                                                @if($company->subActivities->toArray()!=null)
                                                                @if(in_array($sub_activity->id,collect($company->subActivities->toArray())->reduce(function ($carry, $item) {
            if ($carry == null)
                $carry= array($item['sub_activity_area_id']);
            else
                array_push($carry,$item['sub_activity_area_id']);
            return  $carry;
        })))) selected @endif @endif>{{ $sub_activity->label }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="date_creation">Date de Création <span
                                                    class="text-warning">*</span></label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input id="date_creation" name="founded_at" type="text"
                                                       placeholder="Date de Création" class="form-control input-md"
                                                       value="{{$company->founded_at}}"/>
                                                <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="nombre_employes">Nombre d'Employés <span
                                                    class="text-warning">*</span></label>
                                            <select id="nombre_employes" name="size" class="form-control">
                                                <option value="">Choisir un Nombre d'Employés</option>
                                                <option value="Moins de 10"
                                                        @if(strcmp("Moins de 10",$company->size) == 0) selected @endif>
                                                    Moins de 10
                                                </option>
                                                <option value="De 10 à 100"
                                                        @if(strcmp("De 10 à 100",$company->size) == 0) selected @endif>
                                                    De 10 à 100
                                                </option>
                                                <option value="De 100 à 500"
                                                        @if(strcmp("De 100 à 500",$company->size) == 0) selected @endif>
                                                    De 100 à 500
                                                </option>
                                                <option value="De 500 à 1000"
                                                        @if(strcmp("De 500 à 1000",$company->size) == 0) selected @endif>
                                                    De 500 à 1000
                                                </option>
                                                <option value="Plus de 1000"
                                                        @if(strcmp("Plus de 1000",$company->size) == 0) selected @endif>
                                                    Plus de 1000
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12" >
                                        <div class="form-group">
                                            <label class="control-label" for="org">Partenaire<span
                                                    class="text-warning">*</span></label>
                                            <select id="org" name="organisation" class="form-control" required>
                                                <option value="">Choisir un partenaire</option>
                                                <option value="E-AFRIKA" @if(strcmp("E-AFRIKA",$company->organisation) == 0) selected @endif>e-Afrika</option>
                                                <option value="WIA" @if(strcmp("WIA",$company->organisation) == 0) selected @endif>WOMEN IN AFRICA</option>
                                                <option value="AGM" @if(strcmp("AGM",$company->organisation) == 0) selected @endif>AFRIQUE GRENIER DU MONDE</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="pays_id">Pays <span
                                                    class="text-warning">*</span></label>
                                            <select id="pays_id" name="country_id" class="form-control">
                                                <option value="">Choisir un Pays</option>
                                                @foreach (\App\Country::all() as $country)
                                                    <option value="{{ $country->id }}"
                                                            @if($company->country_id == $country->id) selected @endif>{{ $country->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="ville">Ville <span
                                                    class="text-warning">*</span></label>
                                            <input id="ville" name="town" type="text" placeholder="Ville"
                                                   class="form-control input-md" value="{{$company->town}}">
                                        </div>
                                    </div>
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="code_postal">Code Postal</label>
                                            <input id="code_postal" name="code_postal" type="text"
                                                   placeholder="Code Postal" class="form-control input-md"
                                                   value="{{$company->code_postal}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="telephone">Téléphone <span
                                                    class="text-warning">*</span></label>
                                            <input id="telephone" name="phone" type="text" placeholder="Téléphone"
                                                   class="form-control input-md" value="{{$company->phone}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="email">E-mail <span
                                                    class="text-warning">*</span></label>
                                            <input id="email" name="email" type="email" placeholder="E-mail"
                                                   class="form-control input-md" value="{{$company->email}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="siteweb">Site Web</label>
                                            <input id="siteweb" name="website" type="text" placeholder="Site Web"
                                                   class="form-control input-md" value="{{$company->website}}">
                                        </div>
                                    </div>
                                    @if($company->teams->count()>0)
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="nom_directeur_general">Nom du
                                                    Directeur
                                                    Général</label>
                                                <input name="team_member1[]" type="hidden"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->first()->id}}">
                                                <input id="nom_directeur_general" name="team_member1[]" type="text"
                                                       placeholder="Nom du Directeur Général"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->first()->name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="telephone_directeur_general">Téléphone
                                                    du
                                                    Directeur Général</label>
                                                <input id="telephone_directeur_general" name="team_member1[]"
                                                       type="text" placeholder="Téléphone du Directeur Général"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="email_directeur_general">E-mail du
                                                    Directeur Général</label>
                                                <input id="email_directeur_general" name="team_member1[]"
                                                       type="text" placeholder="E-mail du Directeur Général"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->first()->email}}">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="nom_directeur_general">Nom du
                                                    Directeur
                                                    Général</label>
                                                <input id="nom_directeur_general" name="team_member1[]" type="text"
                                                       placeholder="Nom du Directeur Général"
                                                       class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="telephone_directeur_general">Téléphone
                                                    du
                                                    Directeur Général</label>
                                                <input id="telephone_directeur_general" name="team_member1[]"
                                                       type="text" placeholder="Téléphone du Directeur Général"
                                                       class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="email_directeur_general">E-mail du
                                                    Directeur Général</label>
                                                <input id="email_directeur_general" name="team_member1[]"
                                                       type="text" placeholder="E-mail du Directeur Général"
                                                       class="form-control input-md">
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="adresse_localisation">Adresse /
                                                Localisation <span class="text-warning">*</span></label>
                                            <input type="hidden" name="lat">
                                            <input type="hidden" name="lng">
                                            <input id="adresse_localisation" name="location" type="text"
                                                   placeholder="Adresse / Localisation" class="form-control input-md"
                                                   value="{{$company->location}}">
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="zone">Zone <span
                                                    class="text-warning">*</span></label>
                                            <select id="zone" name="zone" class="form-control">
                                                <option value="">Choisir une Zone</option>
                                                <option value="Dans la ville"
                                                        @if(strcmp("Dans la ville",$company->zone) == 0) selected @endif>
                                                    Dans la ville
                                                </option>
                                                <option value="Dans 2 villes"
                                                        @if(strcmp("Dans 2 villes",$company->zone) == 0) selected @endif>
                                                    Dans 2 villes
                                                </option>
                                                <option value="Dans plus de 2 villes"
                                                        @if(strcmp("Dans plus de 2 villes",$company->zone) == 0) selected @endif>
                                                    Dans plus de 2 villes
                                                </option>
                                                <option value="Dans tout le Pays"
                                                        @if(strcmp("Dans tout le Pays",$company->zone) == 0) selected @endif>
                                                    Dans tout le Pays
                                                </option>
                                                <option value="Dans 1 à 3 pays"
                                                        @if(strcmp("Dans 1 à 3 pays",$company->zone) == 0) selected @endif>
                                                    Dans 1 à 3 pays
                                                </option>
                                                <option value="Dans plus de 3 pays"
                                                        @if(strcmp("Dans plus de 3 pays",$company->zone) == 0) selected @endif>
                                                    Dans plus de 3 pays
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="nombre_agences">Nombre d'Agences <span
                                                    class="text-warning">*</span></label>
                                            <input id="nombre_agences" name="agencies" type="text"
                                                   placeholder="Nombre d'Agences" class="form-control input-md"
                                                   value="{{$company->agencies}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step-hidden"><i
                                    class="fa fa-chevron-left"></i> Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/produits.png') }}"/> Services et Produits</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-services">
                                            <label class="control-label" for="services">Services <span
                                                    class="text-warning">*</span></label>
                                            @if(count(explode(';', $company->services))>1)
                                                @foreach (explode(';', $company->services) as $key=>$service)
                                                    @if ($service != '')
                                                        <div class="form-group input-group">
                                                            <input name="services[]" type="text" placeholder="Service"
                                                                   class="services form-control input-md"
                                                                   value="{{$service}}">
                                                            <span class="input-group-btn">
                                                                @if($key==count(explode(';', $company->services))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @elseif(!empty(json_decode($company->services,true)))
                                                @foreach (json_decode($company->services,true) as $key=>$service)
                                                    @if ($service != '')
                                                        <div class="form-group input-group">
                                                            <input name="services[]" type="text" placeholder="Service"
                                                                   class="services form-control input-md"
                                                                   value="{{$service}}">
                                                            <span class="input-group-btn">
                                                                 @if($key==count(json_decode($company->services,true))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="form-group input-group">
                                                    <input name="services[]" type="text" placeholder="Service"
                                                           class="services form-control input-md">
                                                    <span class="input-group-btn"><button type="button"
                                                                                          class="btn btn-default btn-add">+</button></span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-produits">
                                            <label class="control-label" for="produits">Produits</label>
                                            @if(count(explode(';', $company->products))>1)
                                                @foreach (explode(';', $company->products) as $key=>$produit)
                                                    @if ($produit != '')
                                                        <div class="form-group input-group">
                                                            <input name="products[]" type="text" placeholder="Produit"
                                                                   class="produits form-control input-md"
                                                                   value="{{$produit}}">
                                                            <span class="input-group-btn">
                                                       @if($key==count(explode(';', $company->products))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–</button>
                                                                @endif
                                                </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @elseif(!empty(json_decode($company->products,true)))
                                                @foreach (json_decode($company->products,true) as $key=>$produit)
                                                    @if ($produit != '')
                                                        <div class="form-group input-group">
                                                            <input name="products[]" type="text" placeholder="Produit"
                                                                   class="produits form-control input-md"
                                                                   value="{{$produit}}">
                                                            <span class="input-group-btn">
                                                     @if($key==count(json_decode($company->products,true))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="form-group input-group">
                                                    <input name="products[]" type="text" placeholder="Produit"
                                                           class="produits form-control input-md">
                                                    <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-add">+</button>
                                                </span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/partenaires.png') }}"/> Partenaires et Clients</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-partenaires">
                                            <label class="control-label" for="partenaires">Partenaires</label>
                                            @if(count(explode(';', $company->partners))>1)
                                                @foreach (explode(';', $company->partners) as $key=>$partenaire)
                                                    @if ($partenaire != '')
                                                        <div class="form-group input-group">
                                                            <input name="partners[]" type="text"
                                                                   placeholder="Partenaire"
                                                                   class="partenaires form-control input-md"
                                                                   value="{{$partenaire}}">
                                                            <span class="input-group-btn">
                                                                  @if($key==count(explode(';', $company->partners))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @elseif(!empty(json_decode($company->partners,true)))
                                                @foreach (json_decode($company->partners,true) as $key=>$partenaire)
                                                    @if ($partenaire != '')
                                                        <div class="form-group input-group">
                                                            <input name="partners[]" type="text"
                                                                   placeholder="Partenaire"
                                                                   class="partenaires form-control input-md"
                                                                   value="{{$partenaire}}">
                                                            <span class="input-group-btn">
                                                             @if($key==count(json_decode($company->partners,true))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="form-group input-group">
                                                    <input name="partners[]" type="text" placeholder="Partenaire"
                                                           class="partenaires form-control input-md">
                                                    <span class="input-group-btn"><button type="button"
                                                                                          class="btn btn-default btn-add">+</button></span>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-clients">
                                            <label class="control-label" for="clients">Clients</label>
                                            @if(count(explode(';', $company->clients))>1)
                                                @foreach (explode(';', $company->clients) as $key=>$client)
                                                    @if ($client != '')
                                                        <div class="form-group input-group">
                                                            <input name="clients[]" type="text" placeholder="Client"
                                                                   class="clients form-control input-md"
                                                                   value="{{$client}}">
                                                            <span class="input-group-btn">
                                                                @if($key==count(explode(';', $company->clients))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @elseif(!empty(json_decode($company->clients,true)))
                                                @foreach (json_decode($company->clients,true) as $key=>$client)
                                                    @if ($client != '')
                                                        <div class="form-group input-group">
                                                            <input name="clients[]" type="text" placeholder="Client"
                                                                   class="clients form-control input-md"
                                                                   value="{{$client}}">
                                                            <span class="input-group-btn">
                                                             @if($key==count(json_decode($company->clients,true))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="form-group input-group">
                                                    <input name="clients[]" type="text" placeholder="Client"
                                                           class="clients form-control input-md">
                                                    <span class="input-group-btn"><button type="button"
                                                                                          class="btn btn-default btn-add">+</button></span>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu4" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/organisation.png') }}"/> Informations Économiques</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Select Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="chiffre_affaires">Chiffre d'Affaires <span
                                                    class="text-warning">*</span></label>
                                            <select id="chiffre_affaires" name="business_turnover" class="form-control">
                                                <option value="">Choisir un Chiffre d'Affaires</option>
                                                <option value="De 0 à 10 millions"
                                                        @if(strcmp("De 0 à 10 millions",$company->business_turnover) == 0) selected @endif>
                                                    De 0 à 10 millions
                                                </option>
                                                <option value="De 10 à 100 millions"
                                                        @if(strcmp("De 10 à 100 millions",$company->business_turnover) == 0) selected @endif>
                                                    De 10 à 100 millions
                                                </option>
                                                <option value="De 100 à 500 millions"
                                                        @if(strcmp("De 100 à 500 millions",$company->business_turnover) == 0) selected @endif>
                                                    De 100 à 500 millions
                                                </option>
                                                <option value="De 500 millions à 1 milliard"
                                                        @if(strcmp("De 500 millions à 1 milliard",$company->business_turnover) == 0) selected @endif>
                                                    De 500 millions à 1
                                                    milliard
                                                </option>
                                                <option value="De 1 à 5 milliards"
                                                        @if(strcmp("De 1 à 5 milliards",$company->business_turnover) == 0) selected @endif>
                                                    De 1 à 5 milliards
                                                </option>
                                                <option value="De 5 à 10 milliards"
                                                        @if(strcmp("De 5 à 10 milliards",$company->business_turnover) == 0) selected @endif>
                                                    De 5 à 10 milliards
                                                </option>
                                                <option value="Plus de 10 milliards"
                                                        @if(strcmp("Plus de 10 milliards",$company->business_turnover) == 0) selected @endif>
                                                    Plus de 10 milliards
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="marche">Parts de Marché</label>
                                            <input id="marche" name="market" type="text" placeholder="Parts de Marché"
                                                   class="form-control input-md" value="{{$company->market}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu5" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/atouts.png') }}"/> Atouts et Réalisations</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-atouts">
                                            <label class="control-label" for="atouts">Atouts</label>
                                            @if(count(explode(';', $company->assets))>1)
                                                @foreach (explode(';', $company->assets) as $key=>$atout)
                                                    @if ($atout != '')
                                                        <div class="form-group input-group">
                                                            <input name="assets[]" type="text" placeholder="Atout"
                                                                   class="atouts form-control input-md"
                                                                   value="{{$atout}}">
                                                            <span class="input-group-btn">
                                                                @if($key==count(explode(';', $company->assets))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @elseif(!empty(json_decode($company->assets,true) ))
                                                @foreach (json_decode($company->assets,true) as $key=>$atout)
                                                    @if ($atout != '')
                                                        <div class="form-group input-group">
                                                            <input name="assets[]" type="text" placeholder="Atout"
                                                                   class="atouts form-control input-md"
                                                                   value="{{$atout}}">
                                                            <span class="input-group-btn">
                                                              @if($key==count(json_decode($company->assets,true))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="form-group input-group">
                                                    <input name="assets[]" type="text" placeholder="Atout"
                                                           class="atouts form-control input-md">
                                                    <span class="input-group-btn"><button type="button"
                                                                                          class="btn btn-default btn-add">+</button></span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group form-realisations">
                                            <label class="control-label" for="realisations">Réalisations</label>
                                            @if(count(explode(';', $company->realisations))>1)
                                                @foreach (explode(';', $company->realisations) as $key=>$realisation)
                                                    @if ($realisation != '')
                                                        <div class="form-group input-group">
                                                            <input name="realisations[]" type="text"
                                                                   placeholder="Réalisation"
                                                                   class="realisations form-control input-md"
                                                                   value="{{$realisation}}">
                                                            <span class="input-group-btn">
                                                                 @if($key==count(explode(';', $company->realisations))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @elseif(!empty(json_decode($company->realisations ,true)))
                                                @foreach (json_decode($company->realisations ,true) as $key=>$realisation)
                                                    @if ($realisation != '')
                                                        <div class="form-group input-group">
                                                            <input name="realisations[]" type="text"
                                                                   placeholder="Réalisation"
                                                                   class="realisations form-control input-md"
                                                                   value="{{$realisation}}">
                                                            <span class="input-group-btn">
                                                               @if($key==count(json_decode($company->realisations,true))-1)
                                                                    <button type="button"
                                                                            class="btn btn-default btn-add">+</button>
                                                                @else
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-remove">–
                                                                </button>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <div class="form-group input-group">
                                                <input name="realisations[]" type="text" placeholder="Réalisation"
                                                       class="realisations form-control input-md">
                                                <span class="input-group-btn"><button type="button"
                                                                                      class="btn btn-default btn-add">+</button></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu6" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/autres.png') }}"/> Autres Informations</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    @if($company->teams->count()>1)
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_personne">Autre Personne à
                                                    Contacter</label>
                                                <input name="team_member2[]" type="hidden"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->last()->id}}">
                                                <input id="autre_personne" name="team_member2[]" type="text"
                                                       placeholder="Autre Personne à Contacter"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->last()->name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_email">E-mail</label>
                                                <input id="autre_email" name="team_member2[]" type="text"
                                                       placeholder="E-mail"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->last()->email}}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_telephone">Téléphone</label>
                                                <input id="autre_telephone" name="team_member2[]" type="text"
                                                       placeholder="Téléphone" class="form-control input-md"
                                                       value="{{$company->teams->last()->phone}}">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_personne">Autre Personne à
                                                    Contacter</label>
                                                <input id="autre_personne" name="team_member2[]" type="text"
                                                       placeholder="Autre Personne à Contacter"
                                                       class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_email">E-mail</label>
                                                <input id="autre_email" name="team_member2[]" type="text"
                                                       placeholder="E-mail"
                                                       class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_telephone">Téléphone</label>
                                                <input id="autre_telephone" name="team_member2[]" type="text"
                                                       placeholder="Téléphone" class="form-control input-md">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="facebook">Facebook</label>
                                            <input id="facebook" name="facebook" type="text" placeholder="Facebook"
                                                   class="form-control input-md" value="{{$company->facebook}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="youtube">Youtube</label>
                                            <input id="youtube" name="youtube" type="text" placeholder="Youtube"
                                                   class="form-control input-md" value="{{$company->youtube}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well-block">
                                <div class="row">
                                    <!-- Input Basic -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="autres_informations">Autres
                                                Informations</label>
                                            <input name="more_information" type="text" placeholder="Information"
                                                   class="form-control input-md" value="{{$company->more_information}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="slogan">Slogan</label>
                                            <input id="slogan" name="slogan" type="text" placeholder="Slogan"
                                                   class="form-control input-md" value="{{$company->slogan}}">
                                        </div>
                                    </div>
                                    @if($company->teams->count()>1)
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_fax">Fax</label>
                                                <input id="autre_fax" name="team_member2[]" type="text"
                                                       placeholder="Fax"
                                                       class="form-control input-md"
                                                       value="{{$company->teams->last()->phone}}">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" for="autre_fax">Fax</label>
                                                <input id="autre_fax" name="team_member2[]" type="text"
                                                       placeholder="Fax"
                                                       class="form-control input-md">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="twitter">Twitter</label>
                                            <input id="twitter" name="twitter" type="text" placeholder="Twitter"
                                                   class="form-control input-md" {{$company->twitter}}>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="linkedin">Linkedin</label>
                                            <input id="linkedin" name="linkedin" type="text" placeholder="Linkedin"
                                                   class="form-control input-md" {{$company->linkedin}}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-info next-step">Suivant <i
                                    class="fa fa-chevron-right"></i></button>
                        </li>
                    </ul>
                </div>
                <div id="menu7" class="tab-pane fade">
                    <h3><img src="{{ asset('assets/images/organisation.png') }}"/> Enregistrement</h3>
                    <hr/>
                    <p>Renseigner les champs requis aux Formulaires 1 à 6 puis cliquer sur le bouton Suivant.</p>
                    <ul class="list-unstyled list-inline pull-right">
                        <li>
                            <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i>
                                Précédent
                            </button>
                        </li>
                        <li>
                            <button type="submit" class="btn btn-success" disabled="disabled"><i
                                    class="fa fa-check"></i> Terminer
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
        <!-- form end -->
    </div>
</div>

