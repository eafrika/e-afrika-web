<div class="search-result new-job">
    <div class="item">
        <div class="flex-row">
            <div class="grid-content">
                <h3 style="margin-bottom: 0">
                    <img src="https://www.flaticon.com/premium-icon/icons/svg/1316/1316906.svg"
                         alt="sad" style="width: 150px;height: 150px"/>
                    Aucun résultat pour
                    <strong>"{{request('k')}} {{request('s')}}"</strong>
                </h3>
            </div>
        </div>
    </div>
</div>
