<div class="search-result new-job">
    <div class="row">
        @if(\Carbon\Carbon::parse($company->created_at)->diffForHumans(\Carbon\Carbon::now())<=7*2 and $company->created_at!=null)
            <div class="badge">new</div>
        @endif
        <div class="icon col-md-3">
            <a href="{{route('company.detail',$company)}}">
                @if(strpos($company->logo,'bg-logo.png'))
                    <img src="{{ \App\Http\Controllers\Controller::imageGenerator($company->name,'150x90') }}"
                         alt="{{$company->name}}" width="200"/>
                @else
                    <img src="{{asset("assets/system-images/companies/".$company->id)."/".$company->logo}}"
                         alt="{{$company->name}}" height="150" width="200"/>
                @endif
            </a>
        </div>
        <div class="col-md-8">
            <h4>
                <a href="{{route('company.detail',$company)}}"><b>{{$company->name}}</b>
                    @if($company->initials !=null)
                        <span class="company">({{$company->initials}})</span>
                    @endif
                </a>
            </h4>

            {{--<span class="stars"><span class="stars-inner" style="width:17.2px"></span></span>
            <span class="reviews">20%</span>--}}
            <div class="actions">
                @if($company->activity!=null)
                    <p><i class="fa fa-briefcase"></i> {{$company->activity->label}}</p>
                @else
                    <p class="text-animated text-bomb" style="color: red"><i class="fa fa-bomb"></i> {{$company->id}}
                    </p>
                @endif
                <p><i class="fa fa-map-marker"></i> {{$company->town}} </p>
            </div>
            <div class="actions">
                <span class="more"><a href="{{route('company.detail',$company)}}"><i class="fa fa-plus-circle"></i> En savoir plus &hellip;</a></span>
            </div>
        </div>
    </div>
</div>
