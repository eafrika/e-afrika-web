<div id="logo-editor" class="overlay logo-editor" style="display: none;">
    <div class="login-wrapper">
        <div class="login-content" id="loginTarget">
            <h3>Modifier le logo<a class="close" id="close-logo-editor">X</a></h3>


            <div id="upload-demo"></div>
            <form method="POST" action="{{ route('upload-logo',$company) }}" id="logo-form"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-1"></div>
                    <button class="empty file-btn col-md-4">
                        <span>Modifier le logo</span>
                        <input type="file" id="upload" name="logo" value="Choose a file" accept="image/*">
                        <input type="hidden" name="cropped_logo" id="base64Image">
                    </button>
                    <div class="col-md-2"></div>
                    <button id="save-logo" class="col-md-4">Enregistrer le logo</button>
                    <div class="col-md-1"></div>
                </div>
            </form>
        </div>
    </div>
</div>
