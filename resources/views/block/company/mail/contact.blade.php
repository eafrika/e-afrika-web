<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<h2>Bienvenue sur le site e-Afrika</h2>
<p>{{ $data['message'] }}</p>

<p>

    <strong>Nom</strong>: {{ $company->name}} <br/>
    <strong>E-mail</strong>: {{ $company->email }} <br/>
    <strong>Cell</strong>: {{ $company->phone }} <br/>

    @if(array_has($data,'connect'))
        <a href="{{route('company.connect.request.accept',$company)}}"> Accepter la demande</a><br>
        <a href="{{route('company.connect.request.cancel',$company)}}"> Supprimer la demande</a>
    @endif
</p>
</body>
</html>
