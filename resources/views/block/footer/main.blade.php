<footer id="footer" class="clearfix">
    <div id="footer-widgets">

        <div class="container">

            <div id="footer-wrapper">

                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div id="meta-3" class="widget widgetFooter widget_meta">
                            <h4 class="widget-title">A propos d'e-Afrika</h4>
                            <ul>
                                <li><a href="{{ route('intro') }}"></i>Qui Sommes-Nous?</a></li>
                                <li><a href="{{ route('terms') }}"></i>Conditions Générales d'Utilisation</a></li>
                                <li><a href="#"></i>Politique de Confidentialité</a></li>
                            </ul>
                        </div>
                    </div> <!-- end widget1 -->

                    <div class="col-sm-6 col-md-3">
                        <div id="recent-posts-3" class="widget widgetFooter widget_recent_entries">
                            <h4 class="widget-title">Suivez-Nous</h4>
                            <ul>
                                <li>
                                    <a href="https://www.linkedin.com/company/e-afrika/" target="_blank"><i
                                            class="fa fa-linkedin-square"></i> Linkedin</a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/pg/eafrika/community/?ref=page_internal"
                                       target="_blank"><i class="fa fa-facebook-square"></i> Facebook</a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/EAfrika4" target="_blank"><i
                                            class="fa fa-twitter-square"></i> Twitter</a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UChSGoZPsOXfiUVerewDbWXA?view_as=subscriber"
                                       target="_blank"><i class="fa fa-youtube-square"></i> Youtube</a>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end widget1 -->

                    <div class="col-sm-6 col-md-3">
                        <div id="recent-posts-3" class="widget widgetFooter widget_recent_entries">
                            <h4 class="widget-title">Contactez-Nous</h4>
                            <ul>
                                <li>
                                    <span><i class="fa fa-envelope-square"></i> email: data@e-Afrika.com</span>
                                </li>
                                <li>
                                    <span><i class="fa fa-phone-square"></i> Hotline: (+237) 234 389 149</span>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end widget1 -->

                    <form method="GET" action="{{ route('newsletter.subscribe') }}" id="alertForm">
                        <div class="col-sm-6 col-md-3">
                            <div id="meta-4" class="widget widgetFooter widget_meta">
                                <h4 class="widget-title">Alertes</h4>
                                <!--  <div class="form-group">
                                    <div class="input-group" >
                                      <span class="input-group-addon">Email :</span>
                                      <input class="form-control" name="email" placeholder="Votre Email ..." type="email">
                                    </div>
                                  </div> -->

                                <div class="form-group form-service">
                                    <div class="input-group">
                                        <span class="input-group-addon">E-mail  :</span>
                                        <input class="form-control" placeholder="Votre Email ..." name="email"
                                               type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Téléphone  :</span>
                                        <input class="form-control" placeholder="+237234389149" name="phone"
                                               type="text">
                                        <span class="input-group-btn">
                   <input type="submit" class="btn btn-primary" value="S'inscrire">
                  </span>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- end widget1 -->
                    </form>
                </div> <!-- end .row -->

            </div> <!-- end #footer-wrapper -->

        </div> <!-- end .container -->
    </div> <!-- end #footer-widgets -->

    <div id="sub-floor">
        <div class="container">
            <div class="row">
                <div class="col-md-12 copyright">
                    <!-- <center><h6>Copyright e-Afrika © 2017</h6></center> -->
                </div>
                <!--  <div class="col-md-2 col-md-offset-6 attribution">
                   <h6>Developer by  <a target="_blank" href="#">Fabrizio</a></h6>
                 </div> -->
            </div> <!-- end .row -->
        </div>
    </div>

</footer>
