<nav class="navbar navbar-default custom-header">
    <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="{{ route('welcome') }}">
                <span
                    style="color:#00aeef;"> e</span>-Afrika</a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>
                <span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav links">
                <li role="presentation"><a href="{{ route('about') }}">A propos d'e-Afrika</a></li>
                <li role="presentation"><a href="{{ route('our-approach') }}">Notre Approche</a></li>
                {{--   <li role="presentation"><a href="#">e-Afrika News</a></li>--}}
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"> Langue
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        <li><a href="#">Français </a></li>
                        <li><a href="#">Anglais </a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (!Auth::check())
                    <ul class="nav navbar-nav links">
                        <li role="presentation">
                            <a href="#" id="login-page">Se Connecter</a>
                        </li>
                        <li role="presentation">
                            <a href="{{route('register')}}">
                                <span style="border:1px solid #00aeef; padding:5px;">S'inscrire</span>
                            </a>
                        </li>
                    </ul>
                @endif

                @if (Auth::check())
                    <li class="dropdown">
                        <a href="{{ route('home') }}" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            Hello {{ \Illuminate\Support\Facades\Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('user.profile.update') }}"><i class="fa fa-user"></i>
                                    Modifier mon Profil</a></li>
                            @if ((new \App\Http\Controllers\CompanyUsersController())->index()['data']['companies']->total()>0)
                                <li>
                                    <a href="{{ route('my-companies') }}"><i class="fa fa-bar-chart-o"></i> Mes
                                        Entreprises</a></li>
                                <li>
                                    <a href="{{ route('company.connexion', \Illuminate\Support\Facades\Auth::user()->main_company()) }}">
                                        <i class="fa fa-comment"></i> Mes Connexions</a>
                                </li>
                                <li>
                                    <a href="{{ route('company.connect.request',\Illuminate\Support\Facades\Auth::user()->main_company()) }}">
                                        <i class="fa fa-user"></i> Demande de Connexions</a>
                                </li>
                            {{--     <li>
                                     <a href="{{ route('home') }}">
                                         <i class="fa fa-dashboard fa-lg"></i>Mon Tableau de Bord</a>
                                 </li>--}}
                        @endif
                        <!-- <li><a href="#"><i class="fa fa-fw fa-cog"></i> Changer de Mots de Passe</a></li> -->
                            <li class="divider"></li>
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                        class="fa fa-user"></i> Se Déconnecter</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">{{ csrf_field() }}</form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>


        </div>
    </div>
</nav>
