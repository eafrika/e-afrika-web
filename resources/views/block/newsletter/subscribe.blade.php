<div class="row row-social row-news">
    <form method="POST" action="{{ route('newsletter.subscribe') }}" role="form" id="newsForm">
        {{ csrf_field() }}
        <input type="hidden" name="email" value="{{ $email }}">
        <input type="hidden" name="phone" value="{{ $phone }}">
        <div class="form-group news-title">
            <div class="col-md-12">
                <label>Etre informé(e) de :</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <div class="checkbox">
                    <label><input type="checkbox" name="new_company">Nouvelle Entreprise</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="checkbox">
                    <select class="form-control" name="activity_area">
                        <option value="">Tous les Secteurs</option>
                        @foreach (\App\ActivityArea::all() as $activity)
                            <option value="{{ $activity->id }}">{{ $activity->label }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <div class="checkbox">
                    <label><input type="checkbox" name="news">Actualités</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="checkbox">
                    <select class="form-control" name="types">
                        <option value="">Tous Types</option>
                        @foreach (\App\Newstype::all() as $type)
                            <option value="{{ $type->id }}">{{ $type->label }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <div class="checkbox">
                    <label><input type="checkbox" name="updates">Nouveautés sur le Site</label>
                </div>
            </div>
        </div>
        <div class="form-group news-title">
            <div class="col-md-12">
                <label>Par</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <div class="checkbox">
                    @if ($email != '')
                        <label><input type="checkbox" name="mail" checked="checked">E-mail</label>
                    @else
                        <label><input type="checkbox" name="mail" disabled="disabled">E-mail</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <div class="checkbox">
                    @if ($phone != '')
                        <label><input type="checkbox" name="whatsapp" checked="checked">Whatsapp ou SMS</label>
                    @else
                        <label><input type="checkbox" name="whatsapp" disabled="disabled">Whatsapp ou SMS</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10">
                <button type="submit" class="btn btn-default"><i class="fa fa-play"></i>Envoyer</button>
            </div>
        </div>
    </form>
</div>
