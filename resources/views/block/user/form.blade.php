<div class="container">
    <div class="row main">
        <div class="logo" style="background-color: rgba(255, 255, 255, 0.5); ">
            <center><img src="{{ asset('assets/images/logo.png') }}" width="200" height="100"></center>
        </div>
        <div class="main-login main-center">
            <h5 style="color:#071f42;">Le remplissage de tous les champs est obligatoire.</h5>

            <form class="" method="POST" action="{{ route('user.profile.update') }}" id="inscriptionForm">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Nom complet</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name"
                                   value="{{ \Illuminate\Support\Facades\Auth::user()->name }}" id="name"
                                   placeholder="Entrez votre Nom complet" required autofocus/>
                            @if ($errors->has('name'))
                                <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="telephone" class="cols-sm-2 control-label">Téléphone</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="phone"
                                   value="{{ \Illuminate\Support\Facades\Auth::user()->phone }}" id="telephone"
                                   placeholder="Entrez votre Téléphone" autofocus/>
                            @if ($errors->has('phone'))
                                <label id="telephone-error" class="error"
                                       for="telephone">{{ $errors->first('phone') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="adresse" class="cols-sm-2 control-label">Adresse</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="adresse"
                                   value="{{ \Illuminate\Support\Facades\Auth::user()->address }}" id="adresse"
                                   placeholder="Entrez votre Adresse" autofocus/>
                            @if ($errors->has('address'))
                                <label id="adresse-error" class="error"
                                       for="adresse">{{ $errors->first('address') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Email</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            <input type="email" class="form-control" name="email"
                                   value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" id="email"
                                   placeholder="Entrez votre Email" required/>
                            @if ($errors->has('email'))
                                <label id="email-error" class="error" for="email">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Mot de passe</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <!--<input type="password" class="form-control" required="required" name="password" id="password" placeholder="Entrez votre Mots de passe"/>-->
                            <input type="password" name="old_password" class="form-control" id="last-password"
                                   placeholder="Mot de passe" required/>
                            @if ($errors->has('old_password'))
                                <label id="last-password-error" class="error"
                                       for="password">{{ $errors->first('old_password') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Nouveau Mot de passe</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <!--<input type="password" class="form-control" required="required" name="password" id="password" placeholder="Entrez votre Mots de passe"/>-->
                            <input type="password" name="password" class="form-control" id="password"
                                   placeholder="Nouveau Mot de passe" required/>
                            @if ($errors->has('password'))
                                <label id="password-error" class="error"
                                       for="password">{{ $errors->first('password') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Confirmer votre mot de passe</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password_confirmation" id="confirm"
                                   placeholder="Confirmer votre mot de passe" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nous_connaitre" class="cols-sm-2 control-label">Comment avez-vous connu e-Afrika
                        ?</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                            <select id="nous_connaitre" name="survey" class="form-control">
                                <option value="">Comment avez-vous connu e-Afrika ?</option>
                                <option value="AGM">AGM</option>
                                <option value="WIA">WIA</option>
                                <option value="Par CICP">Par CICP</option>
                                <option value="A travers une prospection">A travers une prospection</option>
                                <option value="A travers un moteur de recherche">A travers un moteur de recherche
                                </option>
                                <option value="A travers les réseaux sociaux">A travers les réseaux sociaux</option>
                                <option value="A travers les médias">A travers les médias</option>
                                <option value="Par un ami / un collègue / un proche">Par un ami / un collègue / un
                                    proche
                                </option>
                            </select>
                            @if ($errors->has('survey'))
                                <label id="nous_connaitre_error" class="error"
                                       for="nous_connaitre">{{ $errors->first('survey') }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <input type="submit" name="inscription" value="Mise à jour"
                           class="btn btn-primary btn-lg btn-block login-button">
                </div>

                <!--<center><a href="index.html" style="color:#fff; font-size:0.8em;">Retour à l'Acceuil</a></center>-->
                <a href="{{ route('welcome') }}" type="button" id="button"
                   class="btn btn-primary btn-lg btn-block login-button" style="font-size:0.9em;">Retour à
                    l'Acceuil</a></br>
            </form>
        </div>
    </div>
</div>
