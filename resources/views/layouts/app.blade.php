<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fav Icon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon_tW2_icon.ico') }}" type="image/x-icon">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">
    <meta name="twitter:title" content="@yield('title')">
    <meta name="twitter:description" content="@yield('description')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" href="#">
    <link rel="apple-touch-icon" sizes="114x114" href="#">
    <link rel="apple-touch-icon" sizes="72x72" href="#">
    <link rel="apple-touch-icon" sizes="144x144" href="#">

    <link href="{{ asset('assets/css/jquery-ui.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/dropzone.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/rating.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/login.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/stylesheet.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/toast/vanillatoasts.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('assets/css/rating.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('stylesheet')

</head>
<body>
<!-- Formulaire de connexion  -->

@include('auth.login')

@if (session('result'))
    <div class="alert alert-success">
        {{ session('result.alert.success') }}
    </div>
@endif

<audio id="message">
    <source src="{{asset('assets/sound-notification/definite.ogg')}}" type="audio/ogg">
    <source src="{{asset('assets/sound-notification/definite.mp3')}}" type="audio/mpeg">
</audio>
<audio id="connected">
    <source src="{{asset('assets/sound-notification/unconvinced.ogg')}}" type="audio/ogg">
    <source src="{{asset('assets/sound-notification/unconvinced.mp3')}}" type="audio/mpeg">
</audio>

@if (session('error'))
    <div class="alert alert-info">
        {{ session('error.errors.message') }}
    </div>
@endif

@include('block.header.main')

@yield('content')

@include('block.footer.main')


<script src="{{ asset('assets/js/jquery.js') }}"></script>
<script src="{{ asset('assets/js/jquery.localscroll-1.2.7-min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.scrollTo-1.4.2-min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.js') }}"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCPvfzrvXhOrpbBgaZpfHR7MxS1-fwIVxU"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118956387-1"></script>
<script src="{{ asset('assets/js/jquery.geocomplete.js') }}"></script>
<script src="{{ asset('assets/js/moment-with-locales.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>

<script src="{{ asset('assets/js/Chart.js') }}"></script>
<script src="{{ asset('assets/js/Chart.bundle.js') }}"></script>
{{--<script src="{{ asset('assets/js/canvas.js') }}" async></script>
        <script src="{{ asset('assets/js/script2.js') }}"></script> --}}
<script src="{{ asset('assets/js/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/rating.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/javascript.js') }}"></script>
<script src="{{ asset('assets/js/login.js') }}"></script>
<script src="{{ asset('assets/js/registration.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets/toast/vanillatoasts.js') }}"></script>

@yield('script')
@section('chat')
    @auth()
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
        <script>

                @if(\Illuminate\Support\Facades\Auth::user()->main_company()!=null)
            let socket = io.connect('{{env('APP_URL')}}:{{env('NODE_PORT')}}');

            @foreach(\Illuminate\Support\Facades\Auth::user()->main_company()->conversations() as $conversation)
            socket.emit('add-user', {
                'client': {{\Illuminate\Support\Facades\Auth::user()->main_company()->id}},
                'conversation': {{$conversation->id}}
            });
            @endforeach
            socket.on('message', function (data) {
                document.getElementById('message').play();
                VanillaToasts.create({

                    // notification title
                    title: 'Nouveau message de : ' + data.name,

                    // notification message
                    text: data.msg,

                    // success, info, warning, error   / optional parameter
                    type: 'info',

                    // auto dismiss after 5000ms
                    timeout: 5000,

                    // executed when toast is clicked
                    callback: function () {
                        document.location = '{{route('company.connexion',\Illuminate\Support\Facades\Auth::user()->main_company())}}' + "#" + data.client
                    }

                });
            });
            @endif

        </script>
    @endauth
@show


<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-118956387-1');
</script>
</body>
<!doctype html>

