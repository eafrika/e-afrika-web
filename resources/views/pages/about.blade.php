@extends('layouts.app')

@section('title', 'Notre Approche')

@section('content')


    <div class="row container-header">
        <div class="col-md-12">
            <table class="table dashboard-block breadcrumb-block">
                <tbody>
                <tr>
                    <td>
                        <div class="col-md-10">
                            <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                  method="GET">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="k"
                                           placeholder="Recherche sur e-Afrika">
                                </div>
                            </form>

                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>


        <div class="approche col-md-12">

            <!--  <div class="jumbotron hero-technology">
                  <h1 class="hero-title"> <img src="images/logo.png" width="300px" height="150px"/> </h1>

                     <div class="container">

                     </div>
              </div> -->
            <div class="row box-right">
                <div class=" texte2 col-md-7 ">
                    <center><img src="{{asset('assets/images/Logo-e-Afrika-blanc.png')}}" width="360px" height="150px"/>
                    </center>
                    <hr style="color:#fff; width:100%; heigth:10px;">
                    <h2 style="font-size:1.5em;">A propos d'e-Afrika</h2>
                    <p>e-Afrika est une plateforme conçue pour résoudre l'un des problèmes majeurs
                        auxquels les PMEs et TPEs en Afrique sont confrontées, à savoir celui de l'accès
                        au marché et la construction de leur image de marque; afin de mieux se faire
                        connaître et de rester compétitives malgré le poids de la concurrence qui se fait
                        de plus en plus pressante du fait de la mondialisation des économies.</p>
                    <p>Pour réussir ce pari, e-Afrika entend mettre à la disposition de toutes les
                        entreprises et y compris du secteur informel un tremplin connecté. En ce sens,
                        e-Afrika est le Vrai Premier Réseau Social d'Affaires en Afrique.</p>
                </div>
            </div>
        </div>


    </div>
@endsection
