@extends('layouts.app')

@section('title', 'Notre Approche')

@section('content')


    <div class="row container-header">
        <div class="col-md-12">
            <table class="table dashboard-block breadcrumb-block">
                <tbody>
                <tr>
                    <td>
                        <div class="col-md-10">
                            <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                  method="GET">

                                <div class="form-group">
                                    <input type="text" class="form-control" name="k"
                                           placeholder="Recherche sur e-Afrika">
                                </div>
                            </form>

                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>


        <div class="approche col-md-12">

            <!--  <div class="jumbotron hero-technology">
                  <h1 class="hero-title"> <img src="images/logo.png" width="300px" height="150px"/> </h1>

                     <div class="container">

                     </div>
              </div> -->
            <div class="row box-right">
                <div class=" texte2 col-md-7 ">
                    <center><img src="{{asset('assets/images/Logo-e-Afrika-blanc.png')}}" width="360px" height="150px"/>
                    </center>
                    <hr style="color:#fff; width:100%; heigth:10px;">

                    <h3 style="font-size:1.3em; text-align:center; color:#fff;">Redessinons la Carte de l'Afrique
                        <br/>... Et Imaginons...</h3>
                    <p><span class="gras">Imaginons</span>... qu'à n'importe quel coin du monde, une PME/TPE africaine
                        puisse
                        être jointe pour des opportunités d'affaires.</p>
                    <p><span class="gras">Imaginons</span>... que nous n'ayons plus besoin d'appeler une connaissance
                        pour trouver
                        le bon service ou le bon produit.</p>
                    <p><span class="gras">Imaginons</span>... que la plupart des PME/TPEs Africaines puisse accéder à
                        une transformation
                        digitale pour augmenter leur capacité offensive.</p>
                    <p><span class="gras">Imaginons</span>... Un réseau actif, un écosystème où les PME/TPEs africaines
                        pourront
                        échanger pour faire le business...</p>
                    <p>
                    <center><span style="font-weight:bold; font-style:italic;">Imaginons e-Afrika. </span></center>
                    </p>
                </div>
            </div>


        </div>
@endsection
