@extends('layouts.app')

@section('title', 'e-Afrika')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company) }}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>
                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img src="{{ asset('assets/images/icon-transparent.png') }}"/>
                                            Mes Connexions ></strong>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <table class="table dashboard-block">
                    <tbody>
                    <tr>
                        <td class="text-center">
                            <h4>My e-Afrika</h4>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img
                                src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"/>
                            <div>
                                <strong>{{ $company->name }}</strong>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-center">
                                <strong>Potentiels Clients Entreprises</strong>
                                <a href="{{ route('company.search') }}">
                                    <h3 class="text-info" id="totalClient">loading ...</h3>
                                </a>

                                <strong>Connectés avec</strong>
                                {{--          <h3 class="text-info">{{ $connect_client_entreprises or 0 }}</h3>--}}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-center">
                                <strong>Potentiels Partenaires Entreprises</strong>
                                <a href="{{ route('company.search') }}"><h3 class="text-info"
                                                                            id="totalPartenaire">loading ...</h3>
                                </a>
                                <strong>Connectés avec</strong>
                                {{--     <h3 class="text-info">{{ $connect_partenaire_entreprises or 0}}</h3>--}}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-center">
                                <strong>Potentiels Fournisseurs Entreprises</strong>
                                <a href="{{ route('company.search') }}"><h3 class="text-info"
                                                                            id="totalFournisseur">loading
                                        ...</h3>
                                </a>
                                <strong>Connectés avec</strong>
                                {{--               <h3 class="text-info">{{ $connect_fournisseur_entreprises or 0 }}</h3>--}}
                            </div>
                        </td>
                    </tr>
                    <tr>

                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4">
                <table class="table dashboard-block connect-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="text-center">
                                <span class="edit-user"><strong>Mes Connexions</strong></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                @forelse ($conversations as $key=>$conversation)
                                    @if($key==0)
                                        <input type="hidden" id="init-conversation" value="{{$conversation->id}}"/>
                                    @endif
                                    @if($conversation->first_company->id == $company->id)
                                        <li class="contact"
                                            onclick="switchView(event,{{$conversation->second_company->id}},{{$conversation->id}})"
                                            @if($key==0)id="init"@endif>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-4 text-center">
                                                        <div class="connect-title"
                                                             data-id="{{ $conversation->second_company->id }}"
                                                             data-logo="{{ $conversation->second_company->logo }}"
                                                             data-nom="{{ $conversation->second_company->name }}">
                                                            <img
                                                                src="{{ asset('assets/system-images/companies') }}/{{ $conversation->second_company->id }}/{{ $conversation->second_company->logo }}"
                                                                alt="User Avatar">
                                                        </div>
                                                        <strong class="connect-title"
                                                                data-id="{{ $conversation->second_company->id }}"
                                                                data-logo="{{ $conversation->second_company->logo }}"
                                                                data-nom="{{ $conversation->second_company->name }}">{{ $conversation->second_company->name }}</strong>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <span class="connect-title"
                                                              data-id="{{ $conversation->second_company->id }}"
                                                              data-logo="{{ $conversation->second_company->logo }}"
                                                              data-nom="{{ $conversation->second_company->name }}">Dernière Conversation</span>
                                                        <span id="connect-{{ $conversation->second_company->id }}"
                                                              class="connect-icon pull-right">0</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @else
                                        <li class="contact"
                                            onclick="switchView(event,{{$conversation->first_company->id}},{{$conversation->id}})"
                                            @if($key==0)id="init"@endif>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-4 text-center">
                                                        <div class="connect-title"
                                                             data-id="{{ $conversation->first_company->id }}"
                                                             data-logo="{{ $conversation->first_company->logo }}"
                                                             data-nom="{{ $conversation->first_company->name }}">
                                                            <img
                                                                src="{{ asset('assets/system-images/companies') }}/{{ $conversation->first_company->id }}/{{ $conversation->first_company->logo }}"
                                                                alt="User Avatar">
                                                        </div>
                                                        <strong class="connect-title"
                                                                data-id="{{ $conversation->first_company->id }}"
                                                                data-logo="{{ $conversation->first_company->logo }}"
                                                                data-nom="{{ $conversation->first_company->name }}">{{ $conversation->first_company->name }}</strong>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <span class="connect-title"
                                                              data-id="{{ $conversation->first_company->id }}"
                                                              data-logo="{{ $conversation->first_company->logo }}"
                                                              data-nom="{{ $conversation->first_company->name }}">Dernière Conversation</span>
                                                        <span id="connect-{{ $conversation->first_company->id }}"
                                                              class="connect-icon pull-right">0</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @empty
                                @endforelse
                            </ul>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-5">
                <table class="table dashboard-block conversation-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-3 text-center">
                                <div>
                                    <img src="{{ asset('assets/images/bg-logo.png') }}" alt="User Avatar"
                                         class="connect-logo">
                                </div>
                                <strong class="connect-nom">Conversation</strong>
                            </div>
                            <div class="col-md-9">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="container chat-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-collapse collapse in" id="collapseOne">
                                                <div class="panel-body">
                                                    @forelse ($conversations as $key=>$conversation)
                                                        @if($conversation->first_company->id == $company->id)
                                                            <ul class="chat" id="{{$conversation->second_company->id}}"
                                                                data-content="{{$conversation->id}}">
                                                                @foreach($conversation->messages as $message)
                                                                    @if($message->company_id==$company->id)
                                                                        @include('block.company.chat.message.right',['message'=>$message])
                                                                    @else
                                                                        @include('block.company.chat.message.left',['message'=>$message])
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        @else
                                                            <ul class="chat" id="{{$conversation->first_company->id}}"
                                                                data-content="{{$conversation->id}}">
                                                                @foreach($conversation->messages as $message)
                                                                    @if($message->company_id==$company->id)
                                                                        @include('block.company.chat.message.right',['message'=>$message])
                                                                    @else
                                                                        @include('block.company.chat.message.left',['message'=>$message])
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    @empty
                                                        <ul class="chat" id="id">

                                                        </ul>
                                                    @endforelse
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="input-group">
                                                        <input type="hidden" id="csrf_token_input"
                                                               value="{{csrf_token()}}"/>
                                                        <input id="msg" type="text" class="form-control input-sm"
                                                               placeholder="Ecrivez un message ici ..."/>
                                                        <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-sm"
                                                                id="btn-chat">Envoyer</button>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection

@section('script')

    <script>

        $(document).ready(function () {

            $('#media').carousel({
                interval: 10000
            });

            $('.container-rating div').rating();

            var rating = 2;
            for (var i = 0; (i < rating); i++) {
                var val = $('.container-rating div').find('input.rating').eq(i).val();
                $('.container-rating div').find('a[title="' + val + '"]').addClass('fullStar');
            }

            $('.container-rating div .stars').unbind();
            $('.container-rating div .stars .star').unbind();

            //Google Map
            var latitude = $('#google-map').data('latitude');
            var longitude = $('#google-map').data('longitude');

            function initialize_map() {
                var myLatlng = new google.maps.LatLng(latitude, longitude);
                var mapOptions = {
                    zoom: 14,
                    scrollwheel: false,
                    center: myLatlng
                };
                var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
                var contentString = '';
                var infowindow = new google.maps.InfoWindow({
                    content: '<b class="map-content">' + $('#google-map').data('address') + '</b>'
                });
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize_map);

            $('#carousel-slide').carousel({
                interval: 3000
            })
        });

    </script>

    <script id="config" type="text/json">
    {
        "api.find":"{{ route('company.search') }}",
        "token" :"{{ $company->id }}"
    }




    </script>

    <script src="{{ asset('assets/js/connexion.js') }}" async></script>

    {{-- <script type="text/javascript">
         $(document).ready(function () {

             const patenaire = new Connexion();

             patenaire.find({'type': 'clients', 'id': '#totalClient'});

             patenaire.find({'type': 'partenaires', 'id': '#totalPartenaire'});

             patenaire.find({'type': 'fournisseurs', 'id': '#totalFournisseur'});

             $("#loginForm").validate({
                 rules: {
                     "email": {
                         required: true,
                         email: true
                     },
                     "password": "required"
                 },
                 messages: {
                     "email": {
                         required: "Ce champ est obligatoire.",
                         email: "L'adresse email n'est pas valide."
                     },
                     "password": "Ce champ est obligatoire."
                 }
             });

             var token = $('form.form-inline').find('input').val();

             $.ajaxSetup({
                 headers: {
                     'X-CSRF-Token': token
                 }
             });

             /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
             $("span.connect-title").each(function () {
                 var connect_id = $(this).attr('data-id');
                 $.ajax({
                     url: '{{ route("company.search") }}',
                     type: 'GET',
                     dataType: 'JSON',
                     data: {connect_id: connect_id}
                 }).done(function (data) {
                     $('#connect-' + connect_id).text(data.message[0].total);
                     console.log('Response data: ', data);
                 }).fail(function (jqXHR, textStatus) {
                     console.log('Request failed: ' + textStatus);
                 });
             });

             // Connexion à socket.io
             var socket = io.connect('https://e-afrika.com:8080');

             // On demande le name, on l'envoie au serveur et on l'affiche dans le titre
             var sender_id = '{{ $company->id }}';
             var name = '{{ $company->nom }}';

             $('.connect-title').click(function () {
                 var receiver_id = $(this).attr('data-id');
                 var receiver_logo = $(this).attr('data-logo');
                 var receiver_nom = $(this).attr('data-nom');

                 $('.connect-title').removeClass('connect-online');
                 $(this).parent().parent().find('span.connect-title').addClass('connect-online');

                 $('#connect-' + receiver_id).text(0);
                 $('.connect-logo').attr('src', '{{ asset("assets/system-images/companies") }}/' + receiver_id + '/' + receiver_logo);
                 $('.connect-nom').text(receiver_nom);

                 $.ajax({
                     url: '{{ route("company.search") }}',
                     type: 'GET',
                     dataType: 'JSON',
                     data: {receiver_id: receiver_id}
                 }).done(function (data) {
                     $('#input-chat').attr('data-id', receiver_id);
                     $('#input-chat').attr('data-name', name);

                     $('.receiver-message').not('.hidden').remove();
                     $('.sender-message').not('.hidden').remove();

                     var n = data.messages.length;
                     for (var i = 0; i < n; i++) {
                         var nom = data.entreprise.nom;
                         var message = data.messages[i].contenu;
                         if (data.messages[i].entreprise_id == sender_id) {
                             var sender_message = $('.sender-message.hidden').clone();
                             sender_message.find('strong').text(name);
                             sender_message.find('p').text(message);
                             sender_message.removeClass('hidden');
                             insert_message(sender_message);
                         } else {
                             var receiver_message = $('.receiver-message.hidden').clone();
                             receiver_message.find('strong').text(nom);
                             receiver_message.find('p').text(message);
                             receiver_message.removeClass('hidden');
                             insert_message(receiver_message);
                         }
                     }
                     console.log('Response data: ', data);
                 }).fail(function (jqXHR, textStatus) {
                     console.log('Request failed: ' + textStatus);
                 });
             });

             socket.emit('connect_client', {sender_id: sender_id});

             // Quand on reçoit un message, on l'insère dans la page
             socket.on('message_client', function (data) {
                 var sender_id = data.sender_id;
                 var receiver_id = $('#input-chat').attr('data-id');
                 var message_id = data.message_id;
                 if (sender_id == receiver_id) {
                     $.ajax({
                         url: '{{ route("company.search", "0") }}',
                         type: 'PUT',
                         dataType: 'JSON',
                         data: {sender_id: sender_id, message_id: message_id}
                     }).done(function (data) {
                         var nom = data.entreprise.nom;
                         var message = data.message.contenu;
                         var receiver_message = $('.receiver-message.hidden').clone();
                         receiver_message.find('strong').text(nom);
                         receiver_message.find('p').text(message);
                         receiver_message.removeClass('hidden');
                         insert_message(receiver_message);
                         console.log('Response data: ', data);
                     }).fail(function (jqXHR, textStatus) {
                         console.log('Request failed: ' + textStatus);
                     });
                 } else {
                     var n = $('#connect-' + sender_id).text();
                     $('#connect-' + sender_id).text(parseInt(n) + 1);
                 }
             });

             // Quand un nouveau client se connecte, on affiche l'information
             socket.on('connect_client', function (data) {
                 var receiver_id = data.sender_id;
                 socket.emit('private_connect_client', {receiver_id: receiver_id});
                 $('#connect-' + receiver_id).addClass('connect-online');
                 console.log(receiver_id + ' a rejoint le Chat!');
             });

             socket.on('private_connect_client', function (data) {
                 var sender_id = data.sender_id;
                 $('#connect-' + sender_id).addClass('connect-online');
                 console.log(sender_id + ' a rejoint le Chat!');
             });

             // Quand un nouveau client se connecte, on affiche l'information
             socket.on('disconnect_client', function (data) {
                 var sender_id = data.sender_id;
                 $('#connect-' + sender_id).removeClass('connect-online');
                 console.log(sender_id + ' a quitté le Chat!');
             });

             // Lorsqu'on envoie le formulaire, on transmet le message et on l'affiche sur la page
             $('#btn-chat').click(function (event) {
                 event.preventDefault();

                 var receiver_id = $('#input-chat').attr('data-id');
                 var message = $('#input-chat').val();

                 if (!isNaN(receiver_id)) {
                     if (message != '') {
                         $.ajax({
                             url: '{{ route("company.search") }}',
                             type: 'POST',
                             dataType: 'JSON',
                             data: {receiver_id: receiver_id, message: message}
                         }).done(function (data) {
                             var message_id = data.message.id;
                             var name = $('#input-chat').attr('data-name');
                             socket.emit('message_client', {receiver_id: receiver_id, message_id: message_id}); // Transmet le message aux autres
                             var sender_message = $('.sender-message.hidden').clone();
                             sender_message.find('strong').text(name);
                             sender_message.find('p').text(message);
                             sender_message.removeClass('hidden');
                             insert_message(sender_message); // Affiche le message aussi sur notre page
                             $('#input-chat').val('').focus(); // Vide la zone de Chat et remet le focus dessus

                             console.log('Response data: ', data);
                         }).fail(function (jqXHR, textStatus) {
                             console.log('Request failed: ' + textStatus);
                         });
                     } else {
                         $("#service-error").remove();
                         $("#collapseOne").append('<label id="service-error" class="error" for="libelle">Entrez un Message.</label>');
                     }
                 } else {
                     $("#service-error").remove();
                     $("#collapseOne").append('<label id="service-error" class="error" for="libelle">Sélectionnez une Connection.</label>');
                 }
             });

             $('#input-chat').keypress(function (event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();

                     var receiver_id = $('#input-chat').attr('data-id');
                     var message = $('#input-chat').val();

                     if (!isNaN(receiver_id)) {
                         if (message != '') {
                             $.ajax({
                                 url: '{{ route("company.search") }}',
                                 type: 'POST',
                                 dataType: 'JSON',
                                 data: {receiver_id: receiver_id, message: message}
                             }).done(function (data) {
                                 var message_id = data.message.id;
                                 var name = $('#input-chat').attr('data-name');
                                 socket.emit('message_client', {receiver_id: receiver_id, message_id: message_id}); // Transmet le message aux autres
                                 var sender_message = $('.sender-message.hidden').clone();
                                 sender_message.find('strong').text(name);
                                 sender_message.find('p').text(message);
                                 sender_message.removeClass('hidden');
                                 insert_message(sender_message); // Affiche le message aussi sur notre page
                                 $('#input-chat').val('').focus(); // Vide la zone de Chat et remet le focus dessus

                                 console.log('Response data: ', data);
                             }).fail(function (jqXHR, textStatus) {
                                 console.log('Request failed: ' + textStatus);
                             });
                         } else {
                             $("#service-error").remove();
                             $("#collapseOne").append('<label id="service-error" class="error" for="libelle">Entrez un Message.</label>');
                         }
                     } else {
                         $("#service-error").remove();
                         $("#collapseOne").append('<label id="service-error" class="error" for="libelle">Sélectionnez une Connection.</label>');
                     }
                 }
             });

             // Ajoute un message dans la page
             function insert_message(message) {
                 $('#output-chat').append(message);
                 $('#output-chat').scrollTop($('#output-chat')[0].scrollHeight);
             }


         });
     </script>--}}

@endsection

@section('chat')
    @auth()
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
        <script>
            var socket = io.connect('{{env('APP_URL')}}:{{env('NODE_PORT')}}');
            let settings = {
                'client': {{$company->id}},
                'conversation': document.getElementById("init-conversation").value
            };

            socket.emit('add-user', {
                'client': settings.client,
                'conversation': settings.conversation
            });
            socket.on('message', function (data) {
                document.getElementById('message').play();
                $("#" + data.conversation_id).append(
                    '<li class="left clearfix receiver-message">\n' +
                    '                              <span class="chat-img pull-left">\n' +
                    '                                  <img src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}" alt="User Avatar" class=""/>\n' +
                    '                              </span>\n' +
                    '                            <div class="chat-body clearfix">\n' +
                    '                                <div class="header">\n' +
                    '                                    <strong\n' +
                    '                                        class="primary-font">{{ $company->name}}</strong>\n' +
                    '                                    <small class="pull-right text-muted">\n' +
                    '                                        <span class="glyphicon glyphicon-time"></span></small>\n' +
                    '                                </div>\n' +
                    '                                <p class="media-content">' + data.msg +
                    '</p>\n' +
                    '                            </div>\n' +
                    '                        </li>');
                scrollToEnd();
            });

            socket.on('online-user', function (data) {

                if (data.client === settings.client) {
                    document.getElementById('connected').play();
                }
            });
            socket.on('offline-user', function (data) {
                if (data.client === settings.friend)
                    $('.chatbox__title').removeClass('active');
            });

            $(document).ready(function () {
                scrollToEnd();
                $(document).keypress(function (e) {
                    if (e.which == 13) {
                        var msg = $('#msg').val();
                        $('#msg').val('');//reset
                        if (msg.length > 0)
                            send_msg(msg);
                    }
                });
            });

            $('#btn-chat').click(function () {
                var msg = $('#msg').val();
                $('#msg').val('');//reset
                if (msg.length > 0)
                    send_msg(msg);
            });

            function send_msg(msg) {
                $.ajax({
                    headers: {'X-CSRF-Token': $('#csrf_token_input').val()},
                    type: "POST",
                    url: "{{route('message.store')}}",
                    data: {
                        'text': msg,
                        'conversation_id': settings.conversation,
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status) {
                            $('#' + visibleChat).append(
                                '<li class="right clearfix sender-message">\n' +
                                '                              <span class="chat-img pull-right">\n' +
                                '                                  <img src="{{ asset('assets/system-images/companies') }}/{{ Auth::user()->main_company()->id }}/{{ Auth::user()->main_company()->logo  }}" alt="User Avatar" class=""/>\n' +
                                '                              </span>\n' +
                                '                            <div class="chat-body clearfix">\n' +
                                '                                <div class="header">\n' +
                                '                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span></small>\n' +
                                '                                    <strong\n' +
                                '                                        class="pull-right primary-font">{{ Auth::user()->main_company()->name }}</strong>\n' +
                                '                                </div>\n' +
                                '                                <p class="media-content">' + msg +
                                '</p>\n' +
                                '                            </div>\n' +
                                '                        </li>');
                            scrollToEnd();
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }

            function scrollToEnd() {
                var d = $('#' + visibleChat);
                d.scrollTop(d.prop("scrollHeight"));
            }

            let visibleChat = 0;

            function switchView(evt, client, conversation) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("chat");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("contact");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" alert-success", "");
                }
                document.getElementById(client).style.display = "block";
                evt.currentTarget.className += " alert-success";
                settings.client = client;
                settings.conversation = conversation;
                visibleChat = client;
            }

            document.getElementById("init").click();

        </script>
    @endauth
@endsection
