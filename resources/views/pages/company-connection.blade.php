@extends('layouts.app')

@section('title', 'e-Afrika')

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginForm").validate({
                rules: {
                    "email": {
                        required: true,
                        email: true
                    },
                    "password": "required"
                },
                messages: {
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "password": "Ce champ est obligatoire."
                }
            });

            $("#socialForm").validate({
                rules: {
                    "type": "required"
                },
                messages: {
                    "type": ""
                }
            });

            $('#socialForm button[type="submit"]').click(function (event) {
                event.preventDefault();

                if ($('#socialForm input[name="type"]').valid()) {
                    $('#socialForm').trigger('submit');
                } else {
                    $("#service-error").remove();
                    $("#socialForm .col-md-10").prepend('<label id="service-error" class="error" for="libelle">Ce champ est obligatoire.</label>');
                    return false;
                }
            });
        });
    </script>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company) }}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>
                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img src="{{ asset('assets/images/icon-transparent.png') }}"/>Demandes
                                            de Connection ></strong>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @isset($list)
            @include('block.company.connexion.request-list')
        @else
            @include('block.company.connexion.submit')
        @endisset
    </div>
@endsection
