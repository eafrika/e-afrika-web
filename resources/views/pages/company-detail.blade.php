@extends('layouts.app')

@section('title', 'e-Afrika')

@section('stylesheet')
    <style>

        /* Tablet and bigger */
        @media ( min-width: 768px ) {
            .grid-divider {
                position: relative;
                padding: 0;
            }

            .grid-divider > [class*='col-'] {
                position: static;
            }

            .grid-divider > [class*='col-']:nth-child(n+2):before {
                content: "";
                border-left: 1px solid #DDD;
                position: absolute;
                top: 0;
                bottom: 0;
            }

            .col-padding {
                padding: 15px;
            }
        }


        /* ONGLETS */
        .badge {
            border-radius: 12px !important;
            font-size: 11px !important;
            font-weight: 300;
            height: 18px;
            padding: 3px 6px;
            text-align: center;
            text-shadow: none !important;
            vertical-align: middle;
        }


        .portlet {
            border-radius: 4px;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .portlet > .portlet-title > .nav-tabs {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: 0 none;
            display: inline-block;
            float: right;
            margin: 0;
        }

        div, select, textarea, span, img, table, label, td, th, p, a, button, ul, code, pre, li {
            border-radius: 0 !important;
        }

        .portlet > .portlet-body.grey-cascade, .portlet.grey-cascade {
            background-color: #061f42;
        }

        .portlet.box.grey-cascade {
            border-color: #00aeef !important;
            border-image: none;
            border-style: none solid solid;
            border-width: 0 1px 1px;
        }

        .portlet.box > .portlet-title {
            border-bottom: 0 none;
            color: #333;
            margin-bottom: 0;
            padding: 0 10px;
        }

        .portlet.box > .portlet-body {
            background-color: #fff;
            padding: 10px;
        }

        .portlet > .portlet-body {
            border-radius: 0 0 4px 4px;
            clear: both;
        }

        .portlet > .portlet-title > .nav-tabs > li > a {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: 0 none;
            color: #fff;
            margin: 0px;
            padding: 8px 12px;
            border-right: solid 0.5px #fff;
        }

        .portlet > .portlet-title > .nav-tabs > li.active > a, .portlet > .portlet-title > .nav-tabs > li:hover > a {
            background: #fff none repeat scroll 0 0;
            border: 0 none;
            color: #00aeef;
            font-style: bold;
        }

        .nav-tabs > li > a, .nav-pills > li > a {
            border-radius: 4px 4px 0 0;
            font-size: 14px;
        }

        .nav-tabs > li > a, .nav-pills > li > a {
            border-radius: 4px 4px 0 0;
            font-size: 14px;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company)}}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>

                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img
                                                src="{{ asset('assets/images/icon-transparent.png') }}"/>{{ strtoupper($company->name) }}
                                            ></strong></li>
                                    @auth
                                        <li>
                                            <strong>
                                                <img src="{{ asset('assets/images/entreprise.png') }}"/>
                                                <a href="{{ route('contact.company', $company) }}">Contacter
                                                    cette Entreprise</a>
                                            </strong>
                                        </li>
                                        <li>
                                            <strong>
                                                <img src="{{ asset('assets/images/informations.png') }}"/>
                                                <a href="{{ route('company.detail', $company) }}">Partager
                                                    cette Entreprise</a>
                                            </strong>
                                        </li>
                                        @if (\App\User::find(\Illuminate\Support\Facades\Auth::id())->companies()->where('company_id',$company->id)->first()!=null or \App\User::find(\Illuminate\Support\Facades\Auth::id())->isAdmin())
                                            <li>
                                                <strong>
                                                    <img src="{{ asset('assets/images/commentaires.png') }}"/>
                                                    <a href="{{ route('company.update', $company) }}">Modifier</a>
                                                </strong>
                                            </li>
                                            <li>
                                                <strong>
                                                    <img src="{{ asset('assets/images/commentaires.png') }}"/>
                                                    <a href="{{ route('upload-logo', $company) }}">Ajouter un
                                                        logo...</a>
                                                </strong>
                                            </li>
                                            @if (boolval($company->main))
                                                <li><strong><img src="{{ asset('assets/images/') }}/commentaires.png"/>
                                                        <a
                                                            href="{{ route('company.detail', $company)  }}">Définir
                                                            par défaut ...</a></strong></li>
                                            @endif
                                            <li><strong><img src="{{ asset('assets/images/') }}/connections.png"/> <a
                                                        href="{{ route('company.detail', $company)  }}">Mes
                                                        Connexions</a></strong></li>
                                            <li><strong><img src="{{ asset('assets/images/') }}/connections.png"/> <a
                                                        href="{{ route('company.connect.request', $company)  }}">Demande
                                                        de
                                                        Connexions <span
                                                            class="text-info">({{ \App\Social::where('receiver_company_id', $company->id)->where('status',false)->count() }})</span></a></strong>
                                            </li>
                                            <li>
                                                <strong>
                                                    <img src="{{ asset('assets/images/commentaires.png') }}"/>
                                                    <a href="{{ route('company.connect', $company) }}">Ajouter à
                                                        mes Connexions</a>
                                                </strong>
                                            </li>
                                        @else
                                            <li>
                                                <strong>
                                                    <img src="{{ asset('assets/images/commentaires.png') }}"/>
                                                    <a href="{{ route('company.connect', $company) }}">Ajouter à
                                                        mes Connexions</a>
                                                </strong>
                                            </li>
                                        @endif

                                    @endauth

                                    @guest()
                                        <li><strong><img src="{{ asset('assets/images/') }}/entreprise.png"/> <a
                                                    href="{{ route('contact.company', $company) }}">Contacter
                                                    cette Entreprise</a></strong></li>
                                        {{--         <li><strong><img src="{{ asset('assets/images/') }}/informations.png"/> <a
                                                             href="{{ route('company.detail', $company)}}">Partager
                                                             cette Entreprise</a></strong></li>--}}
                                        <li><strong><img src="{{ asset('assets/images/') }}/connections.png"/> <a
                                                    href="{{ route('company.connect', $company)}}">Ajouter à
                                                    mes Connexions</a></strong></li>
                                    @endguest
                                    <li class="container-rating">
                                        <div>
                                            <input type="radio" name="example" class="rating" value="1"
                                                   style="display: block;">
                                            <input type="radio" name="example" class="rating" value="2"
                                                   style="display: none;">
                                            <input type="radio" name="example" class="rating" value="3"
                                                   style="display: none;">
                                            <input type="radio" name="example" class="rating" value="4"
                                                   style="display: none;">
                                            <input type="radio" name="example" class="rating" value="5"
                                                   style="display: none;">
                                            <div class="stars"><a class="star" title="1"></a><a class="star"
                                                                                                title="2"></a><a
                                                    class="star" title="3"></a><a class="star" title="4"></a><a
                                                    class="star" title="5"></a></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="container carousel-slide">
            <div id="carousel-slide" class="carousel slide" data-ride="carousel">
            <?php $n = $company->images->count(); ?>
            @if ($n != 0)
                <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php $i = 0; ?>
                        @foreach ($company->images as $photo)
                            @if ($photo != '')
                                @if ($i == 0)
                                    <li data-target="#carousel-slide" data-slide-to="{{ $i }}" class="active"></li>
                                @else
                                    <li data-target="#carousel-slide" data-slide-to="{{ $i }}"></li>
                                @endif
                                <?php $i++; ?>
                            @endif
                        @endforeach
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $i = 0; ?>
                        @foreach ($company->images as $photo)
                            @if ($photo != '')
                                @if ($i == 0)
                                    <div class="item active">
                                        <img
                                            src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $photo->path }}"
                                            alt="" width="100%">
                                    </div>
                                @else
                                    <div class="item">
                                        <img
                                            src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $photo->path }}"
                                            alt="" width="100%">
                                    </div>
                                @endif
                                <?php $i++; ?>
                            @endif
                        @endforeach
                    </div>
            @else
                @if ($company->activity)
                    <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-slide" data-slide-to="0" class="active"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img
                                    src="{{ asset('assets/system-images/activities') }}/{{ $company->activity->id }}/1.jpg"
                                    alt="" width="100%">
                            </div>
                        </div>
                @else
                    <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-slide" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-slide" data-slide-to="1"></li>
                            <li data-target="#carousel-slide" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{ asset('assets/images/secteurs/0/1.jpg') }}" alt="" width="100%">
                            </div>

                            <div class="item ">
                                <img src="{{ asset('assets/images/secteurs/0/2.jpg') }}" alt="" width="100%">
                            </div>

                            <div class="item">
                                <img src="{{ asset('assets/images/secteurs/0/3.jpg') }}" alt="" width="100%">
                            </div>
                        </div>
                @endif
            @endif

            <!-- Left and right controls -->
                <a class="left carousel-control" href="#carousel-slide" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-slide" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>


        <div class="container">
            <div class="row grid-divider">
                <div class="col-sm-3">
                    <div class="col-padding">
                        <div class="icon-title"><strong><img src="{{ asset('assets/images/produits.png') }}"/> Services
                                & Produits</strong></div>
                        <p>
                        <ul>
                            @if(count(explode(';', $company->services))>1)
                                @foreach (explode(';', $company->services) as $service)
                                    @if ($service != '')
                                        <li>{{ $service }}</li>
                                    @endif
                                @endforeach
                            @elseif(!empty(json_decode($company->services,true)))
                                @foreach (json_decode($company->services,true) as $service)
                                    @if ($service != '')
                                        <li>{{ $service }}</li>
                                    @endif
                                @endforeach
                            @endif
                            @if((explode(';', $company->products))>1)
                                @foreach (explode(';', $company->products) as $produit)
                                    @if ($produit != '')
                                        <li>{{ $produit }}</li>
                                    @endif
                                @endforeach
                            @elseif(!empty(json_decode($company->products,true)))
                                @foreach (json_decode($company->products,true) as $produit)
                                    @if ($produit != '')
                                        <li>{{ $produit }}</li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        </p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-padding">
                        <div class="icon-title"><strong><img src="{{ asset('assets/images/partenaires.png') }}"/>
                                Clients</strong></div>
                        <p>
                        <ul>
                            @if(count(explode(';', $company->clients))>1)
                                @foreach (explode(';', $company->clients) as $client)
                                    @if ($client != '')
                                        <li>{{ $client }}</li>
                                    @endif
                                @endforeach
                            @elseif(!empty(json_decode($company->clients,true)))
                                @foreach (json_decode($company->clients,true) as $client)
                                    @if ($client != '')
                                        <li>{{ $client }}</li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        </p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-padding">
                        <div class="icon-title"><strong><img src="{{ asset('assets/images/partenaires.png') }}"/>
                                Partenaires</strong></div>
                        <p>
                        <ul>
                            @if(count(explode(';', $company->partners))>1)
                                @foreach (explode(';', $company->partners) as $partenaire)
                                    @if ($partenaire != '')
                                        <li>{{ $partenaire }}</li>
                                    @endif
                                @endforeach
                            @elseif(!empty(json_decode($company->partners,true)))
                                @foreach (json_decode($company->partners,true) as $partenaire)
                                    @if ($partenaire != '')
                                        <li>{{ $partenaire }}</li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        </p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-padding">
                        <div class="icon-title"><strong><img src="{{ asset('assets/images/web.png') }}"/> Réseaux
                                Sociaux</strong></div>
                        <p>
                        <ul class="list-unstyled">
                            @if ($company->facebook != '')
                                <li><a href="{{ $company->facebook }}" target="_blank"><i
                                            class="fa fa-facebook-square"></i> Facebook</a></li>
                            @endif
                            @if ($company->twitter != '')
                                <li><a href="{{ $company->twitter }}" target="_blank"><i
                                            class="fa fa-twitter-square"></i> Twitter</a></li>
                            @endif
                            @if ($company->youtube != '')
                                <li><a href="{{ $company->youtube }}" target="_blank"><i
                                            class="fa fa-youtube-square"></i> Youtube</a></li>
                            @endif
                            @if ($company->linkedin != '')
                                <li><a href="{{ $company->linkedin }}" target="_blank"><i
                                            class="fa fa-linkedin-square"></i> Linkedin</a></li>
                            @endif
                        </ul>
                        {{--<ul class="list-unstyled">
                            @if (count(explode(';', $company->location)) == 3)
                                <li>
                                    <div id="google-map" class="wow fadeIn"
                                         data-latitude="{{ explode(';', $company->location)[0] }}"
                                         data-longitude="{{ explode(';', $company->location)[1] }}"
                                         data-address="{{ $company->name }}"></div>
                                </li>
                            @endif
                        </ul>--}}
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block organisation-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet box grey-cascade">
                                            <div class="portlet-title">
                                                <ul class="nav nav-tabs nav-tabs-lg pull-left">
                                                    <li class="active"><a data-toggle="tab" href="#tab_1"
                                                                          aria-expanded="true">
                                                            <div class="hidden-xs" align="center">
                                                                <strong>Description</strong></div>
                                                            <div class="hidden-md hidden-lg hidden-sm">
                                                                <i class="fa fa-flag-o "></i>
                                                            </div>
                                                        </a></li>
                                                    <li class=""><a data-toggle="tab" href="#tab_2"
                                                                    aria-expanded="true">
                                                            <div class="hidden-xs">
                                                                <strong>Atouts et Réalisations</strong>
                                                            </div>
                                                            <div class="hidden-md hidden-lg hidden-sm">
                                                                <i class="fa fa-envelope-o "></i>
                                                            </div>
                                                        </a></li>
                                                    <li class=""><a data-toggle="tab" href="#tab_3"
                                                                    aria-expanded="true">
                                                            <div class="hidden-xs">
                                                                <strong>Autres Informations</strong>
                                                            </div>
                                                            <div class="hidden-md hidden-lg hidden-sm">
                                                                <i class="fa fa-ban danger-icon"></i>
                                                            </div>
                                                        </a></li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tabbable">

                                                    <div class="tab-content">
                                                        <div id="tab_1" class="tab-pane active">
                                                            <p>
                                                            <ul class="list-unstyled">
                                                                @if ($company->description!= '')
                                                                    <li>{{ $company->description }}</li>
                                                                @endif
                                                            </ul>
                                                            </p>

                                                        </div>
                                                        <div id="tab_2" class="tab-pane ">
                                                            <p>
                                                            <ul>
                                                                @if(count(explode(';', $company->assets))>1)
                                                                    @foreach (explode(';', $company->assets) as $atout)
                                                                        @if ($atout != '')
                                                                            <li>{{ $atout }}</li>
                                                                        @endif
                                                                    @endforeach
                                                                @elseif(!empty(json_decode($company->assets,true)))
                                                                    @foreach (json_decode($company->assets,true) as $atout)
                                                                        @if ($atout != '')
                                                                            <li>{{ $atout }}</li>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                                @if(count(explode(';', $company->realisations))>1)
                                                                    @foreach (explode(';', $company->realisations) as $realisation)
                                                                        @if ($realisation != '')
                                                                            <li>{{ $realisation }}</li>
                                                                        @endif
                                                                    @endforeach
                                                                @elseif(!empty(json_decode($company->realisations ,true)))
                                                                    @foreach (json_decode($company->realisations ,true) as $realisation)
                                                                        @if ($realisation != '')
                                                                            <li>{{ $realisation }}</li>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                            </p>
                                                        </div>
                                                        <div id="tab_3" class="tab-pane ">

                                                            <ul>
                                                                @if($company->teams->last()!==null)
                                                                    @if ($company->teams->last()->role != '')
                                                                        <li><strong>Autre
                                                                                Personne: </strong>{{ $company->teams->last()->role }}
                                                                        </li>
                                                                    @endif
                                                                    @if ($company->teams->last()->email != '')
                                                                        <li>
                                                                            <strong>E-mail: </strong>{{$company->teams->last()->email }}
                                                                        </li>
                                                                    @endif
                                                                    @if ($company->teams->last()->fax != '')
                                                                        <li>
                                                                            <strong>Fax: </strong>{{ $company->teams->last()->fax }}
                                                                        </li>
                                                                    @endif
                                                                    @if ($company->main_activity != null)
                                                                        <li><strong>Activités
                                                                                Principales: </strong>{{ $company->activity->label }}
                                                                        </li>
                                                                    @endif
                                                                @endif

                                                                <?php $exist_information = false; ?>
                                                                @foreach (explode(';', $company->more_information) as $information)
                                                                    @if ($information != '')
                                                                        <?php $exist_information = true; ?>
                                                                    @endif
                                                                @endforeach
                                                                @if ($exist_information)
                                                                    <li>
                                                                        <strong>Autres Informations:</strong>
                                                                        @foreach (explode(';', $company->more_information) as $information)
                                                                            @if ($information != '')
                                                                                {{ $information.', ' }}
                                                                            @endif
                                                                        @endforeach
                                                                    </li>
                                                                @endif
                                                            </ul>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="icon-title">
                                            <strong><img src="{{ asset('assets/images/organisation.png') }}"/>
                                                Organisation & Contact</strong>
                                        </div>
                                        <ul>
                                            <li><span class="col-md-5"><strong>Création:</strong></span> <span
                                                    class="col-md-7">{{ (new DateTime($company->founded_at))->format("d-m-Y") }}</span>
                                            </li>
                                            @if ($company->teams->first() != null)
                                                @if( $company->teams->first()->name !=null)
                                                    <li><span
                                                            class="col-md-5"><strong>Directeur Général:</strong></span>
                                                        <span
                                                            class="col-md-7">{{ $company->teams->first()->name }}</span>
                                                    </li>
                                                @endif
                                            @endif
                                            <li><span class="col-md-5"><strong>E-mail:</strong></span> <span
                                                    class="col-md-7">{{ $company->email }}</span></li>
                                            <li><span class="col-md-5"><strong>Téléphone :</strong></span> <span
                                                    class="col-md-7">{{ $company->phone }}</span></li>

                                            <li><span class="col-md-5"><strong>Nombre d'Employés:</strong></span> <span
                                                    class="col-md-7">{{ $company->size }}</span></li>
                                            <li><span class="col-md-5"><strong>Pays:</strong></span> <span
                                                    class="col-md-7">{{$company->country->label }}</span></li>
                                            <li><span class="col-md-5"><strong>Ville:</strong></span> <span
                                                    class="col-md-7">{{ $company->town }}</span></li>
                                            @if (count(explode(';', $company->location)) == 3)
                                                <li><span class="col-md-5"><strong>Localisation:</strong></span> <span
                                                        class="col-md-7">{{ explode(';', $company->location)[2] }}</span>
                                                </li>
                                            @else
                                                <li><span class="col-md-5"><strong>Localisation:</strong></span> <span
                                                        class="col-md-7">{{ $company->location }}</span></li>
                                            @endif
                                            @if ($company->website != '')
                                                @if (preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' , $company->website))
                                                    <li><span class="col-md-5"><strong>Site Web:</strong></span> <span
                                                            class="col-md-7"><a href="{{ $company->website }}"
                                                                                target="_blank">{{ $company->website }}</a></span>
                                                    </li>
                                                @else
                                                    <li><span class="col-md-5"><strong>Site Web:</strong></span> <span
                                                            class="col-md-7"><a
                                                                href="http://{{ $company->website }}"
                                                                target="_blank">{{ $company->website }}</a></span>
                                                    </li>
                                                @endif
                                            @endif
                                        </ul>
                                        {{--  <ul class="list-unstyled">
                                              @if (count(explode(';', $company->location)) == 3)
                                                  <li>
                                                      <div id="google-map" class="wow fadeIn"
                                                           data-latitude="{{ explode(';', $company->location)[0] }}"
                                                           data-longitude="{{ explode(';', $company->location)[1] }}"
                                                           data-address="{{ $company->name }}"></div>
                                                  </li>
                                              @endif
                                          </ul>--}}
                                    </div>

                                </div>


                            </div>


                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @auth()
                @if($company->users()->where('user_id',\Illuminate\Support\Facades\Auth::id())->first()==null)
                    @include('block.company.chat.online')
                @endif
            @elseauth()
                @include('block.company.chat.offline')
            @endauth
        </div>
    </div>



@endsection

@section('script')

    <script>

        $(document).ready(function () {

            $('#media').carousel({
                interval: 10000
            });

            $('.container-rating div').rating();

            var rating = 2;
            for (var i = 0; (i < rating); i++) {
                var val = $('.container-rating div').find('input.rating').eq(i).val();
                $('.container-rating div').find('a[title="' + val + '"]').addClass('fullStar');
            }

            $('.container-rating div .stars').unbind();
            $('.container-rating div .stars .star').unbind();

            //Google Map
            var latitude = $('#google-map').data('latitude');
            var longitude = $('#google-map').data('longitude');

            function initialize_map() {
                var myLatlng = new google.maps.LatLng(latitude, longitude);
                var mapOptions = {
                    zoom: 14,
                    scrollwheel: false,
                    center: myLatlng
                };
                var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
                var contentString = '';
                var infowindow = new google.maps.InfoWindow({
                    content: '<b class="map-content">' + $('#google-map').data('address') + '</b>'
                });
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize_map);

            $('#carousel-slide').carousel({
                interval: 3000
            })
        });

    </script>

@endsection

@section('chat')
    @auth
        @if(Auth::user()->main_company()!=null)
            @if($company->id!=Auth::user()->main_company()->id)
                <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
                <script>
                    var socket = io.connect('{{env('APP_URL')}}:{{env('NODE_PORT')}}');
                    let settings = {
                        'client': {{Auth::user()->main_company()->id}},
                        'conversation': {{$conversation->id}},
                        'friend':{{$company->id}}
                    };
                    socket.emit('add-user', {
                        'client': settings.client,
                        'conversation': settings.conversation
                    });
                    socket.on('message', function (data) {
                        //console.log(data);
                        document.getElementById('message').play();
                        $('#output-chat').append(
                            '<li class="left clearfix receiver-message">\n' +
                            '                              <span class="chat-img pull-left">\n' +
                            '                                  <img src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}" alt="User Avatar" class=""/>\n' +
                            '                              </span>\n' +
                            '                            <div class="chat-body clearfix">\n' +
                            '                                <div class="header">\n' +
                            '                                    <strong\n' +
                            '                                        class="primary-font">{{ $company->name}}</strong>\n' +
                            '                                    <small class="pull-right text-muted">\n' +
                            '                                        <span class="glyphicon glyphicon-time"> ' + new Intl.DateTimeFormat('en-US').format(Date.now()) + '</span></small>\n' +
                            '                                </div>\n' +
                            '                                <p class="media-content">' + data.msg +
                            '</p>\n' +
                            '                            </div>\n' +
                            '                        </li>');
                        scrollToEnd();
                    });

                    socket.on('online-user', function (data) {
                        if (data.client === settings.friend) {
                            document.getElementById('connected').play();
                            $('.chatbox__title').addClass('active');
                        }
                    });
                    socket.on('online-msg', function (data) {
                        if (data.conversation - settings.conversation === 0)
                            $('.chatbox__title').addClass('active');
                    });
                    socket.on('offline-user', function (data) {
                        if (data.client === settings.friend)
                            $('.chatbox__title').removeClass('active');
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        scrollToEnd();
                        $(document).keypress(function (e) {
                            if (e.which == 13) {
                                var msg = $('#msg').val();
                                $('#msg').val('');//reset
                                if (msg.length > 0)
                                    send_msg(msg);
                            }
                        });
                    });

                    $('#btn-chat').click(function () {
                        var msg = $('#msg').val();
                        $('#msg').val('');//reset
                        if (msg.length > 0)
                            send_msg(msg);
                    });

                    function send_msg(msg) {
                        $.ajax({
                            headers: {'X-CSRF-Token': $('#csrf_token_input').val()},
                            type: "POST",
                            url: "{{route('message.store')}}",
                            data: {
                                'text': msg,
                                'conversation_id':{{$conversation->id}},
                            },
                            success: function (data) {
                                //console.log(data);
                                if (data.status == true) {
                                    $('#output-chat').append(
                                        '<li class="right clearfix sender-message">\n' +
                                        '                              <span class="chat-img pull-right">\n' +
                                        '                                  <img src="{{ asset('assets/system-images/companies') }}/{{ Auth::user()->main_company()->id }}/{{ Auth::user()->main_company()->logo  }}" alt="User Avatar" class=""/>\n' +
                                        '                              </span>\n' +
                                        '                            <div class="chat-body clearfix">\n' +
                                        '                                <div class="header">\n' +
                                        '                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"> ' + new Intl.DateTimeFormat('en-US').format(Date.now()) + '</span></small>\n' +
                                        '                                    <strong\n' +
                                        '                                        class="pull-right primary-font">{{ Auth::user()->main_company()->name }}</strong>\n' +
                                        '                                </div>\n' +
                                        '                                <p class="media-content">' + msg +
                                        '</p>\n' +
                                        '                            </div>\n' +
                                        '                        </li>');
                                    scrollToEnd();
                                }
                            },
                            error: function (e) {
                                console.log(e);
                            }
                        });
                    }

                    function scrollToEnd() {
                        var d = $('#output-chat');
                        d.scrollTop(d.prop("scrollHeight"));
                    }
                </script>
            @endif
        @endif
    @endauth
@endsection
