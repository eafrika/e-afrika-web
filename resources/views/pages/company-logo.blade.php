@extends('layouts.app')

@section('title', 'e-Afrika')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company) }}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>
                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img src="{{ asset('assets/images/icon-transparent.png') }}"/>
                                            {{ $company->name }}>
                                        </strong>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row row-servise">
            <h1 class="well text-center">Ajouter un Logo</h1>
            <div class="col-lg-12 well">
                <form method="POST" action="{{ route('upload-logo', $company) }}"
                      enctype="multipart/form-data" id="serviceForm">
                    {{ csrf_field() }}
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <div class="form-group">
                                    <a class="thumbnail" href="{{ url()->current()}}">
                                        <img id="preview" alt=""
                                             src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"></a>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <div class="form-group files">
                                    <label>Téléverser votre Logo</label>
                                    <input type="file" id="upload" name="logo" class="form-control" accept="image/*">
                                    <input type="hidden" name="cropped_logo" id="base64Image">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-lg btn-info">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/croppie/image-to-base64.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginForm").validate({
                rules: {
                    "email": {
                        required: true,
                        email: true
                    },
                    "password": "required"
                },
                messages: {
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "password": "Ce champ est obligatoire."
                }
            });

            $("#serviceForm").validate({
                rules: {
                    "logo": "required"
                },
                messages: {
                    "logo": "Ce champ est obligatoire."
                }
            });

            var token = $('form.form-inline').find('input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': token
                }
            });

            $(document).on('click', '.btn-add', function (event) {
                event.preventDefault();

                var field = $(this).closest('.form-group');
                var field_new = field.clone();

                $(this)
                    .toggleClass('btn-default')
                    .toggleClass('btn-add')
                    .toggleClass('btn-danger')
                    .toggleClass('btn-remove')
                    .html('–');

                field_new.find('input').val('');
                field_new.insertAfter(field);

                field_new.find('select').click(function () {
                    $("#service-error").remove();
                });
            });

            $('.description').click(function () {
                $("#service-error").remove();
            });

            $(document).on('click', '.btn-remove', function (event) {
                event.preventDefault();
                $(this).closest('.form-group').remove();
            });

            $('#upload').on('change', function () {
                var reader = new FileReader();
                reader.addEventListener("load", function () {
                    $('#base64Image').val(reader.result);
                    $('#preview').src = reader.result;
                }, false);
                reader.readAsDataURL(document.getElementById('upload').files[0]);
            });
        });
    </script>
@endsection
