@extends('layouts.app')

@section('title', 'e-Afrika')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company) }}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>
                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img src="{{ asset('assets/images/icon-transparent.png') }}"/>
                                            Mes Connexions ></strong>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <table class="table dashboard-block">
                    <tbody>
                    <tr>
                        <td class="text-center">
                            <h4>My e-Afrika</h4>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img
                                src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"/>
                            <div>
                                <span class="edit-picture"><a href="{{ route('company.search', $company) }}"><i
                                            class="fa fa-pencil-square"></i> Modifier</a></span>
                            </div>
                            <div>
                                <strong>{{ $company->name }}</strong>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="icon-title">
                                <span class="edit-user"><a href="{{route('user.profile.update')}}"><img
                                            src="{{ asset('assets/images/icon-user.png') }}"/> Mon Profil</a></span>
                                <span class="edit-picture"><a href="{{route('user.profile.update')}}"><i
                                            class="fa fa-pencil-square"></i> Modifier</a></span>
                            </div>
                            <div>
                                <span>Membre depuis {{ (new DateTime(\Illuminate\Support\Facades\Auth::user()->created_at))->format("d-m-Y") }}</span>
                            </div>
                            <ul>
                                <li><strong>Mon Entreprise:</strong> {{ $company->name}}</li>
                                <li><strong>Cell:</strong> {{ \Illuminate\Support\Facades\Auth::user()->phone }}</li>
                                <li><strong>E-mail:</strong> {{ \Illuminate\Support\Facades\Auth::user()->email }}</li>
                                <li><strong>Adresse:</strong> {{ \Illuminate\Support\Facades\Auth::user()->address }}
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="icon-title">
                                <span class="edit-user"><a href="{{ route('company.detail', $company) }}"><img
                                            src="{{ asset('assets/images/icon-page.png') }}"/> Ma Page e-Afrika</a></span>
                                <span class="edit-picture"><a href="{{ route('company.detail', $company) }}"><i
                                            class="fa fa-pencil-square"></i> Modifier</a></span>
                            </div>
                            <div>
                                <span>Basic e-Connect</span>
                            </div>
                        </td>
                    </tr>
                    {{--  <tr>
                          <td>
                              <div class="icon-title">
                                  <span class="edit-user"><a href="{{ route('company.search',$company) }}"><img
                                              src="{{ asset('assets/images/icon-dashboard.png') }}"/> Mon Tableau de Board</a></span>
                              </div>
                          </td>
                      </tr>--}}
                    <tr>
                        <td>
                            <div class="icon-title">
                                @if ($company == $company)
                                    <span class="edit-user"><a href="{{ route('company.activities') }}"><img
                                                src="{{ asset('assets/images/icon-order.png') }}"/> Mes Commandes, Paiements et Alertes</a></span>
                                @else
                                    <span class="edit-user"><a href="{{ route('company.activities') }}"><img
                                                src="{{ asset('assets/images/icon-order.png') }}"/> Mes Commandes, Paiements et Alertes</a></span>
                                @endif
                            </div>
                            <ul>
                                <li><strong>Passer Commande</strong></li>
                                <li><strong>Paiements</strong></li>
                                <li><strong>Mon Historique</strong></li>
                                <li><strong>Mes Alertes</strong></li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="icon-title">
                                <span class="edit-user">
                                    <a href="#">
                                        <img src="{{ asset('assets/images/actualite.png') }}"/> Suggestions</a>
                                </span>

                            </div>
                            <ul>
                                <li><a href="{{ route('company.search',$company) }}">Clients</a></li>
                                <li><a href="{{ route('company.search',$company) }}">Partenaires</a></li>
                                <li><a href="{{ route('company.search',$company) }}">Fournisseurs</a></li>
                                <li><a href="{{ route('company.search',$company) }}">Concurrents</a></li>


                            </ul>
                        </td>
                    </tr>
                    @if (\Illuminate\Support\Facades\Auth::user()->isAdmin())
                        <tr>
                            <td>
                                <div class="icon-title">
                                    <span class="edit-user"><a href="{{ route('company.search', $company) }}"><img
                                                src="{{ asset('assets/images/icon-website.png') }}"/> Newsletter</a></span>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <table class="table dashboard-block">
                    <tbody>
                    <tr>
                        <td>
                            <div>
                                <span class="edit-user"><strong>Passer et Réceptionner une Commande</strong></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="container chat-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-collapse collapse in" id="collapseOne">
                                                <div class="panel-body">
                                                    <ul class="chat" id="output-chat">
                                                        <li class="left clearfix receiver-message hidden">
                                                        <span class="chat-img pull-left">
                                                            <img
                                                                src="{{ url('/public/images/entreprises') }}/{{ $company->id }}/{{ $company->logo }}"
                                                                alt="User Avatar" class=""/>
                                                        </span>
                                                            <div class="chat-body clearfix">
                                                                <div class="header">
                                                                    <strong class="primary-font">Jack Sparrow</strong>
                                                                    <small class="pull-right text-muted">
                                                                        <span class="glyphicon glyphicon-time"></span>
                                                                    </small>
                                                                </div>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                    elit.
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li class="right clearfix sender-message hidden">
                                                        <span class="chat-img pull-right">
                                                            <img
                                                                src="{{ url('/public/images/entreprises') }}/{{ $company->id }}/{{ $company->logo }}"
                                                                alt="User Avatar" class=""/>
                                                        </span>
                                                            <div class="chat-body clearfix">
                                                                <div class="header">
                                                                    <small class=" text-muted"><span
                                                                            class="glyphicon glyphicon-time"></span>
                                                                    </small>
                                                                    <strong
                                                                        class="pull-right primary-font">{{ $company->name }}</strong>
                                                                </div>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing
                                                                    elit.
                                                                </p>
                                                            </div>
                                                        </li>
                                                        {{--@foreach ($messages as $message)
                                                            @if($message->entreprise_id == $company->id)
                                                                <li class="left clearfix receiver-message">
                                                                <span class="chat-img pull-left">
                                                                    <img src="{{ url('/public/images/entreprises') }}/{{ $company->id }}/{{ $company->logo }}" alt="User Avatar" class="" />
                                                                </span>
                                                                    <div class="chat-body clearfix">
                                                                        <div class="header">
                                                                            <strong class="primary-font">{{ $company->nom }}</strong> <small class="pull-right text-muted">
                                                                                <span class="glyphicon glyphicon-time"></span></small>
                                                                        </div>
                                                                        <p>{{ $message->contenu }}</p>
                                                                    </div>
                                                                </li>
                                                            @else
                                                                <li class="right clearfix sender-message">
                                                                <span class="chat-img pull-right">
                                                                    <img src="{{ url('/public/images/entreprises') }}/{{ $company->id }}/{{ $company->logo }}" alt="User Avatar" class="" />
                                                                </span>
                                                                    <div class="chat-body clearfix">
                                                                        <div class="header">
                                                                            <small class=" text-muted"><span class="glyphicon glyphicon-time"></span></small>
                                                                            <strong class="pull-right primary-font">{{ $company->nom }}</strong>
                                                                        </div>
                                                                        <p>{{ $message->contenu }}</p>
                                                                    </div>
                                                                </li>
                                                            @endif
                                                        @endforeach--}}
                                                    </ul>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="input-group">
                                                        <input id="input-chat" type="text" class="form-control input-sm"
                                                               placeholder="Ecrivez un message ici ..."
                                                               data-id="{{ $company->id }}"
                                                               data-name="{{ $company->name }}"/>
                                                        <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-sm"
                                                                id="btn-chat">Envoyer</button>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table dashboard-block">
                    <tbody>
                    <tr>
                        <td>
                            <div>
                                <span class="edit-user"><strong>Mes Paiements</strong></span>

                            </div>
                        </td>
                        <td>
                            <form method="post" action="{{route('company.payment.start')}}">
                                @csrf
                                <input type="hidden" name="cmd" value="start"> <!--
Cette Valeur est à ne pas changer et elle est Obligatoire -->
                                <input type="hidden" name="rN"
                                       value="{{\Illuminate\Support\Facades\Auth::user()->name}}"> <!-- le
nom de votre client qui effectue le paiement. c'est facultatif. -->
                                <input type="hidden" name="rT"
                                       value="{{\Illuminate\Support\Facades\Auth::user()->phone}}">
                                <!-- Numéro Téléphone du client qui effectue le paiement (Obligatoire) -->
                                <input type="hidden" name="rE"
                                       value="{{\Illuminate\Support\Facades\Auth::user()->email}}">
                                <!-- Adresse email du client qui effectue le paiement. c'est facultatif. -->

                                <input type="hidden" name="rI" value="XXXX"> <!-- Le
numéro de votre commande. Si votre système ne gère pas de numéro de commande, vous pouvez enlevez ce champs.
c'est facultatif. -->
                                <input type="hidden" name="rOnly" value="1, 2, 5, 16, 17">
                                <!-- Ceci est optionnel. Si vous souhaitez que votre API n’affiche que certains opérateurs, vous pouvez
                                préciser ces opérateurs ici. 1=MTN, 2=Orange, 3=Express Union, 5=Visa via UBA, 10=Dohone, 14= Visa via Wari,
                                15=Wari card,16=VISA/MASTERCARD, 17=YUP. -->
                                <input type="hidden" name="rLocale" value="fr">
                                <!-- le choix de la langue. fr ou en -->
                                <input type="hidden" name="source" value="e-afrika">
                                <!-- Le nom commercial de votre site (Obligatoire) -->
                                <input type="hidden" name="endPage" value="https://www.e-afrika.fr">
                                <!-- Adresse de redirection en cas de SUCCESS de paiement (Obligatoire) -->
                                <input type="hidden" name="notifyPage" value="https://www.e-afrika.fr">
                                <!-- Adresse de notification automatique de votre site en cas de succès de paiement -->
                                <input type="hidden" name="cancelPage" value="https://www.e-afrika.fr">
                                <!-- Adresse de redirection en cas de Annulation de paiement par le client -->
                                <input type="hidden" name="logo" value="{{asset('assets/images/logo-entreprise.png')}}">
                                <!--une adresse url menant au logo de votre site si vous voulez voir apparaitre ce logo pendant le
                                paiement (Facultatif) -->
                                <input type="hidden" name="motif" value="Activation du compte entreprise e-afrika">
                                <!-- le motif est facultatif. Si il est précisé, il sera inscrit dans votre historique DOHONE version
                                excel. Ceci peut être important pour votre comptabilité. -->
                                <button class="btn btn-primary">Payer l'abonnement</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <span class="edit-user"><strong>Historique des Paiements</strong></span>
                            </div>
                            <ul>
                                @forelse($company->packages as $package)
                                    <li>Abonnement {{ (new DateTime($package->buy_at))->format("d-m-Y") }}
                                        à hauteur de {{$package->devise}} {{$package->amount}}
                                        @if($package->verify_at != null)
                                            Payé le {{(new DateTime($package->verify_at))->format("d-m-Y")}}
                                        @else
                                            <span class="alert-danger">en attente de confirmation</span>
                                        @endif
                                    </li>
                                @empty
                                    <li>Aucune abonnement payé</li>
                                @endforelse
                            </ul>
                        </td>
                    </tr>
                    {{-- <tr>
                         <td>
                             <div>
                                 <span class="edit-user"><strong>Factures</strong></span>
                             </div>
                             <ul>
                                 <li><strong>Facture du 01/04/2018</strong> <a href="#"><i class="fa fa-download"></i>
                                         Télécharger</a></li>
                                 <li><strong>Facture du 01/04/2018</strong> <a href="#"><i class="fa fa-download"></i>
                                         Télécharger</a></li>
                                 <li><strong>Facture du 01/04/2018</strong> <a href="#"><i class="fa fa-download"></i>
                                         Télécharger</a></li>
                             </ul>
                         </td>
                     </tr>--}}
                    </tbody>
                </table>
            </div>
            {{-- <div class="col-md-3">
                 <table class="table dashboard-block">
                     <tbody>
                     <tr>
                         <td>
                             <div>
                                 <span class="edit-user"><strong>Mes Alertes</strong></span>
                             </div>
                         </td>
                     </tr>
                     <tr>
                         <td>
                             <div>
                                 <span class="edit-user"><strong>Être informé(e) de:</strong></span>
                             </div>
                             <form class="form-inline" role="form">
                                 <div class="form-group">
                                     <div class="col-md-12">
                                         <input type="checkbox" value="">
                                         <label for="firstname" class="control-label">Nouvelle Entreprise</label>
                                         <select class="form-control pull-right">
                                             <option>Tous</option>
                                         </select>
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <div class="col-md-12">
                                         <input type="checkbox" value="">
                                         <label for="firstname" class="control-label">Actualités</label>
                                         <select class="form-control pull-right">
                                             <option>Tous</option>
                                         </select>
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <div class="col-md-12">
                                         <input type="checkbox" value="">
                                         <label for="firstname" class="control-label">Nouveautés sur le site</label>
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <div class="col-md-12">
                                         <input type="checkbox" value="">
                                         <label for="firstname" class="control-label">E-mail</label>
                                         <input type="checkbox" value="">
                                         <label for="firstname" class="control-label">Whatsapp / SMS</label>
                                     </div>
                                 </div>
                             </form>
                         </td>
                     </tr>
                     </tbody>
                 </table>
             </div>--}}
        </div>
    </div>
@endsection
