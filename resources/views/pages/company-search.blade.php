@extends('layouts.app')

@section('title', 'Result')


@section('content')

    <div class="container">

        <div class="row container-header">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-6 col-md-offset-3">
                                <form action="{{ $route??route('company.search') }}" class="form-inline search-form" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k" id="search"
                                               placeholder="Recherche sur e-Afrika">
                                        <ul id="log" class="ui-widget-content"></ul>
                                    </div>
                                </form>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row grid-divider col-sm-12">
            <div class="col-sm-7 col-sm-offset-2">
                <div class="col-padding block-recherche">
                    <h3><span class="glyphicon glyphicon-search form-control-feedback"></span>
                        Environ {{($companies->total()<10?'0'.$companies->total():$companies->total())}} résultats
                        trouvé(s) pour <strong>"{{request('k')}}"</strong>, Page {{ $companies->currentPage() }}</h3>
                    <table>
                        @if ($companies->total()>0)
                            @foreach ($companies as $company)
                                <tr>
                                    <td width="10%">
                                        <a href="{{route('company.detail',$company)}}">
                                            <img
                                                src="{{asset("assets/system-images/companies/".$company->id)."/".$company->logo}}"
                                                alt="{{$company->name}}"
                                                width="160px" height="85px"/></a>
                                        <div>
                                            <p>
                                                <a href="{{route('company.detail',$company)}}"><strong>{{ strtoupper($company->name) }}</strong></a>
                                            </p>
                                            @if($company->activity!=null)
                                                <p>{{$company->activity->label}}</p>
                                            @else
                                                <p class="text-animated text-bomb" style="color: red"><i
                                                        class="fa fa-bomb"></i> {{$company->id}}
                                                </p>
                                            @endif
                                            <p>{{$company->town}}</p>
                                            <p><a href="{{route('company.detail',$company)}}"><i class="fa fa-eye"></i>
                                                    En savoir plus ...</a></p>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td>
                                {{$companies->links('vendor.pagination.default')}}
                            </td>
                        </tr>
                    </table>


                </div>
            </div>
        </div>

    </div>
@endsection


