@extends('layouts.app')

@section('title', 'e-Afrika')

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {


            $("#entrepriseForm").validate({
                rules: {
                    "name": "required",
                    "main_activity": "required",
                    "legal_status_id": "required",
                    "main_activity_id": "required",
                    "activities[]": "required",
                    "founded_at": "required",
                    "size": "required",
                    "country_id": "required",
                    "town": "required",
                    "phone": "required",
                    "email": {
                        required: true,
                        email: true
                    },
                    "location": "required",
                    "zone": "required",
                    "agencies": "required",
                    "business_turnover": "required"
                },
                messages: {
                    "name": "Ce champ est obligatoire.",
                    "main_activity": "Ce champ est obligatoire.",
                    "legal_status_id": "Ce champ est obligatoire.",
                    "main_activity_id": "Ce champ est obligatoire.",
                    "activities[]": "Ce champ est obligatoire.",
                    "founded_at": "Ce champ est obligatoire.",
                    "size": "Ce champ est obligatoire.",
                    "country_id": "Ce champ est obligatoire.",
                    "town": "Ce champ est obligatoire.",
                    "phone": "Ce champ est obligatoire.",
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "location": "Ce champ est obligatoire.",
                    "zone": "Ce champ est obligatoire.",
                    "agencies": "Ce champ est obligatoire.",
                    "business_turnover": "Ce champ est obligatoire."
                }
            });

            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'fr'
            });

            $('.btn-circle').on('click', function () {
                $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                $(this).addClass('btn-info').removeClass('btn-default').blur();
            });

            $(document).on('click', '.btn-add', function (event) {
                event.preventDefault();

                var field = $(this).closest('.form-group');
                var field_new = field.clone();

                $(this)
                    .toggleClass('btn-default')
                    .toggleClass('btn-add')
                    .toggleClass('btn-danger')
                    .toggleClass('btn-remove')
                    .html('–');

                field_new.find('input').val('');
                field_new.insertAfter(field);

                field_new.find('select').click(function () {
                    $("#service-error").remove();
                });
            });

            $('.services').click(function () {
                $("#service-error").remove();
            });

            $(document).on('click', '.btn-remove', function (event) {
                event.preventDefault();
                $(this).closest('.form-group').remove();
            });

            $(document).on('click', '.data-log', function (event) {
                event.preventDefault();
                $("#nom").val($(this).text());
            });

            var active_form = {
                form_2: false,
                form_3: false,
                form_4: false,
                form_5: false,
                form_6: false,
                form_7: false
            };

            $('.next-step, .prev-step').on('click', function (e) {
                var $activeTab = $('.tab-pane.active');


                if ($(e.target).hasClass('next-step')) {
                    var nextTab = $activeTab.next('.tab-pane').attr('id');
                    switch (nextTab) {
                        case "menu2" :
                            if ($('#entrepriseForm input[name="name"]').valid() && $('#entrepriseForm select[name="legal_status_id"]').valid() && $('#entrepriseForm select[name="main_activity_id"]').valid() && $('#entrepriseForm select[name="activities[]"]').valid() && $('#entrepriseForm input[name="founded_at"]').valid() && $('#entrepriseForm select[name="size"]').valid() && $('#entrepriseForm select[name="country_id"]').valid() && $('#entrepriseForm input[name="town"]').valid() && $('#entrepriseForm input[name="phone"]').valid() && $('#entrepriseForm input[name="email"]').valid() && $('#entrepriseForm input[name="location"]').valid() && $('#entrepriseForm select[name="zone"]').valid() && $('#entrepriseForm input[name="agencies"]').valid()) {
                                active_form.form_2 = true;
                                if (active_form.form_2 && active_form.form_3 && active_form.form_4 && active_form.form_5 && active_form.form_6 && active_form.form_7) {
                                    $('#entrepriseForm button[type="submit"].btn-success').removeAttr('disabled');
                                }

                                $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                                $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                                $('[href="#' + nextTab + '"]').tab('show');
                            }
                            break;
                        case "menu3" :
                            if ($('#entrepriseForm input[name="services[]"]').valid()) {
                                active_form.form_3 = true;
                                if (active_form.form_2 && active_form.form_3 && active_form.form_4 && active_form.form_5 && active_form.form_6 && active_form.form_7) {
                                    $('#entrepriseForm button[type="submit"].btn-success').removeAttr('disabled');
                                }

                                /* declare an checkbox array */
                                var servicesArray = [];
                                var produitsArray = [];

                                /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
                                $(".services").each(function () {
                                    servicesArray.push($(this).val());
                                });
                                $(".produits").each(function () {
                                    produitsArray.push($(this).val());
                                });

                                /* we join the array separated by the comma */
                                var services;
                                var produits;
                                services = servicesArray.join(';');
                                produits = produitsArray.join(';');

                                var n_services = services.split(';').length;
                                if (services.split(';')[n_services - 1] != '') {
                                    $('input[name="serv"]').val(services);
                                    $('input[name="prod"]').val(produits);
                                    $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                                    $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                                    $('[href="#' + nextTab + '"]').tab('show');
                                } else {
                                    $("#service-error").remove();
                                    $(".form-services").append('<label id="service-error" class="error" for="description">Ce champ est obligatoire.</label>');
                                    return false;
                                }
                            }
                            break;
                        case "menu4" :
                            active_form.form_4 = true;
                            if (active_form.form_2 && active_form.form_3 && active_form.form_4 && active_form.form_5 && active_form.form_6 && active_form.form_7) {
                                $('#entrepriseForm button[type="submit"].btn-success').removeAttr('disabled');
                            }

                            /* declare an checkbox array */
                            var partenairesArray = [];
                            var clientsArray = [];

                            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
                            $(".partenaires").each(function () {
                                partenairesArray.push($(this).val());
                            });
                            $(".clients").each(function () {
                                clientsArray.push($(this).val());
                            });

                            /* we join the array separated by the comma */
                            var partenaires;
                            var clients;
                            partenaires = partenairesArray.join(';');
                            clients = clientsArray.join(';');
                            $('input[name="part"]').val(partenaires);
                            $('input[name="clt"]').val(clients);

                            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                            $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                            $('[href="#' + nextTab + '"]').tab('show');
                            break;
                        case "menu5" :
                            if ($('#entrepriseForm select[name="business_turnover"]').valid()) {
                                active_form.form_5 = true;
                                if (active_form.form_2 && active_form.form_3 && active_form.form_4 && active_form.form_5 && active_form.form_6 && active_form.form_7) {
                                    $('#entrepriseForm button[type="submit"].btn-success').removeAttr('disabled');
                                }

                                $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                                $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                                $('[href="#' + nextTab + '"]').tab('show');
                            }
                            break;
                        case "menu6" :
                            active_form.form_6 = true;
                            if (active_form.form_2 && active_form.form_3 && active_form.form_4 && active_form.form_5 && active_form.form_6 && active_form.form_7) {
                                $('#entrepriseForm button[type="submit"].btn-success').removeAttr('disabled');
                            }

                            /* declare an checkbox array */
                            var atoutsArray = [];
                            var realisationsArray = [];

                            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
                            $(".atouts").each(function () {
                                atoutsArray.push($(this).val());
                            });
                            $(".realisations").each(function () {
                                realisationsArray.push($(this).val());
                            });

                            /* we join the array separated by the comma */
                            var atouts;
                            var realisations;
                            atouts = atoutsArray.join(';');
                            realisations = realisationsArray.join(';');
                            $('input[name="ats"]').val(atouts);
                            $('input[name="real"]').val(realisations);

                            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                            $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                            $('[href="#' + nextTab + '"]').tab('show');
                            break;
                        case "menu7" :
                            active_form.form_7 = true;
                            if (active_form.form_2 && active_form.form_3 && active_form.form_4 && active_form.form_5 && active_form.form_6 && active_form.form_7) {
                                $('#entrepriseForm button[type="submit"].btn-success').removeAttr('disabled');
                            }

                            /* declare an checkbox array */
                            var informationsArray = [];

                            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
                            $(".informations").each(function () {
                                informationsArray.push($(this).val());
                            });

                            /* we join the array separated by the comma */
                            var informations;
                            informations = informationsArray.join(';');
                            $('input[name="info"]').val(informations);

                            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                            $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                            $('[href="#' + nextTab + '"]').tab('show');
                            break;
                    }
                } else {
                    var prevTab = $activeTab.prev('.tab-pane').attr('id');
                    $('[href="#' + prevTab + '"]').addClass('btn-info').removeClass('btn-default');
                    $('[href="#' + prevTab + '"]').tab('show');
                }
            });

            $('#adresse_localisation').geocomplete({details: "form"});

            $('#nom').bind('blur', function (e) {
                setTimeout(function () {
                    $("#log").empty();
                    $("#log").hide();
                }, 200);
            });

            $('#nom').keyup(function (e) {
                if (e.keyCode == 13 || e.keyCode == 27) {
                    // Enter pressed... do anything here...
                    $("#log").empty();
                    $("#log").hide();
                } else {
                    if ($(this).val() == '') {
                        // Enter pressed... do anything here...
                        $("#log").empty();
                        $("#log").hide();
                    }
                }
            });

            $("#nom").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '{{ route("company.search.prediction") }}',
                        type: 'GET',
                        dataType: 'JSON',
                        data: {q: request.term}
                    }).done(function (data) {
                        $("#log").empty();
                        $("#log").show();
                        var n = data.q.length;
                        for (var i = 0; i < n; i++) {
                            log(data.q[i]);
                        }
                        console.log('Response data: ', data);
                    }).fail(function (jqXHR, textStatus) {
                        console.log('Request failed: ' + textStatus);
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    log(ui.item ?
                        "Selected: " + ui.item.label :
                        "Nothing selected, input was " + this.value);
                },
                open: function () {
                    $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });

            function log(data) {
                $("<li class='data-log'>").html(data.nom).prependTo("#log");
            }
        });
    </script>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{route('company.detail',$company)}}"><img
                                        src="{{ asset('assets/images/logo.png') }}"
                                        alt="User Avatar"></a>
                            </div>

                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li><strong class="text-info">
                                            <img
                                                src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"/>
                                            Modifier GRATUITEMENT L'ESPACE "e-Afrika" DE VOTRE ENTREPRISE</strong>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @include('block.company.form.update')
    </div>
@endsection
