@extends('layouts.app')

@section('title', 'e-Afrika')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company)}}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>

                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img
                                                src="{{ asset('assets/images/icon-transparent.png') }}"/>{{ strtoupper($company->name) }}
                                            ></strong></li>

                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <form action={{ route('contact.company',$target_company) }} method="post" role="form">
            @csrf
            <div class="row row-social row-contact">
                <div class="col-md-4 col-md-offset-3">
                    <div class="form-group">
                        <strong>De:</strong>
                        <input type="email" class="form-control" id="mobile" name="sender"
                               value="{{ $company->email }}" required
                               style="height: 29px;" readonly="true">

                    </div>
                    <div class="form-group">
                        <strong>A:</strong><input type="email" class="form-control" id="subject" name="receiver"
                                                  value="{{ $target_company->email }}" readonly="true"
                                                  style="height: 29px;">

                    </div>
                </div>
            </div>

            <div class="row row-social row-contact">
                <div class="col-md-4 col-md-offset-3">
                    <strong>Message:</strong><textarea class="form-control" type="textarea" id="message" name="message"
                                                       maxlength="140" rows="7" style="resize: none;">Monsieur,Madame  votre société a attiré mon attention sur e-Afrika.com.Aussi je souhaiterai échanger avec vous de possibles opportunités.</textarea>

                </div>

                <div class="col-md-4">
                    <div class="row" style="margin-top: 30px;">

                        <div class="radio">
                            <label><input name="optradio" type="radio">Comme Pontentiel Client</label>
                        </div>
                        <div class="radio">
                            <label><input name="optradio" type="radio">Comme Pontentiel Fournisseur</label>
                        </div>
                        <div class="radio">
                            <label><input name="optradio" type="radio">Comme Pontentiel Partenaire</label>
                        </div>
                        <div class="radio">
                            <label><input name="optradio" type="radio">Comme Pontentiel Investisseurs</label>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-4 col-md-offset-3">
                        <button type="submit" class="btn btn-default"><i class="fa fa-play"></i>Envoyer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
