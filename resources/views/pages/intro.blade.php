@extends('layouts.app')

@section('title', 'e-Afrika News')

@section('content')

    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="video">
                    <iframe class="embed-responsive-item"
                            src="https://www.youtube.com/embed/uZEl798efCw?rel=0&showinfo=0" frameborder="1"
                            allowfullscreen></iframe>
                </div>
            </div>
            <h3>Le Monde Change</h3>
        </div>
    </div>
@endsection
