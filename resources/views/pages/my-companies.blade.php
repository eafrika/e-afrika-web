@extends('layouts.app')

@section('title', 'e-Afrika')

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginForm").validate({
                rules: {
                    "email": {
                        required: true,
                        email: true
                    },
                    "password": "required"
                },
                messages: {
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "password": "Ce champ est obligatoire."
                }
            });

            var token = $('form.form-inline').find('input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': token
                }
            });

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".connect-title").each(function () {
                var social_id = $(this).attr('data-id');
                $.ajax({
                    url: '{{ route("company.search") }}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {social_id: social_id}
                }).done(function (data) {
                    $('#connect-' + social_id).text($('#connect-' + social_id).text() + ' (' + data.total + ')');
                    console.log('Response data: ', data);
                }).fail(function (jqXHR, textStatus) {
                    console.log('Request failed: ' + textStatus);
                });
            });
        });
    </script>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('company.detail', $company) }}"><img
                                        src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"
                                        alt="User Avatar"></a>
                            </div>
                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li><strong><img src="{{ asset('assets/images/icon-transparent.png') }}"/>Mes
                                            Entreprises ></strong></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <table class="table dashboard-block">
                    <tbody>
                    <tr>
                        <td class="text-center">
                            <h4>My e-Afrika</h4>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img
                                src="{{ asset('assets/system-images/companies') }}/{{ $company->id }}/{{ $company->logo }}"/>
                            <div>
                                <span class="edit-picture"><a href="{{ route('company.search', $company) }}"><i
                                            class="fa fa-pencil-square"></i> Modifier</a></span>
                            </div>
                            <div>
                                <strong>{{ $company->name }}</strong>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="icon-title">
                                <span class="edit-user"><a href="{{route('user.profile.update')}}"><img
                                            src="{{ asset('assets/images/icon-user.png') }}"/> Mon Profil</a></span>
                                <span class="edit-picture"><a href="{{route('user.profile.update')}}"><i
                                            class="fa fa-pencil-square"></i> Modifier</a></span>
                            </div>
                            <div>
                                <span>Membre depuis {{ (new DateTime(\Illuminate\Support\Facades\Auth::user()->created_at))->format("d-m-Y") }}</span>
                            </div>
                            <ul>
                                <li><strong>Mon Entreprise:</strong> {{ $company->name}}</li>
                                <li><strong>Cell:</strong> {{ \Illuminate\Support\Facades\Auth::user()->phone }}</li>
                                <li><strong>E-mail:</strong> {{ \Illuminate\Support\Facades\Auth::user()->email }}</li>
                                <li><strong>Adresse:</strong> {{ \Illuminate\Support\Facades\Auth::user()->address }}
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="icon-title">
                                <span class="edit-user"><a href="{{ route('company.detail', $company) }}"><img
                                            src="{{ asset('assets/images/icon-page.png') }}"/> Ma Page e-Afrika</a></span>
                                <span class="edit-picture"><a href="{{ route('company.detail', $company) }}"><i
                                            class="fa fa-pencil-square"></i> Modifier</a></span>
                            </div>
                            <div>
                                <span>Basic e-Connect</span>
                            </div>
                        </td>
                    </tr>
                    {{--  <tr>
                          <td>
                              <div class="icon-title">
                                  <span class="edit-user"><a href="{{ route('company.search',$company) }}"><img
                                              src="{{ asset('assets/images/icon-dashboard.png') }}"/> Mon Tableau de Board</a></span>
                              </div>
                          </td>
                      </tr>--}}
                    <tr>
                        <td>
                            <div class="icon-title">
                                @if ($company == $company)
                                    <span class="edit-user"><a href="{{ route('company.activities') }}"><img
                                                src="{{ asset('assets/images/icon-order.png') }}"/> Mes Commandes, Paiements et Alertes</a></span>
                                @else
                                    <span class="edit-user"><a href="{{ route('company.activities') }}"><img
                                                src="{{ asset('assets/images/icon-order.png') }}"/> Mes Commandes, Paiements et Alertes</a></span>
                                @endif
                            </div>
                            <ul>
                                <li><strong>Passer Commande</strong></li>
                                <li><strong>Paiements</strong></li>
                                <li><strong>Mon Historique</strong></li>
                                <li><strong>Mes Alertes</strong></li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="icon-title">
                                <span class="edit-user">
                                    <a href="#">
                                        <img src="{{ asset('assets/images/actualite.png') }}"/> Suggestions</a>
                                </span>

                            </div>
                            <ul>
                                <li><a href="{{ route('company.search',$company) }}">Clients</a></li>
                                <li><a href="{{ route('company.search',$company) }}">Partenaires</a></li>
                                <li><a href="{{ route('company.search',$company) }}">Fournisseurs</a></li>
                                <li><a href="{{ route('company.search',$company) }}">Concurrents</a></li>


                            </ul>
                        </td>
                    </tr>
                    @if (\Illuminate\Support\Facades\Auth::user()->isAdmin())
                        <tr>
                            <td>
                                <div class="icon-title">
                                    <span class="edit-user"><a href="{{ route('company.search', $company) }}"><img
                                                src="{{ asset('assets/images/icon-website.png') }}"/> Newsletter</a></span>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col-md-9 dashboard-padding">
                <div class="col-md-9">
                    <div class="col-padding block-recherche">
                        <h3><span class="glyphicon glyphicon-search form-control-feedback"></span>
                            Environ {{($companies->total()<10?'0'.$companies->total():$companies->total())}} résultats
                            trouvé(s), Page {{ $companies->currentPage() }}</h3>
                        <table>
                            @if ($companies->total()>0)
                                @foreach ($companies as $company)
                                    <tr>
                                        <td width="10%">
                                            <a href="{{route('company.detail',$company)}}">
                                                <img
                                                    src="{{asset("assets/system-images/companies/".$company->id)."/".$company->logo}}"
                                                    alt="{{$company->name}}"
                                                    width="160px" height="85px"/></a>
                                            <div>
                                                <p>
                                                    <a href="{{route('company.detail',$company)}}"><strong>{{ strtoupper($company->name) }}</strong></a>
                                                </p>
                                                @if($company->activity!=null)
                                                    <p>{{$company->activity->label}}</p>
                                                @else
                                                    <p class="text-animated text-bomb" style="color: red"><i
                                                            class="fa fa-bomb"></i> {{$company}}
                                                    </p>
                                                @endif
                                                <p>{{$company->town}}</p>
                                                <p><a href="{{ route('company.update', $company) }}">
                                                        <i class="fa fa-pencil-square"></i> Modifier ...</a>
                                                    <a href="{{ route('upload-logo', $company) }}">
                                                        <i class="fa fa-image"></i> Ajouter un Logo ...</a>
                                                    <a href="{{ route('upload-banner', $company) }}">
                                                        <i class="fa fa-image"></i> Ajouter des Photos ...</a>
                                                    @if(\App\User::find(Auth::id())->companies()->where('main', true)->first()->id !== $company->id)
                                                        <a href="{{ route('company.default', $company) }}">
                                                            <i class="fa fa-pencil-square"></i> Définir par défaut
                                                            ...</a>
                                                    @endif
                                                </p>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr>
                                <td>
                                    {{$companies->links('vendor.pagination.default')}}
                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
