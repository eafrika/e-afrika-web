@extends('layouts.app')

@section('title', 'e-Afrika')

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginForm").validate({
                rules: {
                    "email": {
                        required: true,
                        email: true
                    },
                    "password": "required"
                },
                messages: {
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "password": "Ce champ est obligatoire."
                }
            });
        });
    </script>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table dashboard-block breadcrumb-block">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-md-2 text-center">
                                <a href="{{ route('welcome')}}"><img
                                        src="{{ asset('assets/images/logo.png') }}"
                                        alt="User Avatar"></a>
                            </div>

                            <div class="col-md-10">
                                <form action="{{ route('company.search') }}" class="form-inline" role="form"
                                      method="GET">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="k"
                                               placeholder="Recherche sur e-Afrika">
                                    </div>
                                </form>
                                <ul>
                                    <li>
                                        <strong>
                                            <img
                                                src="{{ asset('assets/images/icon-transparent.png') }}"/>{{ strtoupper('ALERTES ') }}
                                            ></strong></li>

                                </ul>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @include('block.newsletter.subscribe')
    </div>
@endsection
