@extends('layouts.app')

@section('title', "e-Afrika Conditions générales d'utilisation")

@section('content')

    <div class="container" style="width:85%; margin-top:35px; text-align:justify;">
        <article id="post-4199" class="post-4199 page type-page status-publish hentry">
            <header class="entry-header" style="text-align:center;margin-bottom: 2em;">
                <h1 class="entry-title">CONDITIONS GENERALES D’UTILISATION</h1>
            </header>
            <div class="entry-content post_content">
                <h4 class="term_m_b">VALIDITE ET DUREE DES CONDITIONS</h4>
                <p>Les règles et droits énoncés dans ce document sont applicables sur toute la durée de la
                    navigation et de l’accès de l’utilisateur à la plateforme et par toute la durée de l’utilisation du
                    service par lui.</p>
                <p>Il est à noter que ces conditions peuvent à tout moment faire l’objet de mises à jour ou
                    modifications liées aux contraintes économiques ou légales.</p>
                <h4 class="term_m_b">RESPONSABILITES</h4>
                <p>e-Afrika ne peut être tenue pour responsable de contenu que l’Utilisateur envoie via ce
                    Module et qui violerait les dispositions des présentes CGU et du droit applicable aux
                    présentes.</p>

                <p>Si l’Utilisateur transmet via ce Module un contenu en violation desdites dispositions, il sera
                    seul tenu pour responsable de cette violation à l'exclusion pleine et entière de la Société.</p>
                <p>La responsabilité de la Société à l'égard de l’Utilisateur ne peut être engagée qu’en cas
                    d’inexécution de ses engagements résultant des CGU.</p>
                <p>L’Utilisateur est parfaitement informé du simple rôle d’intermédiaire de la Société qui ne peut
                    être responsable des produits, biens ou services prestations proposées par les utilisateurs avec
                    lesquelles ils ont choisis de se mettre en relation d’affaire.</p>
                <p>L’Utilisateur a pleinement pris conscience des dispositions du présent article et notamment
                    des garanties et limitations de responsabilité susvisées, conditions essentielles sans lesquelles
                    la Société n’aurait jamais contracté.</p>
                <p>De la même manière, la Société n’est nullement responsable des liens figurant sur son Site
                    Internet vers d’autres sites Internet qui ne lui appartiennent pas et qui ne sont pas contrôlés
                    par elle, dès lors qu’elle n’exerce aucun contrôle sur le contenu, les chartes de protection des
                    données personnelles ou les pratiques des sites tiers.</p>
                <p>e-Afrika n’est pas responsable de la disponibilité de ces sites et ne peut en contrôler le
                    contenu ni valider la publicité, les produits et autres informations diffusées sur ces sites
                    Internet.</p>
                <h4 class="term_m_b">LIMITES DE NOTRE RESPONSABILITE</h4>

                <p>Relativement à la loi, e-Afrika décline toute responsabilité pour les pertes de bénéfices, de
                    revenus ou de données, ou les dommages et intérêts indirects, spéciaux, consécutifs,
                    exemplaires ou punitifs.</p>
                <p>Relativement à la loi, la responsabilité totale d’e-Afrika, pour toute réclamation dans le
                    cadre des présentes Conditions d’ Utilisation, y compris pour toute garantie implicite, est
                    limitée au montant que vous nous avez payé pour utiliser les Services</p>
                <p>Relativement à la loi sur l’économie numérique du 21 juin 2004, la plateforme e-Afrika est un
                    simple intermédiaire technique. De ce fait, elle ne peut pas voir sa responsabilité civile
                    engagée du fait des activités ou des informations stockées à la demande d'un utilisateur de ses
                    services si elle n'a pas eu connaissance de leur caractère illicite ou des faits et circonstances
                    faisant apparaître ce caractère illicite.</p>
                <p>En aucun cas, e-Afrika, ne sera tenue responsable pour toute perte ou dommage qui
                    n’aurait pas été raisonnablement prévisible.</p>
                <p>A travers la plateforme, les entreprises inscrites peuvent se voir attribuer des notes par des
                    fournisseurs, partenaires, clients sur le degré de satisfaction lié à la transaction commerciale
                    dont la plateforme a été l’instigatrice, ou basé sur leurs échanges antérieurs à la plateforme.
                    Ainsi, e-Afrika ne saurait garantir la véracité des notes attribuées à ces utilisateurs compte
                    tenu de ce que ces opérations commerciales ne se sont pas déroulées sur la plateforme.</p>
                <p>e-Afrika se désengage de la qualité du service acheté ou vendu par un utilisateur, son rôle se
                    limitant à la simple mise en relation de ces derniers. L’utilisateur de la plateforme s’engage
                    alors à ne pas se contenter des notes apparaissant sur le profil des utilisateurs, mais par
                    prudence de prendre des mesures adéquates pour se prémunir d’un service qui serait défaillant
                    ou non-livré.</p>


                <h4 class="term_m_b">UTILISATION DE NOS SERVICES</h4>
                <p>Si vous utilisez nos Services pour le compte d’une entreprise, cette dernière doit accepter
                    les présentes Conditions d’Utilisation. Elle doit en outre dégager de toute responsabilité e-
                    Afrika SA, ses agents et ses salariés et les garantir contre toute réclamation, poursuite ou
                    action en justice résultant de ou liée à son utilisation des Services ou faisant suite à une
                    violation des présentes Conditions d’Utilisation, y compris toute responsabilité et charge
                    financière résultant de réclamations, de pertes ou de dommages constatés, de poursuites
                    engagées et de jugements prononcés, et des frais de justice et d’avocat afférents.</p>

                <h4 class="term_m_b">MODIFICATION ET RESILIATION DE NOS SERVICES</h4>
                <p>e-Afrika n’a de cesse de modifier et d’améliorer ses Services. Nous sommes donc
                    susceptibles d’ajouter ou de supprimer des fonctionnalités ou des fonctions, et il peut
                    également arriver que nous suspendions ou interrompions complètement un Service.</p>
                <p>Vous pouvez cesser d’utiliser nos Services à tout moment. Nous espérons cependant que
                    vous continuerez de les utiliser. e-Afrika est en droit de cesser de vous fournir tout ou
                    partie des Services, ou d’ajouter ou de créer de nouvelles limites à l’utilisation des
                    Services et ce, à tout moment.</p>
                <p>Pour nous, vous restez propriétaire des données que vous nous confiez et nous pensons
                    qu’il est important que vous puissiez y accéder. Si nous devons interrompre un Service,
                    dans la mesure du possible, nous vous en avertissons dans un délai raisonnable et vous
                    donnons la possibilité de récupérer des informations de ce Service.</p>

                <h4 class="term_m_b">DESCRIPTION DU SERVICE</h4>

                <p>Le Service est fourni en l'état.</p>
                <p>La plateforme n’est tenue que par une obligation d’assistance personnalisée notamment
                    technique.</p>
                <p>Elle décline toute garantie expresse ou implicite notamment concernant la compatibilité du
                    Site à l’utilisation que l’Utilisateur en fera.</p>

                <p>Elle ne garantit pas davantage que les fichiers transmis par l’Utilisateur ne puissent pas faire
                    l’objet d’intrusions de tiers non autorisés ni être corrompus ou téléchargés ni encore que les
                    informations et les données circulant sur l’Internet sont protégées contre de telles attaques ou
                    des détournements éventuels. Elle ne garantit donc pas la confidentialité des messages
                    envoyés via entre utilisateurs.</p>
                <p>Le Service permet la recherche par géolocalisation, par produit, par bien ou service et par
                    consultation d'un profil. Une fois que l’utilisateur a identifié l’objet de sa recherche, il peut
                    entrer en contact avec son interlocuteur via le service de messagerie privée, par email ou par
                    téléphone.</p>

                <p>La navigation sur le Site Internet est libre ainsi que certaines fonctionnalités pendant
                    l'utilisation du Service.</p>
                <p>Cependant, en application des dispositions de la <strong>loi informatique et libertés du n°78-17 du 6
                        janvier 1978 </strong>, L'Utilisateur doit obligatoirement donner, de manière expresse, son
                    accord à la
                    collecte et au traitement par la Société ou ses partenaires de certaines données personnelles
                    dites comportementales nécessaires à la fourniture du Service et notamment à l'établissement
                    de statistiques concernant les mesures de la fréquentation de chaque profil et ce
                    conformément aux dispositions légales applicables. Ainsi, parmi les données collectées
                    figurent l'adresse IP de l'Utilisateur.</p>

                <p>L'utilisateur dispose d’un droit d’accès, de modification, de rectification et de suppression des
                    données qui le concernent.</p>

                <h4 class="term_m_b">UTILISATION DE NOS SERVICES</h4>
                <p>L’utilisation de nos Services ne vous confère aucun droit de propriété intellectuelle sur
                    nos Services ni sur les contenus auxquels vous accédez. Vous ne devez utiliser aucun
                    contenu obtenu par l’intermédiaire de nos Services sans l’autorisation du propriétaire
                    dudit contenu, à moins d’y être autorisé par la loi. Ces Conditions d’Utilisation ne vous
                    confèrent pas le droit d’utiliser une quelconque marque ou un quelconque logo présent
                    dans nos Services. Nous pouvons suspendre ou cesser la fourniture de nos Services si vous
                    ne respectez pas les conditions ou règlements applicables, ou si nous examinons une
                    suspicion d’utilisation impropre. Nous nous réservons le droit de supprimer ou de refuser
                    d’afficher tout contenu que nous estimons raisonnablement être en violation de la loi ou
                    de notre règlement. Le fait que nous nous réservions ce droit ne signifie pas
                    nécessairement que nous vérifions les contenus. Dès lors, veuillez ne pas présumer que
                    nous vérifions les contenus.</p>
                <p>Dans le cadre de votre utilisation des Services et de l’exécution de notre engagement
                    contractuel, nous sommes susceptibles de vous adresser des messages liés au
                    fonctionnement ou à l’administration des Services ainsi que d’autres informations. Vous
                    pouvez choisir de ne plus recevoir certains de ces messages.</p>

                <p>Certains de nos Services sont disponibles sur les appareils mobiles. Ne les utilisez pas
                    d’une manière susceptible de vous distraire et de vous empêcher de respecter le code de la
                    route et les règles de sécurité en matière de conduite.</p>

                <h4 class="term_m_b">PROTECTION DE LA VIE PRIVEE ET DES DONNEES PERSONNELLES</h4>
                <p>Les règles de confidentialité de e-Afrika expliquent comment nous traitons vos données à
                    caractère personnel et protégeons votre vie privée lors de votre utilisation de nos Services.
                    En utilisant nos Services, vous acceptez qu’e-Afrika puisse utiliser ces données
                    conformément à ces Règles de confidentialité.</p>
                <p>Nous répondons aux notifications d’atteinte présumée aux droits d’auteur en désactivant
                    les comptes des utilisateurs ayant plusieurs fois porté atteinte à ces droits, conformément à
                    la procédure établie par la <strong> loi américaine dénommée « Digital Millennium Copyright
                        Act » </strong>.</p>

                <h4 class="term_m_b">INFORMATIQUE ET LIBERTES (Données personnelles)</h4>

                <p>Afin d’assurer la fourniture du Service et permettre aux Utilisateurs d’être mis en relation
                    avec les autres utilisateurs répondant à leurs critères de recherche, la Société doit
                    nécessairement traiter les données personnelles de certains utilisateurs.</p>
                <p>A cet effet, e-Afrika informe les utilisateurs que sa plateforme utilise les cookies.</p>

                <p>Ainsi, grâce au traitement de leurs données personnelles, des personnes utilisatrices de la
                    plateforme ainsi que les non-utilisatrice pourront être mis en relation efficacement.</p>
                <p>Ces données peuvent être transmises auprès des prestataires techniques de la Société, dans la
                    seule finalité de la bonne exécution du Service, ou l’établissement de statistiques sur la
                    consultation des profils des utilisateurs.</p>
                <p>Dans tous les cas, l'Utilisateur dispose d'un droit d'accès, de modification, d'opposition et de
                    suppression des données personnelles le concernant en écrivant à l'adresse suivante : <strong>
                        e-Afrika, Nouvelle route Bastos Immeuble Air France BP 4790, Yaoundé Cameroun </strong></p>
                <h4 class="term_m_b">A PROPOS DE CES CONDITIONS D’UTILISATION</h4>

                <p>Nous sommes susceptibles de modifier ces Conditions d’Utilisation ou toute autre
                    condition d’utilisation complémentaire s’appliquant à un Service, par exemple, pour
                    refléter des modifications de la loi ou de nos Services. Nous vous recommandons de
                    consulter régulièrement les Conditions d’Utilisation. Les modifications apportées à ces
                    Conditions d’Utilisation seront signalées sur cette page. Nous publierons un avis de
                    modification des conditions d’utilisation additionnelles dans le Service concerné. Les
                    modifications ne s’appliqueront pas de façon rétroactive et entreront en vigueur au moins
                    quatorze (14) jours après leur publication. Toutefois, les modifications spécifiques à une
                    nouvelle fonctionnalité d’un service ou les modifications apportées pour des raisons
                    juridiques s’appliqueront immédiatement. Si vous n’acceptez pas les modifications
                    apportées aux Conditions d’Utilisation d’un service donné, vous devez cesser toute
                    utilisation de ce Service.</p>
                <p>En cas de conflit entre ces Conditions d’Utilisation et des conditions d’utilisation
                    additionnelles, ce sont ces dernières qui prévalent.</p>
                <p>Ces Conditions d’Utilisation régissent votre relation avec e-Afrika. Elles ne créent pas de
                    droit pour des tiers bénéficiaires.</p>
                <p>Si vous ne respectez pas ces Conditions d’Utilisation et que nous ne prenons pas
                    immédiatement de mesure à ce sujet, cela ne signifie pas que nous renonçons à nos droits
                    (par exemple, à prendre une mesure ultérieurement).</p>
                <p>S’il s’avère qu’une condition particulière n’est pas applicable, cela sera sans incidence sur
                    les autres conditions de ces Conditions d’Utilisation.</p>
            </div>
        </article>
    </div>
@endsection
