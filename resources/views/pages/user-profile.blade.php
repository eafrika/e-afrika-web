<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>S'inscrire</title>

    <!-- Fav Icon -->
    <link rel="shortcut icon" href="#">

    <link rel="apple-touch-icon" href="#">
    <link rel="apple-touch-icon" sizes="114x114" href="#">
    <link rel="apple-touch-icon" sizes="72x72" href="#">
    <link rel="apple-touch-icon" sizes="144x144" href="#">

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/user.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/script2.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#inscriptionForm").validate({
                rules: {
                    "name": "required",
                    "email": {
                        required: true,
                        email: true
                    },
                    "nom_utilisateur": "required",
                    "telephone": "required",
                    "adresse": "required",
                    "password": "required",
                    "last_password": "required",
                    "password_confirmation": "required",
                    "nous_connaitre": "required"
                },
                messages: {
                    "name": "Ce champ est obligatoire.",
                    "email": {
                        required: "Ce champ est obligatoire.",
                        email: "L'adresse email n'est pas valide."
                    },
                    "nom_utilisateur": "Ce champ est obligatoire.",
                    "telephone": "Ce champ est obligatoire.",
                    "adresse": "Ce champ est obligatoire.",
                    "password": "Ce champ est obligatoire.",
                    "last_password": "Ce champ est obligatoire.",
                    "password_confirmation": "Ce champ est obligatoire.",
                    "nous_connaitre": "Ce champ est obligatoire."
                }
            });
            $('#nous_connaitre').val('<?php if (isset($current_user)) {
                echo $current_user->nous_connaitre;
            } ?>');
        });
    </script>

</head>
<body>
@include('block.user.form')
</body>
</html>

