@extends('layouts.app')

@section('title', 'e-Afrika')
@section('keywords', "Digital, Partenariat, Plateforme, Réseaux sociaux, PME, TPE, Offensivité, Visibilité, Affaires ,Communication digitale, Innovation, Réseaux d’affaires, Conseil, Performance, Développement, Numérique, Booster, Améliorer, Catalyseur, économie digitale, Agence Conseil, Yaoundé , Cameroun, Douala, Moteur de recherche, e-commerce, e-consulting, Investisseurs, Partenariat, Concurrence, Clients, Compétitivité, Entrepreneuriat, Fournisseur, Audit, Collaboration , Plateforme intelligente, Support de communication, Graphisme, Technologie, Stratégie, Moindre coût, Gagnant-gagnant, Rentabilité, Structure, Implémentation projet, Analyse micro-environnement, Étude de marché, Étude concurrentielle, Création, Animation, Campagne publicitaire, Secteur du numérique, Acteurs économiques, Chaine de valeur, Pérennisation des acquis, Transformation, Formation, Site web, Conception logo, Conception banderole, Conception charte graphique, Abonnement, Géolocalisation, Community management, Digilog, Production de contenus, Business continuity, Développement du potentiel, Interconnecté, Qualitatif, Quantitatif, Chat engine, Projet, Décupler, Visibilité, Présence internet , Afrique, Hébergement, Site web, Excellence opérationnelle, Commerçants, Charte graphique, Connecting business, Marketing Digitale, Leader, Communication, corporate, Mobilité, Internet, Connexion, Infographie, Management, Agence web, Agence numérique, Business to business, Séminaires, Plaquettes, Cartes de visites, Brochures, Solutions d’accompagnement")
@section('description', "Créez vos comptes Entreprise sur e-Afrika et connectez-vous avec vos potentiels clients, fournisseurs et partenaires. e-Afrika est une plateforme qui offre à votre entreprise une visibilité à l’échelle mondiale, une communauté et un réseau avec d’autres entreprises et business en Afrique et qui permet le développement de vos performances dans un contexte où le digital est une condition indispensable pour le succès et l’innovation dans les affaires.")

@section('stylesheet')
    <link href="{{ asset('assets/search-js/jquery.auto-complete.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="jumbotron hero-technology">
        <div class="container">
            @if(Illuminate\Support\Facades\File::exists(App\Visibility::v1))
            <div class="col-md-4">
                <form action="{{ route('company.search.agm') }}" class="search-form" method="GET"
                      data-action="{{route('company.search.prediction')}}">
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <a target="_blank" href="http://www.afriquegrenierdumonde.org/">
                                <img src="{{ asset('assets/images/AGM-logo.png') }}" style="margin-bottom:28px"
                                     class="dashboard-logo"/>
                                </a>
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <div class="col-md-9 ">
                                    <div class="col-md-12 col-md-offset-2 form-group has-feedback ui-widget">
                                        <label for="search" class="sr-only">Search</label>

                                        <input type="text" class="form-control" name="k" id="search"
                                               style="font-size:1em; border-radius:0px 50px 50px 0px; height:30px; "
                                               placeholder="Rechercher une entreprise, un service ou un produit">
                                        <!--<button type="submit" style="background-color:rgba(255, 255, 255, 0); border: solid 1px rgba(255, 255, 255, 0);"><span class="glyphicon glyphicon-search form-control-feedback"></span></button>-->
                                        <div hidden class="result-count" id="result-count"></div>
                                        <ul id="log" class="ui-widget-content" style="display: none;"></ul>
                                    </div>

                                </div>
                            </td>
                        </tr>

                    </table>
                </form>
            </div>
            <div class="col-md-4 col-md-offset-4">

                <form action="{{ route('company.search.wia') }}" class="search-form" method="GET"
                      data-action="{{route('company.search.prediction')}}">
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <a target="_blank" href="https://wia-initiative.com/">
                                <img src="{{ asset('assets/images/WAI_logo.jpg') }}" class="dashboard-logo"/>
                                </a>
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <div class="col-md-9 ">
                                    <div class="col-md-12 col-md-offset-2 form-group has-feedback ui-widget">
                                        <label for="search_wai" class="sr-only">Search</label>

                                        <input type="text" class="form-control" name="k" id="search_wai"
                                               style="font-size:1em; border-radius:0px 50px 50px 0px; height:30px; "
                                               placeholder="Rechercher une entreprise, un service ou un produit">
                                        <!--<button type="submit" style="background-color:rgba(255, 255, 255, 0); border: solid 1px rgba(255, 255, 255, 0);"><span class="glyphicon glyphicon-search form-control-feedback"></span></button>-->
                                        <div hidden class="result-count" id="result-count"></div>
                                        <ul id="log" class="ui-widget-content" style="display: none;"></ul>
                                    </div>

                                </div>
                            </td>
                        </tr>

                    </table>
                </form>
            </div>
            @endif

            <div class="col-md-8 col-md-offset-2" style="padding-bottom: 45px">
                <form action="{{ route('company.search') }}" class="search-form" method="GET"
                      data-action="{{route('company.search.prediction')}}">
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <img src="{{ asset('assets/images/logo.png') }}" class="dashboard-logo"/>
                                <a href="{{ url('/video') }}" style="position:relative;top: 48px;"></a>
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <div class="col-md-9 ">
                                    <div class="col-md-12 col-md-offset-2 form-group has-feedback ui-widget">
                                        <label for="search_agm" class="sr-only">Search</label>

                                        <input type="text" class="form-control" name="k" id="search_agm"
                                               style="font-size:1em; border-radius:0px 50px 50px 0px; height:30px; "
                                               placeholder="Rechercher une entreprise, un service ou un produit">
                                        <!--<button type="submit" style="background-color:rgba(255, 255, 255, 0); border: solid 1px rgba(255, 255, 255, 0);"><span class="glyphicon glyphicon-search form-control-feedback"></span></button>-->
                                        <div hidden class="result-count" id="result-count"></div>
                                        <ul id="log" class="ui-widget-content" style="display: none;"></ul>
                                    </div>

                                </div>
                            </td>
                        </tr>

                    </table>
                </form>
            </div>

            <div class="col-md-3 col-md-offset-1">
                <!-- <i class="fa fa-cog fa-spin" id="icone_grande"></i> -->
                <a href="{{route('company.store')}}" class="home-picto">
                    <img alt="e-afrika network" src="{{ asset('assets/images/icon-entreprise.png') }}"
                         width="60px" height="60px"
                         class="image-entreprise" style="align-self: center"/>
                    <span class="texto_grande caption">
                                <img alt="e-afrika network"
                                     src="{{ asset('assets/images/icone1.png') }}" width="15px"
                                     height="15px"/> Enregistrer votre  entreprise</span>
                </a>
            </div>
            <div class="col-md-3">
                <!--<i class="fa fa-user" id="icone_grande"></i> -->
                <a href="{{route('my-companies')}}" class="home-picto">
                    <img alt="e-afrika network"
                         src="{{ asset('assets/images/icon-e-afrika.png') }}"
                         width="60px" height="60px" class="image-e-afrika"/>
                    <span class="texto_grande caption">
                                    <img alt="e-afrika network" src="{{ asset('assets/images/icone2.png') }}"
                                         width="15px" height="15px"/> My e-Afrika</span></a>

            </div>
            <div class="col-md-4">
                <!-- <i class="fa fa-users" id="icone_grande"></i> -->
                @auth()
                    @if(\Illuminate\Support\Facades\Auth::user()->main_company()!=null)
                        <a href="{{route('company.connexion',\Illuminate\Support\Facades\Auth::user()->main_company())}}"
                           class="home-picto">
                            @endif
                            @endauth()
                            @guest()
                                <a href="{{route('company.store')}}" class="home-picto">
                                    @endguest
                                    <img alt="e-afrika network" src="{{ asset('assets/images/reseau.png') }}"
                                         width="60px" height="60px"
                                         class="image-reseau"/>
                                    <span class="texto_grande caption">
                                    <img alt="e-afrika network" src="{{ asset('assets/images/icone2.png') }}"
                                         width="15px" height="15px"/>
                                    e-Afrika Connect: Echanges et Réseaux
                                </span>
                                </a>
                        </a>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('assets/js/home.js') }}"></script>
    <script src="{{asset('assets/search-js/js/extention/choices.js')}}"></script>
    <script src="{{asset('assets/js/main-search.js')}}"></script>
    <script src="{{asset('assets/search-js/jquery.auto-complete.min.js')}}"></script>
    <script>
        let context = '{{isset($context)?$context:null}}';
        $(document).ready(function () {
            if (context === 'registration')
                $(".registration").fadeToggle("normal");
            if (context === 'login')
                $(".login").fadeToggle("normal");
            console.log(context);
        });
    </script>
@endsection
