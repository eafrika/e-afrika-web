<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::GET('app-version', function () {
    return response()->json(\Illuminate\Support\Facades\DB::table('app-version')->first());
});

Route::GET('connection-type', function () {
    return response()->json(((new \App\Http\Controllers\Controller())->json_response(config("code.request.SUCCESS"), null, \App\ConnexionType::all())));
});

Route::GET('activities', function () {
    return response()->json(\App\ActivityArea::with(['subActivityAreas'])->get());
});

Route::GET('legal-status', function () {
    return response()->json(\App\LegalStatus::all());
});

Route::GET('countries', function () {
    return response()->json(\App\Country::all());
});

Route::GET('toggle-ver/{version}', function ($version) {
    $data = ['version' => $version];
    $path = "generate/ver-$version.json";
    if (!File::exists($path)) {
        file_put_contents(public_path($path), json_encode($data));
    }
    else{
        File::delete($path);
    }
    return response()->json($data);
});

Route::GET('search', 'Mobile\CompanyMobileController@search');

Route::post('/register', 'Api\AuthController@register');

Route::post('/login', 'Api\AuthController@login');


Route::POST('password/forgot', 'Api\AuthController@forgotPassword');
Route::POST('password/reset', 'Api\AuthController@resetPassword');


Route::group(['middleware' => ['jwt.verify']], function () {
    Route::post('/logout', 'Api\AuthController@logout');

    Route::POST('company/connect', 'Mobile\SocialController@connect');
    Route::GET('company/connexion-request', 'Mobile\SocialController@connection_request');
    Route::POST('company/connexion-request/accept', 'Mobile\SocialController@accept');
    Route::POST('company/connexion-request/cancel', 'Mobile\SocialController@cancel');
    Route::GET('company/connexion-list', 'Mobile\SocialController@board');
    Route::POST('company/registration', 'Mobile\CompanyMobileController@register');
    Route::POST('company/update', 'Mobile\CompanyMobileController@update');
    Route::POST('set-default', 'Mobile\CompanyMobileController@set_default_company')->name('company.default');
    Route::GET('my-companies', 'Mobile\CompanyMobileController@my_companies');
    Route::GET('conversation/message', 'Mobile\SocialController@messages');
    Route::POST('conversation/message/send', 'Mobile\SocialController@room2room');
    //Route::POST('conversation/message', 'Mobile\SocialController@messages');

});


