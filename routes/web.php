<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::GET('/', 'Web\WelcomeWebController@index')->name('welcome');

//database transfert
Route::GET('gen', 'Transfert\StructureUpdateController@generateAll')->name('gen.all');

// Authentication Routes...
Route::GET('login', 'Web\WelcomeWebController@login')->name('login.form');

Route::POST('login', 'Auth\LoginController@login')->name('login');
Route::POST('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::GET('register', 'Auth\RegisterController@showRegistrationForm');
Route::POST('register', 'Auth\RegisterController@register')->name('register');

// Password Reset Routes...
Route::GET('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::POST('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::GET('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::POST('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::GET('company/search-result', 'Web\CompanyWebController@search_result')->name('company.search');
Route::GET('company/search-wia', 'Web\CompanyWebController@search_wia')->name('company.search.wia');
Route::GET('company/search-agm', 'Web\CompanyWebController@search_agm')->name('company.search.agm');
Route::GET('company/search', 'Web\CompanyWebController@search')->name('company.search.prediction');
Route::GET('company/detail/{company}', 'Web\CompanyWebController@detail')->name('company.detail');


Route::GET('/home', 'Web\WelcomeWebController@index')->name('home');
Route::GET('about', 'HomeController@about')->name('about');
Route::GET('intro', 'HomeController@intro')->name('intro');
Route::GET('terms-and-conditions', 'HomeController@terms')->name('terms');
Route::GET('our-approach', 'HomeController@approach')->name('our-approach');
Route::POST('/newsletter/subscribe', 'Web\NewsletterWebController@create')->name('newsletter.subscribe');
Route::GET('/newsletter/subscribe', 'Web\NewsletterWebController@index');

Route::GET('/restore/image', 'Transfert\RestoreImageController@restore');

Route::middleware('auth')->group(function () {
    Route::POST('upload-logo/{company}', 'Web\CompanyWebController@upload_logo')->name('upload-logo');
    Route::GET('upload-logo/{company}', 'Web\CompanyWebController@upload_logo_form');
    Route::POST('upload-banner/{company}', 'Web\CompanyWebController@upload_banner')->name('upload-banner');
    Route::GET('upload-banner/{company}', 'Web\CompanyWebController@upload_banner_form');
    Route::GET('my-companies', 'Web\CompanyUserWebController@my_company')->name('my-companies');
    Route::POST('company-contact/{company}', 'Web\CompanyWebController@send_contact')->name('contact.company');
    Route::GET('company-contact/{company}', 'Web\CompanyWebController@show_contact_form');
    Route::GET('set-default/{company}', 'Web\CompanyWebController@set_default_company')->name('company.default');
    Route::GET('company/registration', 'Web\CompanyWebController@show_company_form');
    Route::POST('company/registration', 'Web\CompanyWebController@register')->name('company.store');
    Route::GET('company/update/{company}', 'Web\CompanyWebController@show_company_update_form');
    Route::POST('company/update/{company}', 'Web\CompanyWebController@update')->name('company.update');
    Route::GET('user/profile', 'Web\UserWebController@index');
    Route::POST('user/profile', 'Web\UserWebController@update')->name('user.profile.update');
    Route::GET('company/connect/{company}', 'Web\SocialWebController@index');
    Route::POST('company/connect/{company}', 'Web\SocialWebController@connect')->name('company.connect');
    Route::GET('company/connexion-request/{company}', 'Web\SocialWebController@connection_request')->name('company.connect.request');
    Route::GET('company/connexion-request/accept/{company}', 'Web\SocialWebController@accept')->name('company.connect.request.accept');
    Route::GET('company/connexion-request/cancel/{company}', 'Web\SocialWebController@cancel')->name('company.connect.request.cancel');
    Route::GET('company/connexion/list/{company}', 'Web\SocialWebController@board')->name('company.connexion');
    Route::POST('chat/message', 'Web\MessageWebController@store')->name('message.store');
    Route::POST('chat/message/inbox', 'Web\MessageWebController@room2room')->name('message.store.inbox');
    Route::GET('company-activities', 'Web\CompanyWebController@payment_board')->name('company.activities');
    //Route::GET('payment-form', 'Web\CompanyWebController@payment_form');
    Route::POST('payment-form', 'Web\CompanyWebController@payment_save')->name('company.payment.start');
});


